#Entwicklungspraktikum: Online Trainingsplattform #
###1. Vorraussetzungen

## 1. Voraussetzungen ##
Um das Projekt lokal laufen lassen zu können, müssen folgende Softwares lokal installiert sein: </br>
1. Java 11 <br />
2. JDK 11.0.1 <br />
3. Maven 3.6.0 <br />
4. PostgreSQL 11 <br />
5. Node.js 10.15.1 <br/>

Unter src/main/resources/application.properties können die Zugangsdaten für die Datenbank geändert werden.
<br/>Zugangsdaten PostgreSQL<br/>
    Username: ep <br/>
    Passwort: ep <br/>
    
## 3. Zugangsdaten zum Mailservice ##
Die Zugangsdaten für den Mailservice werden in der application.properties im Verzeichnis
src/main/resources und in src/main/Mailservice festgelegt.
<br/>

## 4. Serveradresse ##
Die Serveradresse kann am Client in der GlobalConfig.vue global geändert werden. Die GlobalConfig.vue
ist im Verzeichnis src/main/resources/static/Vue/src/components zu finden.

Der Serverport wird in der application.properties im Verzeichnis src/main/resources im Bereich HTTPS festgelegt.
Sie muss außerdem im MailService angepasst werden, die entsprechende Zeile ist kommentiert.
<br/>

## 5. HTTPS Zertifikat ##
Dsa HTTPS Zertifikat ist im Verzeichnis src/main/resources/ssl gefunden. Die entsprechenden Zugriffsparameter
sind in der application.properties im Verzeichnis src/main/resources festgehalten.
<br/>

## 6. LogFile ##
Das Konfiguration der Logger ist in der logging.properties im Pfad main/resources/logging.properties zu finden.
Dort wird unter anderem angegeben, in welches File die Logs geschrieben werden.

Die logging.properties wird in der EpApplication.java im Pfad main/java/config/EpApplication.java geladen.
Hier ist der absolute Pfad des Logfiles angegeben und muss entsprechend angepasst werden.