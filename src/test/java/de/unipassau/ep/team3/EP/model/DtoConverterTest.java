package de.unipassau.ep.team3.EP.model;

import de.unipassau.ep.team3.EP.database.ExerciseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
@RunWith(SpringRunner.class)
public class DtoConverterTest {
    @Autowired
    private DtoConverter dtoConverter;

    @Autowired
    private ExerciseRepository exerciseRepository;

    private UserDto uDTO;
    private User user;
    private User userAimtest;
    private Aim testSuperAim;
    private AimDto aimDto;
    private Aim aimFromConvert;
    private Exercise exerciseConvertFrom;
    private ExerciseDto exerciseDtoConvertFrom;
    private Experience experienceConvertFrom;
    private ExperienceDto experienceDtoConvertFrom;
    private URL url;
    private ExerciseWrapper wrapper;
    private ExerciseWrapperDto wrapperDto;
    private Schedule schedule;
    private List<String>  rank = new ArrayList<>();
    private List<Integer>  rankIndexes = new ArrayList<>();
    private List<Exercise> exercises = new LinkedList<>();
    private List<String> executions = new ArrayList<>();
    private List<ExerciseWrapper>  wrappers = new LinkedList<>();
    private List<Exercise> alternativeExercises = new LinkedList<>();
    private Exercise altExercise;
    private ExerciseDto altExerciseDto;



    @Before
    public void setUp() throws Exception {
        uDTO = Mockito.mock(UserDto.class);
        when(uDTO.getPassword()).thenReturn("12345678");
        when(uDTO.getDayOfBirth()).thenReturn(29);
        when(uDTO.getMonthOfBirth()).thenReturn(2);
        when(uDTO.getYearOfBirth()).thenReturn(1996);
        when(uDTO.getName()).thenReturn("Dieter");
        when(uDTO.getZim()).thenReturn("einste01");

        user = Mockito.mock(User.class);
        when(user.getZim()).thenReturn("herman26");
        when(user.getName()).thenReturn("Vanessa");
        when(user.getGender()).thenReturn(Gender.FEMALE);
        when(user.getAim()).thenReturn(aimFromConvert);
        when(user.getExperience()).thenReturn(experienceConvertFrom);
        when(user.getWeight()).thenReturn(75);
        when(user.getHeight()).thenReturn(166);

        userAimtest = Mockito.mock(User.class);
        when(userAimtest.getZim()).thenReturn("herman26");

        testSuperAim = Mockito.mock(Aim.class);
        when(testSuperAim.getId()).thenReturn((long) 1);


        aimDto = Mockito.mock(AimDto.class);
        when(aimDto.getName()).thenReturn("Bauch");
        when(aimDto.getDescription()).thenReturn("Lorem ipsum");
        when(aimDto.getFurtherInformation()).thenReturn("dolor sit amet");

        aimFromConvert = Mockito.mock(Aim.class);
        when(aimFromConvert.getName()).thenReturn("weightloss");
        when(aimFromConvert.getId()).thenReturn((long) 1);
        when(aimFromConvert.getDescription()).thenReturn("lorem ipsum");
        when(aimFromConvert.getFurtherInformation()).thenReturn("dolor sit amet");
        when(aimFromConvert.getEditor()).thenReturn(userAimtest);

        exerciseDtoConvertFrom = Mockito.mock(ExerciseDto.class);
        when(exerciseDtoConvertFrom.getId()).thenReturn(1L);
        when(exerciseDtoConvertFrom.getName()).thenReturn("Sit-up");
        when(exerciseDtoConvertFrom.getDescription()).thenReturn("Lorem ipsum");
        when(exerciseDtoConvertFrom.getVideoURL()).thenReturn("https://www.google.de/");
        when(exerciseDtoConvertFrom.getEditor()).thenReturn("herman26");

        url = new URL("https://www.google.de/");
        exerciseConvertFrom = Mockito.mock(Exercise.class);
            when(exerciseConvertFrom.getName()).thenReturn("Sit-up");
            when(exerciseConvertFrom.getDescription()).thenReturn(("Lorem ipsum"));
            when(exerciseConvertFrom.getVideo()).thenReturn(url);
            when(exerciseConvertFrom.getId()).thenReturn((long)1);
            when(exerciseConvertFrom.getEditor()).thenReturn(userAimtest);


        experienceConvertFrom = Mockito.mock(Experience.class);
        when(experienceConvertFrom.getName()).thenReturn("Expert");
        when(experienceConvertFrom.getId()).thenReturn((long) 1);
        when(experienceConvertFrom.getDescription()).thenReturn("Lorem ipsum");
        when(experienceConvertFrom.getEditor()).thenReturn(userAimtest);

        experienceDtoConvertFrom = Mockito.mock(ExperienceDto.class);
        when(experienceDtoConvertFrom.getName()).thenReturn("New");
        when(experienceDtoConvertFrom.getId()).thenReturn((long) 2);
        when(experienceDtoConvertFrom.getDescription()).thenReturn("Lorem ipsum");
        when(experienceDtoConvertFrom.getEditor()).thenReturn("herman26");

        String [] speed = {"low"};
        int [] pause = {20};
        String [][] iteration = {{"10"}};
        String [][] load = {{"10 RM"}};

        wrapper = Mockito.mock(ExerciseWrapper.class);
        when(wrapper.getSpeed()).thenReturn(speed);
        when(wrapper.getPause()).thenReturn(pause);
        when(wrapper.getLoad()).thenReturn(load);
        when(wrapper.getIteration()).thenReturn(iteration);
        when(wrapper.getId()).thenReturn(1L);

        wrapperDto = Mockito.mock(ExerciseWrapperDto.class);
        when(wrapperDto.getSpeed()).thenReturn(speed);
        when(wrapperDto.getPause()).thenReturn(pause);
        when(wrapperDto.getLoad()).thenReturn(load);
        when(wrapperDto.getIteration()).thenReturn(iteration);

        schedule = Mockito.mock(Schedule.class);
        when(schedule.getId()).thenReturn(1L);
        when(schedule.getAim()).thenReturn(aimFromConvert);
        when(schedule.getLevel()).thenReturn(experienceConvertFrom);
        when(schedule.getGender()).thenReturn(Gender.FEMALE);

        rank.add("A1");
        when(schedule.getRank()).thenReturn(rank);

        rankIndexes.add(1);
        when(schedule.getRankIndexes()).thenReturn(rankIndexes);

        executions.add("high");
        when(schedule.getExecutions()).thenReturn(executions);

        wrappers.add(wrapper);
        when(schedule.getWrappers()).thenReturn(wrappers);

        exercises.add(exerciseConvertFrom);
        when(schedule.getExercises()).thenReturn(exercises);

        altExercise = Mockito.mock(Exercise.class);
        when(altExercise.getId()).thenReturn(3L);
        when(altExercise.getEditor()).thenReturn(user);
        when(altExercise.getName()).thenReturn("jump");
        alternativeExercises.add(altExercise);
        when(schedule.getAlternativeExercises()).thenReturn(alternativeExercises);

        when(schedule.getEditor()).thenReturn(user);

        altExerciseDto = Mockito.mock(ExerciseDto.class);
        when(altExerciseDto.getId()).thenReturn(3L);
        when(altExerciseDto.getName()).thenReturn("jump");
        when(altExerciseDto.getEditor()).thenReturn("herman26");


    }

    /**
     * A test if the DtoConverter converts an UserDto to an User correctly.
     */
    @Test
    public void convertFromDto() {
        User user = DtoConverter.convertFromDto(uDTO);
        User userToCompare = new User("einste01", "Dieter", "12345678", null);
        LocalDate birthday = LocalDate.of(1996, 2, 29);
        userToCompare.setBirthday(birthday);
        assertNotNull(user);
        assertNotNull(userToCompare);
        assertEquals( userToCompare.getBirthday(),user.getBirthday());
        assertEquals(userToCompare.getName(),user.getName() );
        assertEquals(userToCompare.getZim(),user.getZim());
    }

    /**
     * A test to check if a User is converted correctly to a user dto.
     */
    @Test
    public void convertUserToUserDto() {
        UserDto dto = DtoConverter.convertToDto(user);
        UserDto toCompare = new UserDto("herman26","Vanessa");
        toCompare.setGender("FEMALE");
        toCompare.setAimId(1L);
        toCompare.setExperienceId(1L);
        toCompare.setHeight(166);
        toCompare.setWeight(75);
        assertNotNull(dto);
        assertNotNull(toCompare);
        assertEquals(toCompare.getZim(),dto.getZim());
        assertEquals(toCompare.getName(),dto.getName());
        assertEquals(toCompare.getGender(),dto.getGender());
        assertEquals(toCompare.getHeight(),dto.getHeight());
        assertEquals(toCompare.getWeight(),dto.getWeight());

    }

    /**
     * A test if the DtoConverter converts an aim Dto to an aim correctly.
     */
    @Test
    public void convertToAimFromAimDto(){
        Aim aim = DtoConverter.convertFromDto(aimDto);
        Aim aimToCompare = new Aim("Bauch");
        aimToCompare.setDescription("Lorem ipsum");
        aimToCompare.setFurtherInformation( "dolor sit amet");
        assertNotNull(aim);
        assertNotNull(aimToCompare);
        assertEquals(aimToCompare.getName(),aim.getName());
        assertEquals(aimToCompare.getDescription(),aim.getDescription());
        assertEquals(aimToCompare.getFurtherInformation(),aim.getFurtherInformation());
    }

    /**
     * A test if the DtoConverter converts an aim to an aim DTO correctly
     */
    @Test
    public void convertToAimDtoFromAim(){
        AimDto aimDtoToConvert = DtoConverter.convertToDto(aimFromConvert);
        AimDto aimDtoToCompare = new AimDto("weightloss");
        aimDtoToCompare.setSuperAimId((long) 0);
        aimDtoToCompare.setDescription("lorem ipsum");
        aimDtoToCompare.setFurtherInformation("dolor sit amet");
        aimDtoToCompare.setEditorZim("herman26");
        aimDtoToCompare.setId((long)1);
        assertNotNull(aimDtoToCompare);
        assertNotNull(aimDtoToConvert);
        assertEquals(aimDtoToCompare.getName(),aimDtoToConvert.getName());
        assertEquals(aimDtoToCompare.getSuperAimId(),aimDtoToConvert.getSuperAimId());
        assertEquals(aimDtoToCompare.getDescription(),aimDtoToConvert.getDescription());
        assertEquals(aimDtoToCompare.getFurtherInformation(),aimDtoToConvert.getFurtherInformation());
        assertEquals(aimDtoToCompare.getId(),aimDtoToConvert.getId());
        assertEquals(aimDtoToCompare.getEditorZim(),aimDtoToConvert.getEditorZim());
        assertEquals(aimDtoToCompare.getSubAimsCount(),aimDtoToConvert.getSubAimsCount());

    }

    /**
     * A test if the DtoConverter converts an exercise DTO to an exercise object correctly.
     */
    @Test
    public void convertToExerciseFromExerciseDto() throws MalformedURLException {
        Exercise exercise = DtoConverter.convertFromDto(exerciseDtoConvertFrom);
        Exercise exerciseToCompare = new Exercise(("Sit-up"));
        exerciseToCompare.setDescription("Lorem ipsum");
        URL url = new URL("https://www.google.de/");
        exerciseToCompare.setVideo(url);
        assertNotNull(exercise);
        assertNotNull(exerciseToCompare);
        assertEquals(exerciseToCompare.getName(),exercise.getName());
        assertEquals(exerciseToCompare.getDescription(),exercise.getDescription() );
        assertEquals( exerciseToCompare.getVideo(),exercise.getVideo());
    }

    /**
     * A test if the DtoConverter converts an exercise to an exercise DTO correctly.
     */
    @Test
    public void convertToExerciseDtoFromExercise() {
        ExerciseDto exerciseDto = DtoConverter.convertToDto(exerciseConvertFrom);
        ExerciseDto exerciseDtoToCompare = new ExerciseDto("Sit-up");
        exerciseDtoToCompare.setDescription("Lorem ipsum");
        exerciseDtoToCompare.setVideoURL("https://www.google.de/");
        exerciseDtoToCompare.setId((long) 1);
        exerciseDtoToCompare.setEditor("herman26");
        assertNotNull(exerciseDto);
        assertNotNull(exerciseDtoToCompare);
        assertEquals(exerciseDtoToCompare.getName(),exerciseDto.getName());
        assertEquals( exerciseDtoToCompare.getDescription(),exerciseDto.getDescription());
        assertEquals(exerciseDtoToCompare.getVideoURL(),exerciseDto.getVideoURL());
        assertEquals(exerciseDtoToCompare.getId(),exerciseDto.getId());
        assertEquals(exerciseDtoToCompare.getEditor(),exerciseDto.getEditor() );


    }

    /**
     * Tests if the DtoConverter converts an experience level DTO to an experience level correctly.
     */
    @Test
    public void convertToExperienceFromExperienceDto() {
        Experience experience = DtoConverter.convertFromDto(experienceDtoConvertFrom);
        Experience experienceToCompare = new Experience("New");
        experienceToCompare.setId((long)2);
        experienceToCompare.setDescription("Lorem ipsum");
        experienceToCompare.setEditor(userAimtest);
        assertNotNull(experience);
        assertNotNull(experienceToCompare);
        assertEquals(experienceToCompare.getName(),experience.getName());
        assertEquals(experienceToCompare.getDescription(),experience.getDescription());}

    /**
     * Tests if teh DtoCOnverter converts an experience level to an experience level DTO correctly.
     */
    @Test
    public void convertToExperienceDtoFromExperience(){
        ExperienceDto experienceDto = DtoConverter.convertToDto(experienceConvertFrom);
        ExperienceDto experienceDtoToCompare = new ExperienceDto("Expert");
        experienceDtoToCompare.setId((long) 1);
        experienceDtoToCompare.setDescription("Lorem ipsum");
        experienceDtoToCompare.setEditor("herman26");
        assertNotNull(experienceDto);
        assertNotNull(experienceDtoToCompare);
        assertEquals(experienceDto.getName(),experienceDtoToCompare.getName());
        assertEquals(experienceDtoToCompare.getId(),experienceDto.getId());
        assertEquals(experienceDtoToCompare.getDescription(),experienceDto.getDescription());
        assertEquals(experienceDtoToCompare.getEditor(),experienceDto.getEditor());
    }

    @Test
    public void convertWrapperDtoToWrapper() {
        ExerciseWrapper wrapper1 = DtoConverter.convertFromDto(wrapperDto);
        String [] speed = {"low"};
        int [] pause = {20};
        String [][] iteration = {{"10"}};
        String [][] load = {{"10 RM"}};
        ExerciseWrapper toCompare = new ExerciseWrapper(speed,pause,iteration,load);
        assertNotNull(wrapper1);
        assertNotNull(toCompare);
        assertArrayEquals(toCompare.getSpeed(),wrapper1.getSpeed());
        assertArrayEquals(toCompare.getPause(),wrapper1.getPause());
        assertArrayEquals(toCompare.getIteration(),wrapper1.getIteration());
        assertArrayEquals(toCompare.getLoad(),wrapper1.getLoad());
    }

    @Test
    public void convertWrapperToWrapperDto() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
      //  ExerciseWrapperDto wrapperDto1 = DtoConverter.conv
        Method method = DtoConverter.class.getDeclaredMethod("convertToDto", ExerciseWrapper.class);
        method.setAccessible(true);
        ExerciseWrapperDto wrapperDto1 = (ExerciseWrapperDto) method.invoke(dtoConverter,wrapper);
        String [] speed = {"low"};
        int [] pause = {20};
        String [][] iteration = {{"10"}};
        String [][] load = {{"10 RM"}};
        ExerciseWrapperDto toCompare = new ExerciseWrapperDto(speed,pause,iteration,load);
        toCompare.setId(1L);
        assertEquals(toCompare.getId(),wrapperDto1.getId());
        assertArrayEquals(toCompare.getSpeed(),wrapperDto1.getSpeed());
        assertArrayEquals(toCompare.getPause(),wrapperDto1.getPause());
        assertArrayEquals(toCompare.getIteration(),wrapperDto1.getIteration());
        assertArrayEquals(toCompare.getLoad(),wrapperDto1.getLoad());
    }

    @Test
    public void convertScheduleToScheduleDto(){
        ScheduleDto dto = DtoConverter.convertToDto(schedule);
        assertNotNull(dto);
        ScheduleDto toCompare = new ScheduleDto();
        toCompare.setAimId(1L);
        toCompare.setLevelId(1L);
        toCompare.setGender("FEMALE");
        toCompare.setId(1L);

        String [] rank = {"A1"};
        toCompare.setRank(rank);

        int [] rankIndexes = {0};
        toCompare.setRankIndexes(rankIndexes);

        String [] executions = {"high"};
        toCompare.setExecutions(executions);

        ExerciseDto [] exercises = {exerciseDtoConvertFrom};
        toCompare.setExercises(exercises);

        String [] speed = {"low"};
        int [] pause = {20};
        String [][] iteration = {{"10"}};
        String [][] load = {{"10 RM"}};
        ExerciseWrapperDto  wrapperDto = new ExerciseWrapperDto(speed,pause,iteration,load);
        wrapperDto.setId(1L);
        ExerciseWrapperDto [] wrappersDto = {wrapperDto};
        toCompare.setWrapper(wrappersDto);


        ExerciseDto [] alternativeExercises = {altExerciseDto};
        toCompare.setAlternativeExercises(alternativeExercises);


        assertEquals(toCompare.getAimId(),dto.getAimId());
        assertEquals(toCompare.getLevelId(),dto.getLevelId());
        assertEquals(toCompare.getId(),dto.getId());
        assertEquals(toCompare.getGender(),dto.getGender());
        assertArrayEquals(toCompare.getExecutions(),dto.getExecutions());

        //Compare the wrapper
        assertEquals(toCompare.getWrapper()[0].getId(),dto.getWrapper()[0].getId());
        assertArrayEquals(toCompare.getWrapper()[0].getIteration()[0],dto.getWrapper()[0].getIteration()[0]);
        assertArrayEquals(toCompare.getWrapper()[0].getSpeed(),dto.getWrapper()[0].getSpeed());
        assertArrayEquals(toCompare.getWrapper()[0].getPause(),dto.getWrapper()[0].getPause());
        assertArrayEquals(toCompare.getWrapper()[0].getLoad()[0],dto.getWrapper()[0].getLoad()[0]);

        //compare the exercise
        assertEquals(toCompare.getExercises()[0].getId(),dto.getExercises()[0].getId());
        assertEquals(toCompare.getExercises()[0].getName(),dto.getExercises()[0].getName());
        assertEquals(toCompare.getExercises()[0].getEditor(),dto.getExercises()[0].getEditor());

        //compare the alternative exercise
        assertEquals(toCompare.getAlternativeExercises()[0].getId(),dto.getAlternativeExercises()[0].getId());
        assertEquals(toCompare.getAlternativeExercises()[0].getEditor(),dto.getAlternativeExercises()[0].getEditor());
        assertEquals(toCompare.getAlternativeExercises()[0].getName(),dto.getAlternativeExercises()[0].getName());


    }




}