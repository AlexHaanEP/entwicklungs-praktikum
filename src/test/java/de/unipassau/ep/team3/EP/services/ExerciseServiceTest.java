package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.config.WebSecurityConfig;
import de.unipassau.ep.team3.EP.config.Webconfig;
import de.unipassau.ep.team3.EP.database.ExerciseRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.Exercise;
import de.unipassau.ep.team3.EP.model.ExerciseDto;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;

@RunWith(SpringRunner.class)
@SpringBootTest(classes ={ EpApplication.class, Webconfig.class, WebSecurityConfig.class})
public class ExerciseServiceTest {


    @Autowired
    private ExerciseService exerciseService;

    /**
     * A test to check if the alternative exercises are not the own exercise
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testAreNotOwnId(){
        ExerciseDto exerciseDto = new ExerciseDto("Sit-up");
        long[] test1 = {2};
        exerciseDto.setAlternativeIds(test1);
        testBadRequestException(exerciseDto, BadRequestReason.ALTERNATIVE_ID_OWN_ID.toString());
    }

    /**
     * A test to check if the alternative exercises are existing
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testAlternativesAreExisting(){
        ExerciseDto exerciseDto = new ExerciseDto("Sit-up1");
        long[] test1 = {3};
        exerciseDto.setAlternativeIds(test1);
        testBadRequestException(exerciseDto, BadRequestReason.ALTERNATIVE_ID_NOT_EXISTING.toString());
        long[] test2 = {-1};
        exerciseDto.setAlternativeIds(test2);
        testBadRequestException(exerciseDto, BadRequestReason.ALTERNATIVE_ID_NOT_EXISTING.toString());
    }


    /**
     * Tests if the exercise already exists.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testAlreadyExists() {
        ExerciseDto dto = new ExerciseDto("Sit-up3");
        long[] altId = {};
        dto.setAlternativeIds(altId);
        exerciseService.createNewExercise(dto);
        ExerciseDto dto1 = new ExerciseDto("Sit-up3");
        dto1.setAlternativeIds(altId);
        testBadRequestException(dto1, BadRequestReason.ALREADY_EXISTS.toString());
    }

    /**
     * Tests if the name of the exercise is missing.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testNameIsMissing() {
        ExerciseDto dto = new ExerciseDto();
        testBadRequestException(dto, BadRequestReason.NAME_MISSING.toString());
    }

    /**
     * tests if the name of the exercise is invalid.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testNameIsInvalid() {
        ExerciseDto dto = new ExerciseDto("");
        testBadRequestException(dto, BadRequestReason.INVALID_NAME.toString());
    }

    /**
     * A test to check if its not possible to delete the dummy exercise
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testDeleteDummy(){
        testBadRequestExceptionDelete(1L,BadRequestReason.DUMMY_EXERCISE_ID.toString());
    }

    /**
     * A test to check if the right exceptions are thrown if something
     * isn't correct in updating the exercise.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testUpdate(){
        //Name missing
        ExerciseDto dto = new ExerciseDto();
        testBadRequestExceptionForUpdate(dto,BadRequestReason.NAME_MISSING.toString());
        //Name invalid
        dto.setName("");
        testBadRequestExceptionForUpdate(dto,BadRequestReason.INVALID_NAME.toString());
        //Id missing
        dto.setName("Run");
        testBadRequestExceptionForUpdate(dto,BadRequestReason.ID_MISSING.toString());
        //Dummy id
        dto.setId(1L);
        testBadRequestExceptionForUpdate(dto,BadRequestReason.DUMMY_EXERCISE_ID.toString());
    }

    /**
     * A test to check if the deep copy was copied correctly.
     *
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws MalformedURLException
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testDeepCopy() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, MalformedURLException {
        Method method = ExerciseService.class.getDeclaredMethod("deepCopy", Exercise.class);
        method.setAccessible(true);

        Exercise exercise = new Exercise("Hop");
        exercise.setId(5L);
        exercise.setDescription("Hip");
        URL copyURL = new URL("https://www.google.de/");
        exercise.setVideo(copyURL);
        Exercise deepCopy = (Exercise) method.invoke(exerciseService,exercise);

        Exercise toCompare = new Exercise("Hop");
        toCompare.setId(5L);
        toCompare.setDescription("Hip");
         copyURL = new URL("https://www.google.de/");
        toCompare.setVideo(copyURL);
        assertNotNull(deepCopy);
        assertEquals(toCompare.getDescription(),deepCopy.getDescription());
        assertEquals(toCompare.getName(),deepCopy.getName());
        assertEquals(toCompare.getVideo(),deepCopy.getVideo());
        assertTrue(deepCopy.isAssigned());
    }

    /**
     * A test to check if an exception is trown if the exercise doesn't exist
     * and if the right exercise is loadad if the exercise exists.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testFindExercise(){
        //Exercise not existing
        testBadRequestExceptionFindExercise(9L,BadRequestReason.EXERCISE_NOT_EXISTING.toString());
       //Exercise existing
        Exercise exercise = exerciseService.findExerciseById(1L);
        assertNotNull(exercise);
        Exercise toCompare = new Exercise("NoExercise");
        assertEquals(toCompare.getName(),exercise.getName());
    }

    private void testBadRequestException(ExerciseDto dto, String exMsg) {
        try {
            exerciseService.createNewExercise(dto);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    public void testBadRequestExceptionForUpdate(ExerciseDto update, String exMsg) {
        try {
            exerciseService.update(update);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }}

    private void testBadRequestExceptionDelete(Long id, String exMsg) {
        try {
            exerciseService.delete(id);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionFindExercise(Long id, String exMsg) {
        try {
            Method method = ExerciseService.class.getDeclaredMethod("findExerciseById", Long.class);
            method.setAccessible(true);
            method.invoke(exerciseService,id);
            fail();
        } catch (InvocationTargetException e) {
            assertEquals(exMsg, e.getCause().getMessage());
        } catch (Exception e) {
            fail();
        }
    }
}
