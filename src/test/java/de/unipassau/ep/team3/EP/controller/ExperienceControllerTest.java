package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.config.WebSecurityConfig;
import de.unipassau.ep.team3.EP.config.Webconfig;
import de.unipassau.ep.team3.EP.database.ExperienceRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.ExerciseDto;
import de.unipassau.ep.team3.EP.model.Experience;
import de.unipassau.ep.team3.EP.model.ExperienceDto;
import de.unipassau.ep.team3.EP.services.AimService;
import de.unipassau.ep.team3.EP.services.ExperienceService;
import org.assertj.core.util.IterableUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(classes ={ EpApplication.class, Webconfig.class, WebSecurityConfig.class})
@ContextConfiguration
@WebAppConfiguration
public class ExperienceControllerTest {


    @Autowired
    private ExperienceController experienceController;

    @Autowired
    private ExperienceService experienceService;

    @Autowired
    private ExperienceRepository experienceRepository;

    /**
     * A test to check if the experience level in the database, has the same Attributes as its dto.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER", password = "12345678")
    public void testCreateNewExperienceLevel(){
        ExperienceDto experienceDto = new ExperienceDto("Starter");
        experienceDto.setDescription("Lorem ispum");
        experienceController.createNewExperience(experienceDto);
        Experience experience = experienceRepository.findByExperienceId((long)2);
        assertNotNull(experienceDto);
        assertNotNull(experience);
        assertEquals(experienceDto.getName(),experience.getName());
        assertEquals(experienceDto.getDescription(),experience.getDescription());
        assertEquals("herman26",experience.getEditor().getZim());
    }

    /**
     * Tests if the method gets all experience level from the database.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testGetAll(){
        Iterable<ExperienceDto> testIterable = experienceController.getAll();
        Assert.assertNotNull(testIterable);
        assertEquals(2,IterableUtil.sizeOf(testIterable));
    }

    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testDelete(){
        ExperienceDto experienceDto = new ExperienceDto("Pro");
        experienceDto.setDescription("Lorem");
        experienceController.createNewExperience(experienceDto);
        experienceController.delete((long)2);
        testBadRequestException(2L, BadRequestReason.EXPERIENCE_LEVEL_NOT_EXISTING.toString());


    }

    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testUpdate(){
        ExperienceDto dto = new ExperienceDto("new","Has no idea");
        dto.setId((long)1);
        experienceController.update(dto);
        Experience updated = experienceRepository.findByExperienceId(1);
        assertNotNull(updated);
        assertNotNull(dto);
        assertEquals(dto.getName(),updated.getName());
        assertEquals(dto.getDescription(),updated.getDescription());
        assertEquals(updated.getEditor().getZim(),"herman26");

    }
    /**
     * A test to check if the description of an experience level will be loaded correctly.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testGetDescription(){
        ExperienceDto experienceDto = new ExperienceDto("New");
        experienceDto.setDescription("Never heard of the gym");
        experienceController.createNewExperience(experienceDto);
        assertEquals("Never heard of the gym",experienceController.getDescriptions(1L));
    }

    private void testBadRequestException(Long id, String exMsg) {
        try {
            Method method = ExperienceService.class.getDeclaredMethod("findExperienceById", Long.class);
            method.setAccessible(true);
            method.invoke(experienceService,id);
            fail();
        } catch (InvocationTargetException e) {
            assertEquals(exMsg, e.getCause().getMessage());
        } catch (Exception e) {
            fail();
        }
    }


}