package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.config.WebSecurityConfig;
import de.unipassau.ep.team3.EP.config.Webconfig;
import de.unipassau.ep.team3.EP.database.ExerciseRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.DtoConverter;
import de.unipassau.ep.team3.EP.model.Exercise;
import de.unipassau.ep.team3.EP.model.ExerciseDto;
import de.unipassau.ep.team3.EP.model.UserDto;
import org.assertj.core.util.IterableUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(classes ={ EpApplication.class, Webconfig.class, WebSecurityConfig.class})
@ContextConfiguration
@WebAppConfiguration
public class ExerciseControllerTest {

    @Autowired
    private ExerciseController exerciseController;

    @Autowired
    private ExerciseRepository exerciseRepository;

    /**
     * A test to check if the exercise in the database, has the same Attributes as its dto.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testCreateNewExercise(){
        ExerciseDto exerciseDto = new ExerciseDto("Jumping jack");
        exerciseDto.setVideoURL("https://www.google.de/");
        exerciseDto.setDescription("Lorem ipsum");
        String [] test = {"A"};
        exerciseDto.setExecutions(test);
        long [] alternativeIds = {};
        exerciseDto.setAlternativeIds(alternativeIds);
        ExerciseDto tt = exerciseController.createNewExercise(exerciseDto);
        Exercise exercise = exerciseRepository.findByExerciseId(tt.getId());
        assertNotNull(exercise);
        assertNotNull(exerciseDto);
        assertEquals(exerciseDto.getName(), exercise.getName());
        assertEquals(exerciseDto.getDescription(), exercise.getDescription());
        assertEquals(exerciseDto.getVideoURL(),exercise.getVideo().toString());
        assertEquals(exerciseDto.getVideoURL(),exercise.getVideo().toString());
        ExerciseDto returnExerciseDto = DtoConverter.convertToDto(exercise);
        assertNotNull(returnExerciseDto);
        assertEquals(exercise.getId(),returnExerciseDto.getId());
        assertEquals(exercise.getName(),returnExerciseDto.getName());
        assertEquals(exercise.getDescription(),returnExerciseDto.getDescription());
        assertEquals(exercise.getVideo().toString(),returnExerciseDto.getVideoURL());
        assertEquals(exercise.getEditor().getZim(),returnExerciseDto.getEditor());
        assertEquals("herman26",exercise.getEditor().getZim());
    }

    /**
     * Tests if the method only gets all unassigned exercises.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testGetAllUnassigned(){
        ExerciseDto exerciseDto = new ExerciseDto("Sit-up1");
        exerciseDto.setVideoURL("https://www.google.de/");
        exerciseDto.setDescription("Lorem ipsum");
        long [] alternativeIds = {};
        exerciseDto.setAlternativeIds(alternativeIds);
        exerciseController.createNewExercise(exerciseDto);
        Iterable<ExerciseDto> testIterable = exerciseController.getAllUnassigned();
        assertNotNull(testIterable);
        assertEquals(1,IterableUtil.sizeOf(testIterable));
    }

    /**
     * Test if delete was successfull.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testDelete(){
        exerciseController.delete((long)2);
        testBadRequestException((long) 2 ,BadRequestReason.EXERCISE_NOT_EXISTING.toString());
    }

    /**
     * Tests if the exercise was updated succesfully.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testUpdate(){
        ExerciseDto exercise = new ExerciseDto("Roll");
        exercise.setDescription("Hard");
        exercise.setVideoURL("https://www.youtube.com/");
        long [] alternative = {1};
        exercise.setAlternativeIds(alternative);
        ExerciseDto a = exerciseController.createNewExercise(exercise);
        ExerciseDto update = new ExerciseDto("Backroll");
        update.setId(4L);
        update.setDescription("Easy");
        update.setVideoURL("https://www.facebook.com/");
        long [] alternatives = {1};
        update.setAlternativeIds(alternatives);
        exerciseController.update(update);
        Exercise updated = exerciseRepository.findByExerciseId(4);
        assertNotNull(update);
        assertNotNull(updated);
        assertEquals(update.getId(),updated.getId());
        assertEquals(update.getName(),updated.getName());
        assertEquals(update.getDescription(),updated.getDescription());
        assertEquals(updated.getEditor().getZim(),"herman26");
        assertEquals(update.getVideoURL(),updated.getVideo().toString());
    }

    private void testBadRequestException(Long id, String exMsg) {
        try {
            exerciseController.delete(id);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }
}