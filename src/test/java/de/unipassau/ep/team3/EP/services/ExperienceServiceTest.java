package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.config.WebSecurityConfig;
import de.unipassau.ep.team3.EP.config.Webconfig;
import de.unipassau.ep.team3.EP.database.ExperienceRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.Experience;
import de.unipassau.ep.team3.EP.model.ExperienceDto;
import de.unipassau.ep.team3.EP.model.UserDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(classes ={ EpApplication.class, Webconfig.class, WebSecurityConfig.class})
public class ExperienceServiceTest {

    @Autowired
    private ExperienceRepository experienceRepository;

    @Autowired
    private ExperienceService experienceService;

    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER",password = "12345678")
    public void testCreateExperienceNameMissing(){
        ExperienceDto dto = new ExperienceDto();
        testBadRequestExceptionCreateExperience(dto,BadRequestReason.NAME_MISSING.toString());
    }

    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER",password = "12345678")
    public void testCreateExperienceInvalidName(){
        ExperienceDto dto = new ExperienceDto("");
        testBadRequestExceptionCreateExperience(dto,BadRequestReason.INVALID_NAME.toString());
    }

    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER",password = "12345678")
    public void testCreateExperienceAlreadyExist(){
        ExperienceDto dto = new ExperienceDto("known");
        testBadRequestExceptionCreateExperience(dto,BadRequestReason.ALREADY_EXISTS.toString());
    }

    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER",password = "12345678")
    public void testDeleteLastExperienceLevel(){
        ExperienceDto dto = new ExperienceDto("known");
        experienceService.createNewExperienceLevel(dto);
        testBadRequestException(1L, BadRequestReason.LAST_EXPERIENCE.toString());
    }

    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER",password = "12345678")
    public void testFindById(){
        testBadRequestExceptionFindById(5L,BadRequestReason.EXPERIENCE_LEVEL_NOT_EXISTING.toString());
        ExperienceDto dto = new ExperienceDto("Blub");
        dto.setDescription("blub blub");
        experienceService.createNewExperienceLevel(dto);
        Experience level = experienceService.findExperienceById(2L);
        Experience toCompare = new Experience("Blub");
        toCompare.setId(2L);
        toCompare.setDescription("blub blub");
        assertNotNull(level);
        assertEquals(toCompare.getName(),level.getName());
        assertEquals(toCompare.getId(),level.getId());
        assertEquals(toCompare.getDescription(),level.getDescription());
    }

    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER",password = "12345678")
    public void testUpdate(){
        //Name missing
        ExperienceDto dto = new ExperienceDto();
        dto.setId(2L);
        testBadRequestExceptionUpdate(dto,BadRequestReason.NAME_MISSING.toString());
        //Name invalid
        dto.setName("");
        testBadRequestExceptionUpdate(dto,BadRequestReason.INVALID_NAME.toString());

    }


    private void testBadRequestException(Long id, String exMsg) {
        try {
            experienceService.delete(id);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionCreateExperience(ExperienceDto dto, String exMsg) {
        try {
            experienceService.createNewExperienceLevel(dto);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionFindById(Long id, String exMsg) {
        try {
            experienceService.findExperienceById(id);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionUpdate(ExperienceDto dto, String exMsg) {
        try {
            experienceService.update(dto);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

}
