package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.database.UserRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.*;
import de.unipassau.ep.team3.EP.services.ScheduleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EpApplication.class)
public class UserControllerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserController userController;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private AimController aimController;

    @Autowired
    private ExperienceController experienceController;

    /***
     * A test to check if a user is created correctly.
     */
    @Test
    public void testCreateUser() {
        UserDto dto = new UserDto("zim1", "foo", "12345678", 1, 1, 1980);
        userController.createUser(dto);
        User user = userRepository.findByZim("zim1");
        assertNotNull(user);
        assertEquals(dto.getZim(), user.getZim());
        assertEquals(dto.getName(), user.getName());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        assertTrue(encoder.matches("12345678", user.getHash()));
        LocalDate date = LocalDate.of(dto.getYearOfBirth(), dto.getMonthOfBirth(), dto.getDayOfBirth());
        assertEquals(date, user.getBirthday());
    }

    /**
     * A test to check if the user status is loaded correctly.
     */
    @Test
    public void testGetStatus() {
        String status = userController.getUserStatus("herman26");
        assertEquals(UserStatus.TRAINER.toString(),status);
    }

    /**
     * A test to check if the user status is changed correctly.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void  testUpdateUserStatus(){
        StatusDto dto = new StatusDto("haan01",1);
        userController.updateUserStatus(dto);
        assertEquals(UserStatus.USER.toString(),userController.getUserStatus("haan01"));
    }

    /**
     * A test to check if a user is blocked correctly.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testBlockUser(){
        userController.blockUser("winter57");
        assertEquals(UserStatus.UNKNOWN.toString(),userController.getUserStatus("winter57"));
    }

    /**
     * Test if the correct user is returned.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testGetUser(){
       UserDto dto = userController.getUser("winter57");
       assertEquals("winter57",dto.getZim());
       assertEquals("Marco",dto.getName());
    }

    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testChangePassword(){
        UserDto dto = new UserDto();
        userController.changePassword("winter57","12345679");
        User user = userRepository.findByZim("winter57");
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        assertTrue(encoder.matches("12345679", user.getHash()));
    }


    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testFirstLogin(){
        AimDto aim = new AimDto("weightloss");
        aim.setSuperAimId(0L);
        aimController.createAim(aim);

        experienceController.createNewExperience(new ExperienceDto("Pro"));

        ScheduleDto schedule = new ScheduleDto(1L,1L,"MALE");
        String [] execution = {"high"};
        schedule.setExecutions(execution);
        scheduleService.createNewSchedule(schedule);
        UserDto dto = new UserDto();
        dto.setZim("herman26");
        dto.setAimId(1L);
        dto.setExperienceId(1L);
        dto.setGender("MALE");
        dto.setHeight(180);
        dto.setWeight(70);
        userController.firstLogin(dto);

        User user = userRepository.findByZim("herman26");
        assertNotNull(user);
        assertEquals(1L,user.getAim().getId(),0);
        assertEquals(1L,user.getExperience().getId(),0);
        assertEquals(2L,user.getSchedule().getId(),0);
        assertEquals(70,user.getWeight());
        assertEquals(180,user.getHeight());

    }

    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testGetAim(){
        assertEquals(1L,userController.getAim("herman26"),0);
    }

    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testGetSchedule(){
        assertEquals(2L,userController.getSchedule("herman26"),0);
    }












}