package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.config.WebSecurityConfig;
import de.unipassau.ep.team3.EP.config.Webconfig;
import de.unipassau.ep.team3.EP.database.ScheduleRepository;
import de.unipassau.ep.team3.EP.database.UserRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes ={ EpApplication.class, Webconfig.class, WebSecurityConfig.class})
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private AimService aimService;
    @Autowired
    private ExperienceService experienceService;
    @Autowired
    private ExerciseService exerciseService;
    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private UserRepository userRepository;

    private User user;
    private Schedule schedule;

    @Before
    public void setUp() {
        user = Mockito.mock(User.class);

        schedule = Mockito.mock(Schedule.class);
        when(schedule.getId()).thenReturn(1L);
        when(user.getSchedule()).thenReturn(schedule);
    }


    /**
     * A test to check if the right exception is thrown if the zim is missing.
     */
    @Test
    public void testZimIsMissing() {
        UserDto dto = new UserDto(null, "foo", "12345678", 1, 1, 1980);
        testBadRequestException(dto, BadRequestReason.ZIM_MISSING.toString());
    }

    /**
     * A test to check if the right exception is thrown if the zim is invalid.
     */
    @Test
    public void testZimIsInvalid() {
        UserDto dto = new UserDto("", "foo", "12345678", 1, 1, 1980);
        testBadRequestException(dto, BadRequestReason.INVALID_ZIM.toString());
    }

    /**
     * A test to check if the right exception is thrown if the name is missing.
     */
    @Test
    public void testNameIsMissing() {
        UserDto dto = new UserDto("bla", null, "12345678", 1, 1, 1980);
        testBadRequestException(dto, BadRequestReason.NAME_MISSING.toString());
    }

    /**
     * A test to check if the right exception is thrown if the name is invalid.
     */
    @Test
    public void testNameIsInvalid() {
        UserDto dto = new UserDto("bla", "", "12345678", 1, 1, 1980);
        testBadRequestException(dto, BadRequestReason.INVALID_NAME.toString());
    }

    /**
     * A test to check if the right exception is thrown if the user is too old.
     */
    @Test
    public void testUserIsTooOld() {
        UserDto dto = new UserDto("bla", "foo", "12345678", 1, 1, 1900);
        testBadRequestException(dto, BadRequestReason.TOO_OLD.toString());
    }

    /**
     * A test to check if the right exception is thrown if the user is too young.
     */
    @Test
    public void testUserIsTooYoung() {
        UserDto dto = new UserDto("bla", "foo", "12345678", 1, 1, 2010);
        testBadRequestException(dto, BadRequestReason.TOO_YOUNG.toString());
    }

    /**
     * A test to check if the right exception is thrown if the pasword is too short.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER",password = "12345678")
    public void testPasswordTooShort() {
        UserDto dto = new UserDto("bla", "foo", "12678", 1, 1, 1996);
        testBadRequestException(dto, BadRequestReason.PASSWORD_TOO_SHORT.toString());
    }

    /**
     * A test to check if the right exception is thrown if the password is missing.
     */
    @Test
    public void testPasswordIsMissing() {
        UserDto dto = new UserDto("bla", "foo", null, 1, 1, 1996);
        testBadRequestException(dto, BadRequestReason.PASSWORD_MISSING.toString());
    }

    /**
     * A test to check if the right exception is thrown if the day is invalid.
     */
    @Test
    public void testDayIsInvalid() {
        UserDto dto = new UserDto("bla", "foo", "12678", 0, 1, 1996);
        testBadRequestException(dto, BadRequestReason.INVALID_DATE.toString());
    }

    /**
     * A test to check if the right exception is thrown if the year is negative.
     */
    @Test
    public void testNegativeYear() {
        UserDto dto = new UserDto("bla", "foo", "12678", 1, 1, -1);
        testBadRequestException(dto, BadRequestReason.TOO_OLD.toString());
    }

    /**
     * A test to check if the right user status is returned if the user doesn't exist.
     */
    @Test
    public void testStatusUserNotExisting(){
        assertEquals(UserStatus.UNKNOWN.toString(),userService.getUserStatus("muell03"));
    }

    /**
     * A test to check if a user is verified correctly and if the right exection is thrown otherwise.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testVerifyUser(){
        //User verify correctly
        UserDto dto = new UserDto("zim1", "foo", "12345678", 1, 1, 1980);
        userService.createNewUser(dto);
        User user = scheduleService.findUserByZim("zim1");
        assertNotNull(user);
        user.setVerificationToken("1234");
        userRepository.save(user);
        userService.verifyUser("1234");
        assertEquals(UserStatus.USER,scheduleService.findUserByZim("zim1").getStatus());
        //User already verified
        testBadRequestExceptionVerifyUser("1234",BadRequestReason.NOT_EXISTING_OR_ALREADY_VERIFIED.toString());
    }

    /**
     * A test to check if the right training schedule id is returned, null if the user has no schedule.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testGetScheduleId(){

        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");

        String [] rank = {"A1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {0};
        scheduleDto.setRankIndexes(rankIndexes);

        String [] executions = {"high"};
        scheduleDto.setExecutions(executions);

        long [] exerciseIds = {2};
        scheduleDto.setExerciseIds(exerciseIds);

        String [] speed = {"high"};
        int [] pause = {10};
        String [][] iteration = {{"15"}};
        String [][] load = {{"15 RM"}};
        ExerciseWrapperDto  wrapperDto = new ExerciseWrapperDto(speed,pause,iteration,load);
        ExerciseWrapperDto [] wrappersDto = {wrapperDto};
        scheduleDto.setWrapper(wrappersDto);


        long [] alternativeExerciseIds = {1};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);
        Long id = scheduleService.createNewSchedule(scheduleDto);
        schedule = scheduleRepository.findByTrainingScheduleId(id);
        //User has no schedule
        assertNull(userService.getScheduleId("ludwig22"));
       //User has schedule
        User user = scheduleService.findUserByZim("haan01");
        user.setSchedule(schedule);
        userRepository.save(user);
        assertEquals(3L,userService.getScheduleId("haan01"),0);
    }

    /**
     * A test to check if the right aim id is returned, null if the user has no schedule.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testGetAimId(){
        // User has no aim
        assertNull(userService.getAimId("herman26"));
        //User has aim
        AimDto aimDto = new AimDto("Belly");
        aimDto.setSuperAimId(0L);
        aimService.createNewAim(aimDto);
        Aim aim = aimService.findAimById(1L);
        User user = scheduleService.findUserByZim("haan01");
        user.setAim(aim);
        userRepository.save(user);
        assertEquals(1L,userService.getAimId("haan01"),0);
    }

    /**
     * A test to check if at the first login the right exception are thrown
     * if something is wrong or missing in the dto.
     */
    @Test
    @WithMockUser(username = "winter57",roles = "TRAINER",password = "12345678")
    public void testFirstLogin(){
        UserDto dto = new UserDto();
        dto.setZim("winter57");
        testBadRequestExceptionFirstLogin(dto,BadRequestReason.AIM_IS_MISSING.toString());
        dto.setAimId(1L);
        testBadRequestExceptionFirstLogin(dto,BadRequestReason.EXPERIENCE_IS_MISSING.toString());
        experienceService.createNewExperienceLevel(new ExperienceDto("Super"));
        dto.setExperienceId(1L);
        testBadRequestExceptionFirstLogin(dto,BadRequestReason.GENDER_IS_MISSING.toString());
        dto.setGender("MALE");
        dto.setHeight(-1);
        testBadRequestExceptionFirstLogin(dto,BadRequestReason.INVALID_HEIGHT.toString());
        dto.setHeight(170);
        dto.setWeight(-1);
        testBadRequestExceptionFirstLogin(dto,BadRequestReason.INVALID_WEIGHT.toString());

    }

    /**
     * A test to check if the right schedule is selected and if the right
     * exceptions are thrown
     */
    @Test
    @WithMockUser(username = "winter57",roles = "TRAINER",password = "12345678")
    public void testSelectSchedule(){
        //Aim is missing
        UserDto dto = new UserDto();
        dto.setZim("herman26");
        testBadRequestExceptionSelectSchedule(dto,BadRequestReason.AIM_IS_MISSING.toString());

        AimDto aimDto = new AimDto("weightloss");
        aimDto.setSuperAimId(0L);
        aimService.createNewAim(aimDto);

        experienceService.createNewExperienceLevel(new ExperienceDto("Pro"));

        ExerciseDto exerciseDto = new ExerciseDto("Sit-up");
        long [] alternatives = {1};
        exerciseDto.setAlternativeIds(alternatives);
        exerciseService.createNewExercise(exerciseDto);

        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");

        String [] rank = {"A1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {0};
        scheduleDto.setRankIndexes(rankIndexes);

        String [] executions = {"high"};
        scheduleDto.setExecutions(executions);

        long [] exerciseIds = {2};
        scheduleDto.setExerciseIds(exerciseIds);

        String [] speed = {"high"};
        int [] pause = {10};
        String [][] iteration = {{"15"}};
        String [][] load = {{"15 RM"}};
        ExerciseWrapperDto  wrapperDto = new ExerciseWrapperDto(speed,pause,iteration,load);
        ExerciseWrapperDto [] wrappersDto = {wrapperDto};
        scheduleDto.setWrapper(wrappersDto);


        long [] alternativeExerciseIds = {1};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);
        Long id = scheduleService.createNewSchedule(scheduleDto);
        Schedule schedule = scheduleRepository.findByTrainingScheduleId(1L);
        assertNotNull(schedule);
        User user = scheduleService.findUserByZim("herman26");
        user.setSchedule(schedule);
        userRepository.save(user);

        dto.setAimId(1L);
        testBadRequestExceptionSelectSchedule(dto, BadRequestReason.HAS_SCHEDULE.toString());
    }

    private void testBadRequestException(UserDto dto, String exMsg) {
        try {
            userService.createNewUser(dto);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionVerifyUser(String token, String exMsg) {
        try {
            userService.verifyUser(token);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionFirstLogin(UserDto dto, String exMsg) {
        try {
            userService.firstLogin(dto);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionFindSuitableSchedule(Aim aim, Experience level,Gender gender, String exMsg) {
        try {
            Method method = UserService.class.getDeclaredMethod("findSuitableSchedule", Aim.class, Experience.class, Gender.class);
            method.setAccessible(true);
            method.invoke(userService,aim,level,gender);
            fail();
        } catch (InvocationTargetException e) {
            assertEquals(exMsg, e.getCause().getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionSelectSchedule(UserDto dto, String exMsg) {
        try {
            userService.selectSchedule(dto);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }



}