package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.config.WebSecurityConfig;
import de.unipassau.ep.team3.EP.config.Webconfig;
import de.unipassau.ep.team3.EP.database.*;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.*;
import de.unipassau.ep.team3.EP.services.ScheduleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes ={ EpApplication.class, Webconfig.class, WebSecurityConfig.class})
@ContextConfiguration
@WebAppConfiguration
public class ScheduleControllerTest {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private ScheduleController scheduleController;

    @Autowired
    private AimRepository aimRepository;

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Autowired
    private ExperienceRepository experienceRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private AimController aimController;

    @Autowired
    private ExperienceController experienceController;

    @Autowired
    private ExerciseController exerciseController;



    /**
     * A test to check if the aim in the database, has the same Attributes as its dto.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testCreateNewSchedule() {
        AimDto aimDto = new AimDto("weightloss");
        aimDto.setSuperAimId(0L);
        aimController.createAim(aimDto);

        experienceController.createNewExperience(new ExperienceDto("Pro"));

        ExerciseDto exerciseDto = new ExerciseDto("Sit-up");
        long [] alternatives = {1};
        exerciseDto.setAlternativeIds(alternatives);
        exerciseController.createNewExercise(exerciseDto);

        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");

        String [] rank = {"A1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {0};
        scheduleDto.setRankIndexes(rankIndexes);

        String [] executions = {"high"};
        scheduleDto.setExecutions(executions);

        long [] exerciseIds = {2};
        scheduleDto.setExerciseIds(exerciseIds);

        String [] speed = {"high","low"};
        int [] pause = {10};
        String [][] iteration = {{"15"}};
        String [][] load = {{"15 RM"}};
        ExerciseWrapperDto  wrapperDto = new ExerciseWrapperDto(speed,pause,iteration,load);
        ExerciseWrapperDto [] wrappersDto = {wrapperDto};
        scheduleDto.setWrapper(wrappersDto);


        long [] alternativeExerciseIds = {1};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);

        Long id = scheduleController.createSchedule(scheduleDto);
        Schedule schedule = scheduleRepository.findByTrainingScheduleId(id);
        schedule.setAimName("weightloss");
        scheduleRepository.save(schedule);
        assertNotNull(scheduleDto);
        assertNotNull(schedule);
        long ID = schedule.getAim().getId();
        assertEquals(scheduleDto.getAimId(),ID);
        ID = schedule.getLevel().getId();
        assertEquals(scheduleDto.getLevelId(), ID);
        assertEquals(scheduleDto.getGender(), schedule.getGender().toString());
        assertEquals("herman26",schedule.getEditor().getZim());
        assertEquals(scheduleDto.getRank()[0],schedule.getRank().get(0));
        assertEquals((Integer) scheduleDto.getRankIndexes()[0],schedule.getRankIndexes().get(0));
        assertEquals(scheduleDto.getExecutions()[0],schedule.getExecutions().get(0));

        //Testing if name and id equals
        Exercise findExercise = exerciseRepository.findByExerciseId(2);
        assertEquals(schedule.getExercises().get(0).getId(),findExercise.getId());
        assertEquals(schedule.getExercises().get(0).getName(),findExercise.getName());

        //Testing the wrapper
        ExerciseWrapper wrapperDtoToObject = DtoConverter.convertFromDto(scheduleDto.getWrapper()[0]);
        ExerciseWrapper wrapperOfSchedule = schedule.getWrappers().get(0);
        assertArrayEquals(wrapperOfSchedule.getSpeed(),wrapperDtoToObject.getSpeed());
        assertArrayEquals(wrapperOfSchedule.getPause(),wrapperDtoToObject.getPause());
        assertArrayEquals(wrapperOfSchedule.getIteration()[0],wrapperDtoToObject.getIteration()[0]);
        assertArrayEquals(wrapperOfSchedule.getLoad()[0],wrapperDtoToObject.getLoad()[0]);

        //Testing if name and id of the alternative exercise equals
        findExercise = exerciseRepository.findByExerciseId(1);
        assertEquals(schedule.getAlternativeExercises().get(0).getId(),findExercise.getId());
        assertEquals(schedule.getAlternativeExercises().get(0).getName(),findExercise.getName());

    }

    /**
     * A test to check if only all trainings schedules without an owner loaded from the database.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testGetAllUnassigned(){
        Aim aim = aimRepository.findByAimId(1);
        Experience experience = experienceRepository.findByExperienceId(1);
        User owner = userRepository.findByZim("herman26");

        Schedule schedule = new Schedule(aim,experience,Gender.MALE);
        Schedule schedule1 = new Schedule(aim,experience,Gender.MALE);
        schedule.setUser(owner);
        schedule.setAimName("Belly");
        scheduleRepository.save(schedule);
        scheduleRepository.save(schedule1);
        String outputString = scheduleController.getAllUnassigned();
        String toCompare = "[{\"id\":\"2\",\"aimName\":\"No aim\",\"experienceName\":\"No experience level\",\"gender\":\"MALE\"}]";
        assertEquals(toCompare,outputString);
    }

    /**
     * A test to check if a schedule and its wrapper are deleted correctly.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testDelete(){

        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");

        String [] rank = {"A1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {0};
        scheduleDto.setRankIndexes(rankIndexes);

        String [] executions = {"high"};
        scheduleDto.setExecutions(executions);

        long [] exerciseIds = {2};
        scheduleDto.setExerciseIds(exerciseIds);

        String [] speed = {"high"};
        int [] pause = {10};
        String [][] iteration = {{"15"}};
        String [][] load = {{"15 RM"}};
        ExerciseWrapperDto  wrapperDto = new ExerciseWrapperDto(speed,pause,iteration,load);
        ExerciseWrapperDto [] wrappersDto = {wrapperDto};
        scheduleDto.setWrapper(wrappersDto);

        String [] alternativeExecution = {"low"};

        long [] alternativeExerciseIds = {1};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);

        String [] altSpeed = {"low"};
        int [] altPause = {15};
        String [][] altIteration = {{"10"}};
        String [][] altLoad = {{"16 RM"}};
        ExerciseWrapperDto  altWrapperDto = new ExerciseWrapperDto(altSpeed,altPause,altIteration,altLoad);

        Long id = scheduleController.createSchedule(scheduleDto);
        ScheduleDto s =scheduleController.getById((long)1);
        scheduleController.delete((long)3);
        testBadRequestException((long) 3, BadRequestReason.SCHEDULE_NOT_EXISTING.toString());
        testBadRequestExceptionWrapper((long)1,BadRequestReason.WRAPPER_NOT_EXISTING.toString());
    }

    /**
     * A test to check if an schedule is updated correctly.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testUpdate(){
        ExerciseDto exerciseDto = new ExerciseDto("Push-up");
        long [] alternatives = {1};
        exerciseDto.setAlternativeIds(alternatives);
        exerciseController.createNewExercise(exerciseDto);

        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");

        String [] rank = {"B1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {0};
        scheduleDto.setRankIndexes(rankIndexes);

        String [] executions = {"low"};
        scheduleDto.setExecutions(executions);

        long [] exerciseIds = {3};
        scheduleDto.setExerciseIds(exerciseIds);

        String [] speed = {"low"};
        int [] pause = {20};
        String [][] iteration = {{"10"}};
        String [][] load = {{"10 RM"}};
        ExerciseWrapperDto  wrapperDto = new ExerciseWrapperDto(speed,pause,iteration,load);
        ExerciseWrapperDto [] wrappersDto = {wrapperDto};
        scheduleDto.setWrapper(wrappersDto);

        long [] alternativeExerciseIds = {2};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);

        scheduleDto.setId((long)2);

        scheduleController.update(scheduleDto);
        Schedule schedule = scheduleRepository.findByTrainingScheduleId(scheduleDto.getId());
        assertNotNull(scheduleDto);
        assertNotNull(schedule);
        long ID = schedule.getAim().getId();
        assertEquals(scheduleDto.getAimId(),ID);
        ID = schedule.getLevel().getId();
        assertEquals(scheduleDto.getLevelId(), ID);
        assertEquals(scheduleDto.getGender(), schedule.getGender().toString());
        assertEquals("herman26",schedule.getEditor().getZim());
        assertEquals(scheduleDto.getRank()[0],schedule.getRank().get(0));
        assertEquals((Integer) scheduleDto.getRankIndexes()[0],schedule.getRankIndexes().get(0));
        assertEquals(scheduleDto.getExecutions()[0],schedule.getExecutions().get(0));

        //Testing if name and id equals
        Exercise findExercise = exerciseRepository.findByExerciseId(3);
        assertEquals(schedule.getExercises().get(0).getId(),findExercise.getId());
        assertEquals(schedule.getExercises().get(0).getName(),findExercise.getName());

        //Testing the wrapper
        ExerciseWrapper wrapperDtoToObject = DtoConverter.convertFromDto(scheduleDto.getWrapper()[0]);
        ExerciseWrapper wrapperOfSchedule = schedule.getWrappers().get(0);
        assertArrayEquals(wrapperOfSchedule.getSpeed(),wrapperDtoToObject.getSpeed());
        assertArrayEquals(wrapperOfSchedule.getPause(),wrapperDtoToObject.getPause());
        assertArrayEquals(wrapperOfSchedule.getIteration()[0],wrapperDtoToObject.getIteration()[0]);
        assertArrayEquals(wrapperOfSchedule.getLoad()[0],wrapperDtoToObject.getLoad()[0]);

        //Testing if name and id of the alternative exercise equals
        findExercise = exerciseRepository.findByExerciseId(2);
        assertEquals(schedule.getAlternativeExercises().get(0).getId(),findExercise.getId());
        assertEquals(schedule.getAlternativeExercises().get(0).getName(),findExercise.getName());
    }

    /**
     * A test to check if the right name of an schedule is loaded.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testGetAim(){
        assertEquals("Belly",scheduleController.getAim(1L));
    }

    /**
     * A test to check if the progress is loaded correctly.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testGetProgress(){
        assertEquals("{\"currentUnit\":\"0\",\"maxUnit\":\"2\"}", scheduleController.getProgress(3L));

    }

    /**
     * A test to check if the right schedule was loaded from the database
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testGetById(){
        ScheduleDto dto = scheduleController.getById(3L);
        assertNotNull(dto);
        assertEquals(1L,dto.getAimId());
        assertEquals("MALE",dto.getGender());
        assertEquals("herman26",dto.getEditorZim());
        assertEquals("weightloss",dto.getAimName());
        assertNull(dto.getExperienceName());
        String [] rank = {"A1"};
        assertArrayEquals(rank,dto.getRank());
        int [] rankIndexes = {0};
        assertArrayEquals(rankIndexes,dto.getRankIndexes());
        String [] executions = {"high"};
        assertArrayEquals(executions,dto.getExecutions());



    }








    private void testBadRequestException(Long id, String exMsg) {
        try {
            scheduleController.delete(id);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionWrapper(Long id, String exMsg) {
        try {
            scheduleService.findWrapperById(id);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }









}