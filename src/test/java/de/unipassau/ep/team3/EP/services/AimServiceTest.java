package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.config.WebSecurityConfig;
import de.unipassau.ep.team3.EP.config.Webconfig;
import de.unipassau.ep.team3.EP.database.AimRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.Aim;
import de.unipassau.ep.team3.EP.model.AimDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes ={ EpApplication.class, Webconfig.class, WebSecurityConfig.class})
public class AimServiceTest {

    @Autowired
    private AimRepository aimRepository;

    @Autowired
    private AimService aimService;

    /**
     * A test to check if the name is missing, the aim can't be created for the right reason.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testCreateNewAimNameMissing(){
        AimDto aim = new AimDto();
        testBadRequestExceptionCreateAim(aim,BadRequestReason.NAME_MISSING.toString());
    }

    /**
     * A test to check if the name is invalid, the aim can't be created for the right reason.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testCreateNewAimInvalidName(){
        AimDto aim = new AimDto("");
        testBadRequestExceptionCreateAim(aim,BadRequestReason.INVALID_NAME.toString());
    }

    /**
     * A test to check if the super aim id is invalid, the aim can't be created for the right reason.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testCreateNewAimInvalidSuperAim(){
        AimDto aim = new AimDto("Body");
        aim.setSuperAimId(-1L);
        testBadRequestExceptionCreateAim(aim,BadRequestReason.INVALID_SUPER_AIM_ID.toString());
    }

    /**
     * Tests if an aim already exists, case: aim exist.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void alreadyExists() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = AimService.class.getDeclaredMethod("aimAlreadyExists", String.class, Long.class);
        method.setAccessible(true);
        AimDto aim  = new AimDto("weightloss1");
        aim.setSuperAimId((long)0);
        aim.setDescription("lorem ipsum");
        aim.setFurtherInformation("dolor sit amet");
        assertEquals(false,method.invoke(aimService,aim.getName(),aim.getSuperAimId()) );}


    /**
     * Tests if the last super aim in the database can be deleted.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testLastAim(){
        testBadRequestException(1L, BadRequestReason.LAST_AIM.toString());
    }

    /**
     * Tests if the right aim is loaded from the database or
     * an exception is thrown if the aim doesn't exist.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testFindAimById(){
        //Aim not existing
        testBadRequestException(2L,BadRequestReason.AIM_NOT_EXISTING.toString());
        //Aim existing
        AimDto dto = new AimDto("Sprint");
        dto.setSuperAimId(0L);
        dto.setDescription("Lorem ipsum");
        aimService.createNewAim(dto);
        Aim aim = aimRepository.findByAimId(1L);
        Aim aim1 = new Aim("Sprint");
        aim1.setId(1L);
        assertNotNull(aim);
        assertNotNull(aim1);
        assertEquals(aim1.getId(),aim.getId());
        assertEquals(aim1.getName(),aim1.getName());
    }

    /**
     * A test to check if the right exceptions are thrown if
     * name is missing or invalid.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testUpdate(){

        AimDto dto1 = new AimDto();
        dto1.setId(1L);
        testBadRequestExceptionUpdate(dto1,BadRequestReason.NAME_MISSING.toString());
        dto1.setName("");
        testBadRequestExceptionUpdate(dto1,BadRequestReason.INVALID_NAME.toString());
    }



    private void testBadRequestException(long id, String exMsg) {
        try {
            aimService.delete(id);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }

    private void testBadRequestExceptionCreateAim(AimDto dto, String exMsg) {
        try {
            aimService.createNewAim(dto);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }



    private void testBadRequestExceptionUpdate(AimDto dto, String exMsg) {
        try {
            aimService.update(dto);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            fail();
        }
    }



}