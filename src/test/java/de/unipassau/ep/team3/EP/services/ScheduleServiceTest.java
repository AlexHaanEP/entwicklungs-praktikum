package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.config.WebSecurityConfig;
import de.unipassau.ep.team3.EP.config.Webconfig;
import de.unipassau.ep.team3.EP.database.AimRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(classes ={ EpApplication.class, Webconfig.class, WebSecurityConfig.class})
public class ScheduleServiceTest {

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private AimService aimService;

    @Autowired
    private  ExperienceService experienceService;

    @Autowired
    private ExerciseService exerciseService;

    /**
     * A test to check if the right exception is thrown if the aim is not existing.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testAimNotExisting () {
        ScheduleDto scheduleDto = new ScheduleDto(0,1,"MALE");
        testBadRequestException(scheduleDto,BadRequestReason.AIM_NOT_EXISTING.toString());
    }

    /**
     * A test to check if the right exception is thrown if the experience level is not existing.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testExperienceLevelNotExisting () {
        ScheduleDto scheduleDto = new ScheduleDto(1,0,"MALE");
        testBadRequestException(scheduleDto,BadRequestReason.EXPERIENCE_LEVEL_NOT_EXISTING.toString());
    }

    /**
     * A test to check if the right exception is thrown if the gender is invalid.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testInvalidGender () {
        ScheduleDto scheduleDto = new ScheduleDto(1,1,"APACHE_HELICOPTER");
        testBadRequestException(scheduleDto,BadRequestReason.INVALID_GENDER.toString());
    }

    /**
     * A test to check if the right exception is thrown if the exercise is not existing.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testExerciseNotExisting () {
        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");
        String [] rank = {"A1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {0};
        scheduleDto.setRankIndexes(rankIndexes);

        String [] executions = {"high"};
        scheduleDto.setExecutions(executions);

        String [] speed = {"high"};
        int [] pause = {10};
        String [][] iteration = {{"15"}};
        String [][] load = {{"15 RM"}};
        ExerciseWrapperDto  wrapperDto = new ExerciseWrapperDto(speed,pause,iteration,load);
        ExerciseWrapperDto [] wrappersDto = {wrapperDto};
        scheduleDto.setWrapper(wrappersDto);
        long [] exerciseIds = {1000};
        scheduleDto.setExerciseIds(exerciseIds);
        long [] alternativeExerciseIds = {3};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);
        testBadRequestException(scheduleDto,BadRequestReason.EXERCISE_NOT_EXISTING.toString());
    }

    /**
     * A test to check if the right exception is thrown if the rank is invalid.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testInvalidRankFormat () {
        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");
        String[] rank = {"1"};
        scheduleDto.setRank(rank);
        testBadRequestException(scheduleDto,BadRequestReason.INVALID_RANK_FORMAT.toString());
    }

    /**
     * A test to check if the right exception is thrown if the rank index is too high.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testRankIndexTooHigh() {
        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");
        String [] rank = {"A1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {5000};
        scheduleDto.setRankIndexes(rankIndexes);
        testBadRequestException(scheduleDto,BadRequestReason.RANK_TOO_HIGH.toString());
    }

    /**
     * A test to check if the right exception is thrown if the rank index is to low.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testRankIndexTooLow() {
        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");
        int[] rankIndexes = {-1};
        scheduleDto.setRankIndexes(rankIndexes);
    testBadRequestException(scheduleDto,BadRequestReason.RANK_TOO_LOW.toString());}

    /**
     * A test to check if the right exception is thrown if the number of exercises
     * and the number of alternative exercises are unequal.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testUnequalExercisesAlternativesCount(){
        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");
    String [] rank = {"A1"};
    scheduleDto.setRank(rank);

    int [] rankIndexes = {0};
    scheduleDto.setRankIndexes(rankIndexes);

    String [] executions = {"high"};
    scheduleDto.setExecutions(executions);
        long[] exerciseIds = {2,3};
        scheduleDto.setExerciseIds(exerciseIds);
        long[] alternativeExerciseIds = {2};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);
        testBadRequestException(scheduleDto, BadRequestReason.UNEQUAL_NUMBER_EXERCISES_ALTERNATIVES.toString());
    }

    /**
     * A test to check if the right exception is thrown if the alternative exercise is the own exercise.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testAlternativeExerciseIdOwnId(){
        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");
        String [] rank = {"A1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {0};
        scheduleDto.setRankIndexes(rankIndexes);

        String [] executions = {"high"};
        scheduleDto.setExecutions(executions);

        long[] exerciseIds = {2,3};
        scheduleDto.setExerciseIds(exerciseIds);
        long[] alternativeExerciseIds = {2,2};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);
        testBadRequestException(scheduleDto, BadRequestReason.ALTERNATIVE_EXERCISE_OWN_EXERCISE.toString());
    }

    /**
     * A test to check if the right exception is thrown if the exercise is the dummy exercise.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testExerciseIdIsDummyExerciseId(){
        AimDto aimDto = new AimDto("Belly");
        aimDto.setSuperAimId(0L);
        aimService.createNewAim(aimDto);
        experienceService.createNewExperienceLevel(new ExperienceDto("Pro"));
        ExerciseDto exerciseDto = new ExerciseDto("Jump");
        ExerciseDto exerciseDto1 = new ExerciseDto("Hop");
        long [] alt = {1};
        exerciseDto.setAlternativeIds(alt);
        exerciseService.createNewExercise(exerciseDto);
        exerciseDto1.setAlternativeIds(alt);
        exerciseService.createNewExercise(exerciseDto1);

        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");
        String [] rank = {"A1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {0};
        scheduleDto.setRankIndexes(rankIndexes);

        String [] executions = {"high"};
        scheduleDto.setExecutions(executions);

        String [] speed = {"high"};
        int [] pause = {10};
        String [][] iteration = {{"15"}};
        String [][] load = {{"15 RM"}};
        ExerciseWrapperDto  wrapperDto = new ExerciseWrapperDto(speed,pause,iteration,load);
        ExerciseWrapperDto [] wrappersDto = {wrapperDto};
        scheduleDto.setWrapper(wrappersDto);

        long[] exerciseIds = {1};
        scheduleDto.setExerciseIds(exerciseIds);
        long[] alternativeExerciseIds = {2};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);
        testBadRequestException(scheduleDto, BadRequestReason.DUMMY_EXERCISE_ID.toString());
    }

    /**
     * A test to check if the right exception is thrown if the exercise id is invalid.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testInvalidExerciseId(){
        ScheduleDto scheduleDto = new ScheduleDto(1,1,"MALE");
        String [] rank = {"A1"};
        scheduleDto.setRank(rank);

        int [] rankIndexes = {0};
        scheduleDto.setRankIndexes(rankIndexes);

        String [] executions = {"high"};
        scheduleDto.setExecutions(executions);
        long[] exerciseIds = {-1};
        scheduleDto.setExerciseIds(exerciseIds);
        long[] alternativeExerciseIds = {2};
        scheduleDto.setAlternativeExerciseIds(alternativeExerciseIds);
        testBadRequestException(scheduleDto, BadRequestReason.INVALID_EXERCISE_ID.toString());
    }


    private void testBadRequestException(ScheduleDto dto, String exMsg) {
        try {
            scheduleService.createNewSchedule(dto);
            fail();
        } catch (BadRequestException e) {
            assertEquals(exMsg, e.getMessage());
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
    }
}
