package de.unipassau.ep.team3.EP.config.test.config;

import de.unipassau.ep.team3.EP.config.EpApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EpApplication.class)
@ComponentScan(basePackages = {"de.unipassau.ep.team3.EP.model", "de.unipassau.ep.team3.EP.database",
		"de.unipassau.ep.team3.EP.controller", "de.unipassau.ep.team3.EP.services", "de.unipassau.ep.team3.EP.config"})
@EntityScan("de.unipassau.ep.team3.EP.model")
public class EpApplicationTests {

	@Test
	public void contextLoads() {
	}

}
