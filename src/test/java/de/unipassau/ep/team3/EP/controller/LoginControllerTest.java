package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.model.UserDto;
import de.unipassau.ep.team3.EP.services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EpApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LoginControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Autowired
    private UserService userService;

    /**
     * Tests if a user could be logged in successfully.
     *
     */
    @Test
    public void testLoginExistingUser() {

        String username = "login";
        String pw = "12345678";

        UserDto user = new UserDto(username, username, pw, 1,1,1980);
        userService.createNewUser(user);

        // configure credentials for user created by the initializer.
        ResponseEntity<String> result = template.withBasicAuth(username, pw)
                .getForEntity("/login", String.class);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    /**
     * Tests if a user could be logged in if password is wrong or an user doesn't exist.
     */
    @Test
    public void testLoginWrongCredentials() {
        ResponseEntity<String> result2 = template
                .withBasicAuth("wrongname", "wrongpassword")
                .getForEntity("/login", String.class);
        assertEquals(HttpStatus.UNAUTHORIZED, result2.getStatusCode());
    }

    /**
     * Tests if a user could be logged in without username and password.
     */
    @Test
    public void testLoginExistingUserWithoutCredentials() {
        ResponseEntity<String> result3 = template.getForEntity("/login", String.class);
        assertEquals(HttpStatus.UNAUTHORIZED, result3.getStatusCode());
    }
}