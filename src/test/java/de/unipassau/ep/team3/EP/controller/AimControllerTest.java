package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.config.EpApplication;
import de.unipassau.ep.team3.EP.config.WebSecurityConfig;
import de.unipassau.ep.team3.EP.config.Webconfig;
import de.unipassau.ep.team3.EP.database.AimRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.Aim;
import de.unipassau.ep.team3.EP.model.AimDto;
import de.unipassau.ep.team3.EP.model.UserDto;
import de.unipassau.ep.team3.EP.services.AimService;
import org.assertj.core.util.IterableUtil;
import org.assertj.core.util.VisibleForTesting;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(classes ={ EpApplication.class, Webconfig.class, WebSecurityConfig.class})
@ContextConfiguration
@WebAppConfiguration
public class AimControllerTest {

    @Autowired
    private AimRepository aimRepository;

    @Autowired
    private AimService aimService;

    @Autowired
    private AimController aimController;


    /**
     * A test to check if the aim in the database, has the same Attributes as its dto.
     */
    @Test
    @WithMockUser(username = "herman26",roles = "TRAINER",password = "12345678")
    public void testCreateNewAim() {
        AimDto aimDto = new AimDto("stomach");
        aimDto.setSuperAimId((long)0);
        aimDto.setDescription("lorem ipsum");
        aimDto.setFurtherInformation("dolor sit amet");
        aimController.createAim(aimDto);
        Aim aim = aimRepository.findByAimId(1);
        assertNotNull(aim);
        assertNotNull(aimDto);
        assertEquals(aimDto.getName(), aim.getName());
        assertEquals(aimDto.getDescription(), aim.getDescription());
        assertEquals(aimDto.getFurtherInformation(), aim.getFurtherInformation());
    }

    /**
     * A test to check if the aim is updated correctly.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testUpdate(){
        AimDto toUpdate = new AimDto("Crossfit");
        toUpdate.setSuperAimId((long) 0);
        toUpdate.setDescription("lorem ");
        toUpdate.setFurtherInformation("ipsum");
        aimController.createAim(toUpdate);
        AimDto update = new AimDto("PleaseNoCrossfit");
        update.setId((long) 3);
        update.setDescription("uncool");
        update.setFurtherInformation("lorem ipsum");
        aimController.update(update);
        Aim updated = aimRepository.findByAimId(3);
        assertNotNull(toUpdate);
        assertNotNull(updated);
        assertEquals(updated.getId(),update.getId());
        assertEquals(updated.getDescription(),update.getDescription());
        assertEquals(updated.getFurtherInformation(),update.getFurtherInformation());
        assertEquals(updated.getEditor().getZim(),"herman26");

    }

    /**
     * A test to check if the description of an aim will be loaded correctly.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testGetDescription(){
        assertEquals("lorem ipsum",aimController.getDescriptions(1L));
    }

    /**
     * A test to check if all aims from the database are loaded.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testGetAll(){
        assertEquals(1,IterableUtil.sizeOf(aimController.getAll()));
    }

    /**
     * A test to check if the aim is really deleted.
     */
    @Test
    @WithMockUser(username = "herman26", roles = "TRAINER")
    public void testDelete(){
        AimDto subAim = new AimDto("Crossfit2");
        subAim.setSuperAimId((long) 0);
        subAim.setDescription("lorem ");
        subAim.setFurtherInformation("ipsum");
        aimController.createAim(subAim);
        aimController.delete(2L);
        testBadRequestException(2L, BadRequestReason.AIM_NOT_EXISTING.toString());
    }

    private void testBadRequestException(Long id, String exMsg) {
        try {
            Method method = AimService.class.getDeclaredMethod("findAimById", Long.class);
            method.setAccessible(true);
            method.invoke(aimService,id);
            fail();
        } catch (InvocationTargetException e) {
            assertEquals(exMsg, e.getCause().getMessage());
        } catch (Exception e) {
            fail();
        }
    }
}