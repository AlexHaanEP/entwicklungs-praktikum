package de.unipassau.ep.team3.EP.config.test.config.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EpApplicationTests {

	@Test
	public void contextLoads() {
	}

}
