package de.unipassau.ep.team3.EP.database;

import de.unipassau.ep.team3.EP.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule,Long> {

    /**
     * Gets an training schedule by its id.
     *
     * @param id The training schedule's id.
     * @return The corresponding training schedule or {@code null}.
     */
    @Query("SELECT s FROM Schedule AS s WHERE s.id = :id")
    Schedule findByTrainingScheduleId(@Param("id") long id);

    /**
     * Gets a list of exercises which ar not assigned.
     *
     * @return The corresponding exercises or {@code null}.
     */
    @Query("SELECT s FROM Schedule AS s WHERE s.user = null")
    LinkedList<Schedule> findUnassigned();

    /**
     * Gets all schedules which mathes the following parameter or {@code null}.
     *
     * @param aim The aim.
     * @param level The exeperience level.
     * @param gender The gender.
     * @return The list of suitable schedules.
     */
    @Query("SELECT s FROM Schedule AS s WHERE s.aim = :aim AND s.level = :level AND s.user = null AND s.unlocked = true AND (s.gender = :gender OR s.gender = 2)")
    LinkedList<Schedule> findSchedulesByParameter(@Param("aim") Aim aim, @Param("level") Experience level, @Param("gender") Gender gender);
}
