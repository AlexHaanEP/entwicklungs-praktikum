package de.unipassau.ep.team3.EP.model;


import javax.persistence.*;

/**
 * A model class representing an exercise wrapper of the online training plattform.
 */
@Entity
@Table(name="\"exerciseWrapper\"")
public class ExerciseWrapper {

    /**
     * The ID of the exercise wrapper.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The array of speed for the exercise.
     */
    private String [] speed;

    /**
     * The array of pause for the exercise.
     */
    private int [] pause;

    /**
     * The array of iteration for each set in each unit of the exercise.
     */
    private String [][] iteration;

    /**
     * The array of speed for each set in each unit of the exercise.
     */
    private String [][] load;

    @ManyToOne
    private Schedule schedule;

    /**
     * Creates an exercise wrapper.
     */
    public ExerciseWrapper () {}

    /**
     * Creates an exercise wrapper.
     *
     * @param speed The exercise's speeds.
     * @param pause The exercise's pauses.
     * @param iteration The exercise's iterations.
     * @param load The exercise's load.
     */
    public ExerciseWrapper( String [] speed,int [] pause,String [][] iteration,String [][]load){
        this.speed = speed;
        this.pause = pause;
        this.iteration = iteration;
        this.load = load;
    }

    //Getter & Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String[] getSpeed() {
        return speed;
    }

    public void setSpeed(String[] speed) {
        this.speed = speed;
    }

    public int[] getPause() {
        return pause;
    }

    public void setPause(int[] pause) {
        this.pause = pause;
    }

    public String[][] getIteration() {
        return iteration;
    }

    public void setIteration(String[][] iteration) {
        this.iteration = iteration;
    }

    public String[][] getLoad() {
        return load;
    }

    public void setLoad(String[][] load) {
        this.load = load;
    }


}
