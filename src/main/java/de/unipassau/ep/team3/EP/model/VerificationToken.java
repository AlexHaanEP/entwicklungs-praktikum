package de.unipassau.ep.team3.EP.model;

import java.util.UUID;

public class VerificationToken {

    /**
     * @return Returns a random generated String
     */
    public static String generateString() {
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.replace("-", "");
        return uuid;
    }
}
