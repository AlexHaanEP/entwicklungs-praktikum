package de.unipassau.ep.team3.EP.database;

import de.unipassau.ep.team3.EP.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;

/**
 * The database repository for user management.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

    /**
     * Finds a user with the given zim ID.
     *
     * @param zim The user's zim ID.
     * @return The user if existing or {@code null} otherwise.
     */
     User findByZim(String zim);

    /**
     * Finds the user that is linked to a verification token.
     *
     * @param token An unique verification token
     * @return The user assigned to the token or {@code null} if the verification token does not exist.
     */
    @Query("SELECT u FROM User AS u WHERE u.verificationToken = :token")
    User findVerificationToken(@Param("token") String token);

    /**
     * Gets all user from the database ordered by name.
     *
     * @return A linked list of all users.
     */
    @Query("SELECT u FROM User AS u ORDER BY u.name")
    LinkedList<User> getAllUser();
}
