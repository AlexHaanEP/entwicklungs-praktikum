package de.unipassau.ep.team3.EP.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * A model class representing an experience level of training of the online training plattform.
 */
@Entity
@Table(name="\"experiencelevel\"")
public class Experience {

    /**
     * The ID of the experience level.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The name of the experience level.
     */
    @NotNull
    private String name;

    /**
     * The description of the experience level.
     */
    @Column(length = 1000)
    private String description;

    /**
     * The user who edited the experience the latest.
     */

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, mappedBy = "level")
    private List<Schedule> schedules = new LinkedList<>();

    /**
     * The list of users assigned to the experience level.
     */
    @OneToMany(mappedBy = "experience")
    private List<User> users = new LinkedList<>();

    /**
     * The experience will be removed from the schedules, before it is deleted.
     */
    @PreRemove
    private void preRemove() {
        for (Schedule s : schedules) {
            s.setLevel(null);
            s.setUnlocked(false);
        }
        for(User u: users){
            u.setExperience(null);
        }
    }

    /**
     * The user who edited the experience the latest.
     */
    @ManyToOne
    private User editor;

    /**
     * Creates a new experience level.
     */
    Experience() {}

    /**
     * Creates a new experience level.
     */
    public Experience(String name){
        this.name = name;
    }

    //Getter & Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getEditor() {
        return editor;
    }

    public void setEditor(User editor) {
        this.editor = editor;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
