package de.unipassau.ep.team3.EP.controller;


import de.unipassau.ep.team3.EP.model.ExerciseDto;
import de.unipassau.ep.team3.EP.services.ExerciseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * A RestController providing the endpoints subsequent to /exercises .
 */
@Component
@RestController
@RequestMapping(value = "/exercises")
@CrossOrigin(origins = "*")
public class ExerciseController {

    /**
     * The logger for this class.
     */
    private static final Logger log = LoggerFactory.getLogger(ExerciseController.class.getName() );

    /**
     * The MIME type to use.
     */
    private static final String MIME_TYPE_JSON = "application/json";

    /**
     * The service to use to manage Exercise entities.
     */
    private final ExerciseService exerciseService;

    /**
     * Creates a new ExerciseController object.
     *
     * @param exerciseService The exercise controller's service to manage exercise entities.
     */
    @Autowired
    public ExerciseController(ExerciseService exerciseService) {
        this.exerciseService = exerciseService;
    }

    /**
     * An endpoint to create a new exercise.
     *
     * @param exerciseDto The aim's data transfer object.
     */
    @PostMapping(value ="", consumes = MIME_TYPE_JSON)
    @ResponseStatus(HttpStatus.OK)
    public ExerciseDto createNewExercise(@RequestBody ExerciseDto exerciseDto){
        log.info("Got a POST request on /exercises/");
        ExerciseDto id = exerciseService.createNewExercise(exerciseDto);
        log.info("Exercise was created");
        return id;
    }

    /**
     * An endpoint to get all aims.
     */
    @GetMapping(value = "")
    public Iterable<ExerciseDto> getAllUnassigned(){
        log.info("Got a GET request on /exercises/");
        return exerciseService.findAll();
    }

    /**
     * An endpoint to update an exercise.
     */
    @PutMapping(value = "/update")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody ExerciseDto exerciseDto){
        log.info("Got a PUT request on /exercises/update");
        exerciseService.update(exerciseDto);
    }

    /**
     * An endpoint to delete an exercise.
     */
    @DeleteMapping(value ="/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id){
        log.info("Got a DELETE request on /exercises/delete/{id}");
        exerciseService.delete(id);
    }
}