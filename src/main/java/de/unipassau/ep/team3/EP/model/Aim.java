package de.unipassau.ep.team3.EP.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.LinkedList;

/**
 * A model class representing an aim of training of the online training plattform.
 */
@Entity
@Table(name="\"aim\"")
public class Aim {

    /**
     * The ID of the aim.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The name of the aim.
     */
    @NotNull
    private String name;

    /**
     * The id of the super aim of this aim.
     */
    @ManyToOne
    private Aim superAim;

    @OneToMany(mappedBy = "aim")
    private List<User> users = new LinkedList<>();

    /**
     * The description of the aim.
     */
    @Column(length = 1000)
    private String description;

    /**
     * The description of the aim.
     */
    private String furtherInformation;

    /**
     *  A list of more specified aims to the aim.
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval  = true,fetch = FetchType.EAGER, mappedBy = "superAim")
    private List<Aim> subAims = new LinkedList<>();

    /**
     * The list of schedules which are assigned to the aim
     */
    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, mappedBy = "aim")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Schedule> schedules = new LinkedList<>();

    /***
     * Deleting the aim, it must be removed for all its schedules.
     */
    @PreRemove
    private void preRemove() {
        for (Schedule s : schedules) {
            s.setAim(null);
            s.setUnlocked(false);
        }
        for(User u :users){
            u.setAim(null);
        }

    }

    /**
     * The user who edited the aim latest.
     */
    @ManyToOne
    private User editor;

    /**
     * Creates a new aim.
     */
    Aim () {}

    /**
     * Creates a new aim.
     * @param name The aim's name.
     */
   public Aim(String name ){
        this.name = name;
    }

    //Getter  & Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFurtherInformation() {
        return furtherInformation;
    }

    public void setFurtherInformation(String furtherInformation) {
        this.furtherInformation = furtherInformation;
    }

    /**
     * Gets the list of more specified aims to the aim.
     * @return The aim's sub aim ist.
     */
    public List<Aim> getSubAims() {
        return subAims;
    }

    public void setSubAims(List<Aim> subAims) {
        this.subAims = subAims;
    }

    public User getEditor() {
        return editor;
    }

    public void setEditor(User editor) {
        this.editor = editor;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Aim getSuperAim() {
        return superAim;
    }

    public void setSuperAim(Aim superAim) {
        this.superAim = superAim;
    }

    public void addSubAim(Aim subAim){
            this.subAims.add(subAim);
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public void addSchedules(Schedule schedule){
        this.schedules.add(schedule);
    }

}
