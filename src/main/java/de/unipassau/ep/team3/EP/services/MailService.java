package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.controller.UserController;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.internet.InternetHeaders;
import java.util.Properties;

 class MailService {
    private String recipient;
    private String name;
    private String token;

    private static final Logger log = LoggerFactory.getLogger( UserController.class.getName() );

     /**
      * Creates a new MailService referenced in other classes to send computer generated mails where required.
      *
      * @param recipient Recipient of the Mail.
      * @param name Name of the Recipient.
      * @param token The recipient's unique verification token.
      */
    MailService(String recipient, String name, String token) {
        this.recipient = recipient;
        this.name = name;
        this.token = token;
    }

    /**
     * Sends standardized E-Mails with unique verification tokens.
     */
    void sendMail() {
        try {
            JavaMailSender emailSender = getJavaMailSender();
            SimpleMailMessage message = new SimpleMailMessage();
            InternetHeaders headers = new InternetHeaders();
            headers.addHeader("Content-type", "text/html; charset=UTF-8");
            String html = "Hallo " + name + ",\n\nvielen Dank für deine Interesse an unserer Trainingsplattform. Um deine " +
                    "Registrierung abzuschließen und deinen Account zu aktivieren klicke bitte auf den folgenden Link. " +
                    "Das Sportzentrum wünscht dir viel Erfolg mit deinem Trainingsplan.\n\n\n" +

                    // URL the server is running at.
                    "https://epws18.padim.fim.uni-passau.de/team3/users/verify/" +
                    token;
            message.setTo(recipient);
            message.setSubject("Account Aktivierung");
            message.setText(html);
            emailSender.send(message);
            log.info("Mail an " + recipient + " versendet. Inhalt: " + html);
        } catch (MailException e) {
            log.error(e.toString());
            throw new BadRequestException(BadRequestReason.SENDING_MAIL_FAILED);
        }
    }

     /**
      * Creates and configures the MailSender.
      *
      * @return Returns the fully configured JavaMailSender Object
      */
    private JavaMailSender getJavaMailSender(){
        JavaMailSenderImpl emailSender = new JavaMailSenderImpl();
        emailSender.setHost("smtp.gmail.com");
        emailSender.setPort(587);
        emailSender.setUsername("aktivierung.team3@gmail.com");
        emailSender.setPassword("!tr4Uwb3");
        Properties props = emailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        return emailSender;
    }
}
