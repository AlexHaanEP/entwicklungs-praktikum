package de.unipassau.ep.team3.EP.database;

import de.unipassau.ep.team3.EP.model.Experience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExperienceRepository extends JpaRepository<Experience,Long> {

    /**
     * Gets an aim by its id.
     *
     * @param id The aim's id.
     * @return The corresponding aim or {@code null}.
     */
    @Query("SELECT e FROM Experience AS e WHERE e.id = :id")
    Experience findByExperienceId(@Param("id") long id);

    /**
     * Gets an experience level by its name.
     *
     * @param name The experience level's name.
     * @return The corresponding experience level or {@code null}.
     */
    @Query("SELECT e FROM Experience AS e WHERE e.name = :name")
    List<Experience> findByExperienceName(@Param("name") String name);

}
