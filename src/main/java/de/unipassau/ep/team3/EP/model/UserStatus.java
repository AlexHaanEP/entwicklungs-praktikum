package de.unipassau.ep.team3.EP.model;

/**
 * an enum of possible user statuses.
 */
public enum UserStatus {
    /**
     * This user has more rights than USER
     */
    TRAINER ,

    /**
     *This user is a normal user
     */
    USER,

    /**
     * The user isn't verified through the verification token or the user
     * doesn't exist.
     */
    UNKNOWN,
}
