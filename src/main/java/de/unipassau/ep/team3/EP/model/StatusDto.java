package de.unipassau.ep.team3.EP.model;

public class StatusDto {

    /**
     * The zim ID of the user.
     */
    private String zim;

    /**
     * The status of the user.
     */
    private int status;

    StatusDto() {}

    public StatusDto(String zim, int status) {
        this.zim = zim;
        this.status = status;
    }

    //Getter & Setter

    public String getZim() {
        return zim;
    }

    public int getStatus() {
        return status;
    }

}
