package de.unipassau.ep.team3.EP.database;

import de.unipassau.ep.team3.EP.model.ExerciseWrapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ExerciseWrapperRepository extends JpaRepository<ExerciseWrapper,Long> {

    /**
     * Gets an aim by its id.
     *
     * @param id The aim's id.
     * @return The corresponding aim or {@code null}.
     */
    @Query("SELECT w FROM ExerciseWrapper AS w WHERE w.id = :id")
    ExerciseWrapper findByWrapperId(@Param("id") long id);
}
