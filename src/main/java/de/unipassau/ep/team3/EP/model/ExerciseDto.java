package de.unipassau.ep.team3.EP.model;

/**
 * Data transfer object class for the Exercise model.
 */
public class ExerciseDto {
    /**
     * The ID of the exercise.
     */
    private Long id;

    /**
     * The name of an exercise
     */
    private String name;

    /**
     * The description of an exercise.
     */
    private String description;

   /**
     * The user who edited the exercise last.
     */
    private String editor;

    /**
     * The URL to a video of the exercise
     */
    private String videoURL;

    /**
     * The id of an an alternative exercise
     */
    private long [] alternativeIds;

    /**
     * The array of execution possibiities.
     */
    private String [] executions;

    /**
     * If true, the exercise is either the dummy exercise or an phantom exercise.
     */
    private boolean assigned;

    /**
     * Creates a new exercise transfer object.
     */
    public ExerciseDto(){}

    /**
     * Creates a new exercise transfer object.
     *
     * @param name The exercise's name.
     */
    public ExerciseDto(String name){
        this.name = name;
    }

    //Getter & Setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public long[] getAlternativeIds() {
        return alternativeIds;
    }

    public void setAlternativeIds(long[] alternativeIds) {
        this.alternativeIds = alternativeIds;
    }

    public String[] getExecutions() {
        return executions;
    }

    public void setExecutions(String[] executions) {
        this.executions = executions;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }
}
