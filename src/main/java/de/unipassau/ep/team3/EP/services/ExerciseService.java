package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.database.ExerciseRepository;
import de.unipassau.ep.team3.EP.database.UserRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A spring service class to manage exercises of the online training plattform.
 */
@Service
public class ExerciseService {
    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger(ExerciseService.class.getName());
    /**
     * The database repository for exercises.
     */
    private final ExerciseRepository exerciseRepository;

    /**
     * The database repository for users.
     */
    private final UserRepository userRepository;

    /**
     * Creates a new exercise service.
     *
     * @param exerciseRepository The exercise service's exercise repository automatically injected..
     * @param userRepository     The exercise service'S user repository automaticaly injected.
     */
    public ExerciseService(ExerciseRepository exerciseRepository, UserRepository userRepository) {
        this.exerciseRepository = exerciseRepository;
        this.userRepository = userRepository;
    }

    /**
     * Creates a new exercise.
     *
     * @param exerciseDto The exercise's DTO be created from.
     * @return The ID of the newly created exercise.
     */

    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public ExerciseDto createNewExercise(ExerciseDto exerciseDto) {
        if (exerciseDto.getName() == null) {
            log.error(BadRequestReason.NAME_MISSING.toString());
            throw new BadRequestException(BadRequestReason.NAME_MISSING);
        } else if (exerciseDto.getName().equals("")) {
            log.error(BadRequestReason.INVALID_NAME.toString());
            throw new BadRequestException(BadRequestReason.INVALID_NAME);
        } else if (alreadyExists(exerciseDto.getName())) {
            log.error(BadRequestReason.ALREADY_EXISTS.toString() + " name:" + exerciseDto.getName());
            throw new BadRequestException(BadRequestReason.ALREADY_EXISTS);
        } else if (!areNotOwnId(exerciseDto.getAlternativeIds())) {
            log.error(BadRequestReason.ALTERNATIVE_ID_OWN_ID.toString());
            throw new BadRequestException(BadRequestReason.ALTERNATIVE_ID_OWN_ID);
        } else if (!alternativesAreExisting(exerciseDto.getAlternativeIds())) {
            log.error(BadRequestReason.ALTERNATIVE_ID_NOT_EXISTING.toString());
            throw new BadRequestException(BadRequestReason.ALTERNATIVE_ID_NOT_EXISTING);
        }
        {
            Exercise exercise = DtoConverter.convertFromDto(exerciseDto);
            log.debug("Exercise was created from the exerciseDto with name: " + exerciseDto.getName());
            if (exerciseDto.getAlternativeIds() != null) {
                long[] alternatives = exerciseDto.getAlternativeIds();
                for (long alternative : alternatives) {
                    if (alternative > 0) {
                        Exercise alternativeExercise = exerciseRepository.findByExerciseId(alternative);
                        exercise.addAlternativeExercise(alternativeExercise);
                        log.debug("Exercise was added as alternative exercise id:" + alternative);
                    }
                }
                log.debug("The alternative exercises where added to the exercise");
            }

            String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
            User editor = userRepository.findByZim(currentUser);
            exercise.setEditor(editor);
            log.debug("The editor: " + editor.getZim() + " for the exercise was set");

            Exercise db_exercise = exerciseRepository.save(exercise);
            log.info("The exercise was saved in the database.");
            return DtoConverter.convertToDto(db_exercise);

        }
    }

    /**
     * A method to update an exercise.
     *
     * @param exerciseDto The exerciseDto to get the updates from.
     */
    @Modifying
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public void update(ExerciseDto exerciseDto) {
        if (exerciseDto.getName() == null) {
            log.error(BadRequestReason.NAME_MISSING.toString());
            throw new BadRequestException(BadRequestReason.NAME_MISSING);
        } else if (exerciseDto.getName().equals("")) {
            log.error(BadRequestReason.INVALID_NAME.toString());
            throw new BadRequestException(BadRequestReason.INVALID_NAME);
        } else if (exerciseDto.getId() == null) {
            log.error(BadRequestReason.ID_MISSING.toString() + " of exercise " + exerciseDto.getName());
            throw new BadRequestException(BadRequestReason.ID_MISSING);
        } else if (exerciseDto.getId() == 1) {
            log.error(BadRequestReason.DUMMY_EXERCISE_ID.toString());
            throw new BadRequestException(BadRequestReason.DUMMY_EXERCISE_ID);
        }
        {
            Exercise toUpdate = findExerciseById(exerciseDto.getId());
            if (!toUpdate.getName().equals(exerciseDto.getName())
                    && exerciseRepository.findByName(exerciseDto.getName()).size() > 0) {
                log.error(BadRequestReason.ALREADY_EXISTS.toString() + " " + exerciseDto.getName());
                throw new BadRequestException(BadRequestReason.ALREADY_EXISTS);
            }
            toUpdate = DtoConverter.convertFromDto(exerciseDto);
            toUpdate.setId(exerciseDto.getId());
            if (exerciseDto.getAlternativeIds() != null) {
                long[] alternatives = exerciseDto.getAlternativeIds();
                for (long alternative : alternatives) {
                    if (alternative > 0) {
                        if (alternative == toUpdate.getId()) {
                            throw new BadRequestException(BadRequestReason.ALTERNATIVE_ID_OWN_ID);
                        }
                        Exercise alternativeExercise = findExerciseById(alternative);
                        toUpdate.addAlternativeExercise(alternativeExercise);
                        log.debug("Alternative exercise " + alternativeExercise.getId()+" was added to exercise "+ toUpdate.getId());
                    } else {
                        log.error(BadRequestReason.EXERCISE_NOT_EXISTING.toString() + " with id: " + alternative);
                        throw new BadRequestException(BadRequestReason.EXERCISE_NOT_EXISTING);
                    }
                }
            }

            String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
            User editor = userRepository.findByZim(currentUser);
            toUpdate.setEditor(editor);
            log.debug("The editor: " + editor.getZim() + " was updated ");
             exerciseRepository.save(toUpdate);
            log.info("The exercise was updated in the database.");
        }
    }

    /**
     * A method to delete an experience level
     *
     * @param id The id of the exercise to deleted.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public void delete(Long id) {
        if (id == 1) {
            log.error(BadRequestReason.DUMMY_EXERCISE_ID.toString());
            throw new BadRequestException(BadRequestReason.DUMMY_EXERCISE_ID);
        }

        Exercise toDelete = findExerciseById(id);
        Exercise deepCopy = exerciseRepository.save(deepCopy(toDelete));

        for (Schedule s : toDelete.getScheduleList()) {

            int toDeleteIndex = s.getExercises().indexOf(toDelete);
            if (s.getUser() == null) {
                s.getExercises().remove(toDeleteIndex);
                s.getWrappers().remove(toDeleteIndex);
                s.getAlternativeExercises().remove(toDeleteIndex);

                s.getRank().remove(toDeleteIndex);
                s.getRankIndexes().remove(Integer.valueOf(toDeleteIndex));
                s.setUnlocked(false);
                log.info(" The exercise was deleted from the blanco schedule " + s.getId());
            } else {
                s.getExercises().remove(toDeleteIndex);
                s.getExercises().add(toDeleteIndex, deepCopy);
                deepCopy.getScheduleList().add(s);
                log.info("The exercise was replaced by a deep compy of it in schedule " + s.getId());
            }
        }

        Exercise dummy = findExerciseById((long) 1);
        for (Schedule s : toDelete.getScheduleListAlternative()) {
            int i = s.getAlternativeExercises().indexOf(toDelete);
            s.getAlternativeExercises().set(i, dummy);
            log.info(" The exercise was replaced in the schedule " + s.getId() + " with the dummy exercise");
        }

        exerciseRepository.delete(findExerciseById(id));
        log.info("The exercise was deleted");
    }


    /**
     * Gets all execises from the database as DTOs.
     *
     * @return A list of DTOs.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public Iterable<ExerciseDto> findAll() {
        return exerciseRepository.findAllButDummyExercise().stream()
                .map(DtoConverter::convertToDto).collect(Collectors.toList());
    }

    /**
     * A method to check if the id's are not the exercise's id.
     *
     * @param alternative The alternative exercises id's array.
     * @return {@code true} if its the id's are not the exercise's id.
     * {@code false} otherwise.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    private boolean areNotOwnId(long[] alternative) {
        for (long check : alternative) {
            if (check == findHighestId() + 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * A method to check if the id's are existing.
     *
     * @param alternative The alternative exercises id's array.
     * @return {@code true} if its the id's are existing.
     * {@code false} otherwise.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    private boolean alternativesAreExisting(long[] alternative) {
        for (long check : alternative) {
            if (check > findHighestId() + 1 || check < 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * A method to get the highest id of the database, if null return 0.
     *
     * @return The highest id.
     */
    private Long findHighestId() {
        if (exerciseRepository.findHighestId() != null) {
            log.debug("Highest exercise id : "+ exerciseRepository.findHighestId());
            return exerciseRepository.findHighestId();
        } else {
            return 0L;
        }
    }

    /**
     * Gets an exercise from the database.
     *
     * @param id The ID of the exercise.
     * @return The exercise.
     * @throws BadRequestException Exercise with this ID doesn't exists.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    Exercise findExerciseById(Long id) {
        Exercise exercise = exerciseRepository.findByExerciseId(id);
        if (exercise == null) {
            log.error(BadRequestReason.EXERCISE_NOT_EXISTING.toString() + " id: " + id);
            throw new BadRequestException(BadRequestReason.EXERCISE_NOT_EXISTING);
        }
        log.info("The exercise with name: " + exercise.getName() + " was loaded from the database. id:" + exercise.getId());
        return exercise;
    }

    /**
     * Creates a deep copy of an exercise
     *
     * @param toCopy The Exercise to deep copy from
     * @return The deep copy.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    private Exercise deepCopy(Exercise toCopy) {
        Exercise deepCopy = new Exercise(toCopy.getName());
        deepCopy.setDescription(toCopy.getDescription());
        if (toCopy.getVideo() != null) {
            try {
                URL copyURL = new URL(toCopy.getVideo().toString());
                deepCopy.setVideo(copyURL);
            } catch (MalformedURLException e) {
                log.error(BadRequestReason.INVALID_URL.toString());
                throw new BadRequestException(BadRequestReason.INVALID_URL);
            }
        }
        deepCopy.setAssigned(true);
        log.info(" A deep copy of the exercise "+deepCopy.getId()+" was created ");
        return deepCopy;
    }

    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    private boolean alreadyExists(String name) {
        List<Exercise> exercises = exerciseRepository.findByName(name);
        for (Exercise e : exercises) {
            if (!e.isAssigned()) {
                return true;
            }
        }
        return false;
    }

}

