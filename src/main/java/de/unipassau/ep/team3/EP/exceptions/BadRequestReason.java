package de.unipassau.ep.team3.EP.exceptions;

public enum BadRequestReason {

    /**
     * The user is younger than 16 years.
     */
    TOO_OLD,

    /**
     * The user is older than 70 years.
     */
    TOO_YOUNG,

    /**
     * The date is invalid.
     */
    INVALID_DATE,

    /**
     * The password is to short, it must have a min length of 8 characters.
     */
    PASSWORD_TOO_SHORT,

    /**
     * The zim ID is invalid.
     */
    INVALID_ZIM,

    /**
     * The zim ID is missing.
     */
    ZIM_MISSING,

    /**
     * The name of the user is missing.
     */
    NAME_MISSING,

    /**
     * The name of the user is invalid
     */
    INVALID_NAME,

    /**
     * The password is missing.
     */
    PASSWORD_MISSING,

    /**
     * Sending an Mail failed
     */
    SENDING_MAIL_FAILED,

    /**
     * The User has already been verified.
     */
    NOT_EXISTING_OR_ALREADY_VERIFIED,

    /**
     * The user already exists.
     */
    ALREADY_EXISTS,

    /**
     * The super aim id is smaller than 1.
     */
    INVALID_SUPER_AIM_ID,

    /**
     * The alternative exercise id is the id of the exercise.
     */
    ALTERNATIVE_ID_OWN_ID,

    /**
     * The exercise with the alternative id doesn't exists.
     */
    ALTERNATIVE_ID_NOT_EXISTING,

    /**
     * The given URL is invalid
     */
    INVALID_URL,
    /**
     * The super aim is not existing
     */
    SUPER_AIM_NOT_EXISTING,

    /**
     * The aim is not existing.
     */
    AIM_NOT_EXISTING,

    /**
     * The experience level is not existing
     */
    EXPERIENCE_LEVEL_NOT_EXISTING ,

    /**
     * The gender is either not existing or wrong written.
     */
    INVALID_GENDER,

    /**
     * The user is not existing.
     */
    USER_NOT_EXISTING,

    /**
     * Th
     */
    EXERCISE_NOT_EXISTING,

    /**
     * The rank is too high to be valid.
     */
    RANK_TOO_HIGH,

    /**
     * The rank is too low to be valid
     */
    RANK_TOO_LOW,

    /**
     * The rank doesn't match the given rank format.
     */
    INVALID_RANK_FORMAT,

    /**
     * The given alternative exercise id is the own id.
     */
    ALTERNATIVE_EXERCISE_OWN_EXERCISE,

    /**
     * The number of exercises and alternative exercises is not even.
     * Every exercise must hav exactly one alternative exercise.
     */
    UNEQUAL_NUMBER_EXERCISES_ALTERNATIVES,

    /**
     * The number of exercise wrapper and alternative exercise wrapper is uneven.
     */
    UNEQUAL_NUMBER_EXERCISES_WRAPPER_ALTERNATIVES,

    /**
     * The given exercise ID is the ID of the dummy exercise
     */
    DUMMY_EXERCISE_ID,

    /**
     * The exercise ID is smaller than 1.
     */
    INVALID_EXERCISE_ID,

    /**
     * There is only one super aim left in the database.
     */
    LAST_AIM,
    /**
     * There is only one eperience level in the database left.
     */
    LAST_EXPERIENCE,

    /**
     * The id is missing.
     */
    ID_MISSING,

    /**
     * The schedule doesn't exist.
     */
    SCHEDULE_NOT_EXISTING,

    /**
     * The exercise wrapper doesn't exists.
     */
    WRAPPER_NOT_EXISTING ,

    /**
     * The schedule is assigned to a user.
     */
    SCHEDULE_IS_ASSIGNED,

    /**
     * The aim of the suer is missing.
     */
    AIM_IS_MISSING,

    /**
     * The experience level of the user is missing.
     */
    EXPERIENCE_IS_MISSING,

    /**
     * The gender of the user is missing.
     */
    GENDER_IS_MISSING,

    /**
     * The weight is smaller than zero.
     */
    INVALID_WEIGHT,

    /**
     * The height is smaller than zero.
     */
    INVALID_HEIGHT,

    /**
     * The schedule is missing.
     */
    NO_SCHEDULES_AVAILABLE,

    /**
     * The selected schedule is not assigned to a user.
     */
    NOT_A_USER_SCHEDULE,

    /**
     * The user as already a schedule.
     */
    HAS_SCHEDULE,

}
