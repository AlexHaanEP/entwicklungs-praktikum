package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.database.ScheduleRepository;
import de.unipassau.ep.team3.EP.database.UserRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.User;
import de.unipassau.ep.team3.EP.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import de.unipassau.ep.team3.EP.model.VerificationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Null;
import java.sql.Timestamp;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Year;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * A spring service class to manage users of the online training plattform.
 */
@Service
public class UserService implements UserDetailsService {

    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger(UserService.class.getName());
    /**
     * The minimal password length of any user.
     */
    private static final int PASSWORD_MIN_LENGTH = 8;

    /**
     * The user's minimal age to use the online training plattform.
     */
    private static final int MIN_AGE = 16;

    /**
     * The user's maximal age to use the online training plattform.
     */
    private static final int MAX_AGE = 70;

    /**
     * The database reporitory for the user object.
     */
    private UserRepository userRepository;

    /**
     * The aim's service for the user .
     */
    private final AimService aimService;

    /**
     * The experience level's service for the user.
     */
    private final ExperienceService experienceService;

    /**
     * The training schedule's service for the user.
     */
    private final ScheduleService scheduleService;

    /**
     * The training schedule's service for the user.
     */
    private final ScheduleRepository scheduleRepository;

    /**
     * Creates a new user service.
     *
     * @param userRepository     The user service's user repository.
     * @param aimService         The user service's aim service.
     * @param experienceService  The user service's experience service.
     * @param scheduleService    The user service's schedule service
     * @param scheduleRepository The user service's schedule repository.
     */
    UserService(UserRepository userRepository, AimService aimService, ExperienceService experienceService, ScheduleService scheduleService, ScheduleRepository scheduleRepository) {
        this.userRepository = userRepository;
        this.aimService = aimService;
        this.experienceService = experienceService;
        this.scheduleService = scheduleService;
        this.scheduleRepository = scheduleRepository;
    }

    /**
     * Checks for correct input and creates a new user.
     * Used for registration.
     *
     * @param userDto The user's dto to be created from.
     */
    public void createNewUser(UserDto userDto) {
        try {
            LocalDate birthday = LocalDate.of(userDto.getYearOfBirth(),
                    userDto.getMonthOfBirth(), userDto.getDayOfBirth());
            log.info("The birthday is valid");
            //Checking for wrong user input.
            if (userRepository.findByZim(userDto.getZim()) != null) {
                log.error(BadRequestReason.ALREADY_EXISTS.toString());
                throw new BadRequestException(BadRequestReason.ALREADY_EXISTS);
            } else if (userDto.getZim() == null) {
                log.error(BadRequestReason.ZIM_MISSING.toString());
                throw new BadRequestException(BadRequestReason.ZIM_MISSING);
            } else if (userDto.getZim().equals("") || userDto.getZim().length() > 8) {
                log.error(BadRequestReason.INVALID_NAME.toString());
                throw new BadRequestException(BadRequestReason.INVALID_ZIM);
            } else if (userDto.getName() == null) {
                log.error(BadRequestReason.NAME_MISSING.toString());
                throw new BadRequestException(BadRequestReason.NAME_MISSING);
            } else if (userDto.getName().equals("")) {
                log.error(BadRequestReason.INVALID_NAME.toString());
                throw new BadRequestException(BadRequestReason.INVALID_NAME);
            } else if (!isOldEnough(birthday)) {
                log.error(BadRequestReason.TOO_YOUNG.toString());
                throw new BadRequestException(BadRequestReason.TOO_YOUNG);
            } else if (!isYoungEnough(birthday)) {
                log.error(BadRequestReason.TOO_OLD.toString());
                throw new BadRequestException(BadRequestReason.TOO_OLD);
            } else if (userDto.getPassword() == null) {
                log.error(BadRequestReason.PASSWORD_MISSING.toString());
                throw new BadRequestException(BadRequestReason.PASSWORD_MISSING);
            } else if (userDto.getPassword().length() < PASSWORD_MIN_LENGTH) {
                log.error(BadRequestReason.PASSWORD_TOO_SHORT.toString());
                throw new BadRequestException(BadRequestReason.PASSWORD_TOO_SHORT);
            } else {
                String token;

                //Ensuring the token has not already been assigned to an account.
                do {
                    token = VerificationToken.generateString();
                } while (userRepository.findVerificationToken(token) != null);

                userDto.setVerificationToken(token);
                log.debug("Token was set ");
                MailService mailService = new MailService(userDto.getZim() + "@gw.uni-passau.de", userDto.getName(), token);
                mailService.sendMail();
                log.debug("Mail was send");
                userDto.setBirthday(birthday);
                userRepository.save(DtoConverter.convertFromDto(userDto));
                log.info("The user "+userDto.getZim()+" was saved in the database.");

            }
        } catch (DateTimeException e) {
            log.error("The Date is invalid." + e);
            throw new BadRequestException(BadRequestReason.INVALID_DATE);
        }
    }

    /**
     * Activates an users account after verifying his token via Email.
     *
     * @param verificationToken An unique verification token.
     */
    public void verifyUser(String verificationToken) {
        User toVerify = (userRepository.findVerificationToken(verificationToken));
        if (toVerify == null) {
            log.error(BadRequestReason.NOT_EXISTING_OR_ALREADY_VERIFIED.toString());
            throw new BadRequestException(BadRequestReason.NOT_EXISTING_OR_ALREADY_VERIFIED);
        }
        toVerify.setValidUser(true);
        toVerify.setStatus(UserStatus.USER);
        toVerify.setVerificationToken(null);
        userRepository.save(toVerify);
        log.info("The user " + toVerify + "was updated to User status");
    }

    /**
     * Searches for all User saved in the database and converted into an Object saving name, "ZIM-Kennung"
     * the user's status and last login only.
     *
     * @return Returns an JSONObject list with all users saved in the database.
     */
    public String getAllUser() {
        LinkedList<User> userList = userRepository.getAllUser();
        StringBuilder sb = new StringBuilder();
        sb.append("User:");
        try {
            while (userList.peekFirst() != null) {
                User it = userList.pollFirst();
                sb.append("{");
                sb.append("\"name\"" + ":" + "\"").append(it.getName()).append("\"");
                sb.append(",");
                sb.append("\"zim\"" + ":" + "\"").append(it.getZim()).append("\"");
                sb.append(",");
                sb.append("\"status\"" + ":" + "\"").append(it.getStatus()).append("\"");
                sb.append(",");
                sb.append("\"login\"" + ":" + "\"").append(it.getLastLogin()).append("\"");
                sb.append("}");
                if (userList.peekFirst() != null) {
                    sb.append(" , ");
                }
            }
        } catch (NullPointerException e) {
            log.error(e.toString());
        }
        return sb.toString();
    }

    /**
     * Checks if the user is old enough (above 16 years).
     *
     * @param birthday The user's birthday.
     * @return true if user is 16 or older {@code false} otherwise.
     */
    private static boolean isOldEnough(LocalDate birthday) {
        LocalDate minAge = LocalDate.now().minusYears(MIN_AGE);
        return !birthday.isAfter(minAge);
    }

    /**
     * Checks if the user is young enough (under 70 years).
     *
     * @param birthday The user's birthday.
     * @return true if user is 70 or younger {@code false} otherwise.
     */
    private static boolean isYoungEnough(LocalDate birthday) {
        LocalDate maxAge = LocalDate.now().minusYears(MAX_AGE);
        return !birthday.isBefore(maxAge);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String zim) throws UsernameNotFoundException {
        log.debug("Called with ZIM-ID: " + zim);
        User user = userRepository.findByZim(zim);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + user.getStatus().toString()));

        return new org.springframework.security.core.userdetails.User(user.getZim(), user.getHash(),
                grantedAuthorities);
    }

    /**
     * A method to get the status of a spezific user.
     * @param zim The zim ID of teh user.
     * @return The user's status as string.
     * @throws BadRequestException if the specified user does not exist.
     */
    public String getUserStatus(String zim) throws BadRequestException {
        User user = userRepository.findByZim(zim);
        if (user != null) {
            log.debug("The user " + user + " has the status " + user.getStatus().toString());
            return user.getStatus().toString();
        } else {
            log.debug("The user is either blocked or not registered");
            return UserStatus.UNKNOWN.toString();
        }
    }

    /**
     * A method to set update the last login timestamp of a user.
     *
     * @param zim The current user's zim ID.
     */
    public void setCurrentLastLogin(String zim) {
        User current = userRepository.findByZim(zim);
        current.setLastLogin(new Timestamp(System.currentTimeMillis()));
        userRepository.save(current);
        log.info("The user " + current.getZim() + " was last logged in " + current.getLastLogin().toString());

    }

    /**
     * Updates a user's status and saves the new status in the database.
     *
     * @param userStatus The user who's status has changed.
     */
    public void updateUserStatus(StatusDto userStatus) {
        String zim = userStatus.getZim();
        int status = userStatus.getStatus();
        User current = userRepository.findByZim(zim);
        if (status == 0) {
            current.setStatus(UserStatus.TRAINER);
            log.debug("The user got the status TRAINER");
        } else if (status == 1) {
            current.setStatus(UserStatus.USER);
            log.debug("The user got the status USER");
        } else {
            log.debug("The user got the status UNKNOWN");
            current.setStatus(UserStatus.UNKNOWN);
        }
        userRepository.save(current);
        log.info("The user " + zim + "'s status was updated. ");
    }

    /**
     * Gets the id of the tranings schedule of the user.
     *
     * @param zim The user's zim ID.
     * @return The training schedule's id or null if he has none.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public Long getScheduleId(String zim) {
        User user = scheduleService.findUserByZim(zim);
        if (user.getSchedule() == null) {
            log.debug("The user " + user.getZim() + " has no schedule");
            return null;
        } else {
            log.debug("The user " + user.getZim() + " has the schedule with the id " + user.getSchedule().getId());
            return user.getSchedule().getId();
        }
    }

    /**
     * Gets the id of the aim of the user.
     *
     * @param zim The user's zim ID.
     * @return The aim's id or null if he has none.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public Long getAimId(String zim) {
        User user = scheduleService.findUserByZim(zim);
        if (user.getAim() == null) {
            log.debug("The user " + user.getZim() + " has no aim");
            return null;
        } else {
            log.debug("The user " + user.getZim() + " has theaim with the id " + user.getAim().getId());
            return user.getAim().getId();
        }
    }

    /**
     * A method, to save the user input inclusive a suitable training schedule for the user.
     *
     * @param userDto The user's data to save.
     * @return The id of the training schedule of the user.
     */
    @Transactional
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public Long firstLogin(UserDto userDto) {
        User user = scheduleService.findUserByZim(userDto.getZim());

        //The aim for the user will be set
        if (userDto.getAimId() == null) {
            log.error("The aim for the user " + user.getZim() + " is missing");
            throw new BadRequestException(BadRequestReason.AIM_IS_MISSING);
        }
        Aim aim = aimService.findAimById(userDto.getAimId());
        user.setAim(aim);
        aim.getUsers().add(user);
        log.debug("The aim with the id " + user.getAim().getId() + " was set for user " + user.getZim());

        //The experience level for the user will be set
        if (userDto.getExperienceId() == null) {
            log.error("The experience for the user " + user.getZim() + " is missing");
            throw new BadRequestException(BadRequestReason.EXPERIENCE_IS_MISSING);
        }
        Experience level = experienceService.findExperienceById(userDto.getExperienceId());
        user.setExperience(level);
        level.getUsers().add(user);
        log.debug("The experience with the id " + user.getExperience().getId() + " was set for user " + user.getZim());

        //The gender for the user will be set.
        if (userDto.getGender() == null) {
            log.error("The gender for the user " + user.getZim() + " is missing");
            throw new BadRequestException(BadRequestReason.GENDER_IS_MISSING);
        }
        Gender gender = scheduleService.testValidGender(userDto.getGender());
        user.setGender(gender);
        log.debug("The gender " + user.getGender().toString() + " was set for user " + user.getZim());

        if (userDto.getHeight() < 0) {
            log.error(BadRequestReason.INVALID_HEIGHT.toString());
            throw new BadRequestException(BadRequestReason.INVALID_HEIGHT);
        } else {
            user.setHeight(userDto.getHeight());
        }

        if (userDto.getWeight() < 0) {
            log.error(BadRequestReason.INVALID_HEIGHT.toString());
            throw new BadRequestException(BadRequestReason.INVALID_WEIGHT);
        } else {
            user.setWeight(userDto.getWeight());
        }
        return getSchedule(user);

    }

    /**
     * Selects all trainings schedules matching the following options an choose one randomly.
     *
     * @param aim    The aim of a user.
     * @param level  The experience level of a user.
     * @param gender The gender of a user.
     * @return The suitable schedule for the user.
     */
    private Schedule findSuitableSchedule(Aim aim, Experience level, Gender gender) {
        LinkedList<Schedule> schedules = scheduleRepository.findSchedulesByParameter(aim, level, gender);
        log.debug("The lenght of the schedule list is" + schedules.size());
        if (schedules.size() == 0) {
            if (aim.getSuperAim() != null) {
                log.debug("No suitable schedule for the aim, it will be searched schedules with superaim " + aim.getSuperAim().getId());
                findSuitableSchedule(aim.getSuperAim(), level, gender);
            } else {
                log.error(BadRequestReason.NO_SCHEDULES_AVAILABLE.toString());
                throw new BadRequestException(BadRequestReason.NO_SCHEDULES_AVAILABLE);
            }
        }
        int randomNum = ThreadLocalRandom.current().nextInt(0, schedules.size());
        if (randomNum != 0) {
            randomNum -= 1;
        }
        log.info("Random number index is " + randomNum);
        return schedules.get(randomNum);
    }

    /**
     * Returns a specific user.
     *
     * @param zim The zim of the user.
     * @return The user DTO.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public UserDto getUser(String zim) {
        User user = scheduleService.findUserByZim(zim);
        return DtoConverter.convertToDto(user);
    }

    /**
     * Sets a new aim for the user, selects and sets an suitable schedule and return its id.
     *
     * @param userDto The user DTO to get the aim from and
     * @return The ID of the schedule.
     */
    @Transactional
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public Long selectSchedule(UserDto userDto) {
        User user = scheduleService.findUserByZim(userDto.getZim());
        log.debug("The user to get a schedule for is " + user.getZim());
        if (userDto.getAimId() == null) {
            log.error(BadRequestReason.AIM_IS_MISSING.toString());
            throw new BadRequestException(BadRequestReason.AIM_IS_MISSING);
        }
        Aim aim = aimService.findAimById(userDto.getAimId());
        user.setAim(aim);
        aim.getUsers().add(user);
        log.debug("The aim " + aim.getId() + ", " + aim.getName() + " was set for the user ");
        if (user.getSchedule() != null) {
            log.error("The user " + user.getZim() + " " + BadRequestReason.HAS_SCHEDULE);
            throw new BadRequestException(BadRequestReason.HAS_SCHEDULE);
        }

        return getSchedule(user);
    }

    /**
     * Selects an suitable trainingsschedule for the user and configures it for te user.
     *
     * @param user The user to get a schedule for.
     * @return The id of the selecdet training schedule.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    private Long getSchedule(User user) {
        Schedule db_schedule = findSuitableSchedule(user.getAim(), user.getExperience(), user.getGender());
        log.debug("The suitable schedule " + db_schedule.getId() + "was found for the user " + user.getZim());
        Schedule trainingsSchedule = scheduleService.deepCopy(db_schedule);
        log.debug("A deep copy of the schedule was created");

        trainingsSchedule.setUser(user);
        user.setSchedule(trainingsSchedule);
        userRepository.save(user);
        log.debug("The deep copy was saved for the user as trainings schedule");

        Schedule e = scheduleRepository.save(trainingsSchedule);
        log.debug("The schedule " + e.getId() + "was saved");

        scheduleService.setReferencesFromExerciseToSchedule(e);
        log.debug("The references were set");
        return e.getId();
    }

    /**
     * Changes the password of a user
     *
     * @param zim      The zim ID.
     * @param password Th epassword to change.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public void changePassword(String zim, String password) {
        User user = scheduleService.findUserByZim(zim);

        if (password.length() < PASSWORD_MIN_LENGTH) {
            log.error(BadRequestReason.PASSWORD_TOO_SHORT.toString());
            throw new BadRequestException(BadRequestReason.PASSWORD_TOO_SHORT);
        }
        user.setHash(null);
        user.setHash((new BCryptPasswordEncoder()).encode(password));
        userRepository.save(user);
        log.info("A new password was saved for the user " + user.getZim());
    }

    /**
     * A method to block a user.
     *
     * @param zim The zim ID of the user to be blocked
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public void blockUser(String zim) {
        User user = scheduleService.findUserByZim(zim);
        user.setStatus(UserStatus.UNKNOWN);
        userRepository.save(user);
        log.info("The user " + user.getZim() + " was blocked");
    }

    /**
     * A method to change the height,wheigt and experience level of a user.
     *
     * @param userDto The user DTO to get the changes from.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public void changePersonalData(UserDto userDto) {
        User user = scheduleService.findUserByZim(userDto.getZim());
        if (userDto.getHeight() != 0) {
            user.setHeight(userDto.getHeight());
        }
        if (userDto.getWeight() != 0) {
            user.setWeight(userDto.getWeight());
        }
        Experience level = experienceService.findExperienceById(userDto.getExperienceId());
        user.setExperience(level);
        userRepository.save(user);
        log.info("The user " + user.getZim() + " was updated with the experience level " + user.getExperience().getId());
    }




}