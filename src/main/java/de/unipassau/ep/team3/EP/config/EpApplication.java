package de.unipassau.ep.team3.EP.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.logging.LogManager;

@Configuration
@ComponentScan(basePackages = {"de.unipassau.ep.team3.EP.model", "de.unipassau.ep.team3.EP.database",
        "de.unipassau.ep.team3.EP.controller", "de.unipassau.ep.team3.EP.services", "de.unipassau.ep.team3.EP.config"})
@EntityScan("de.unipassau.ep.team3.EP.model")
@EnableJpaRepositories("de.unipassau.ep.team3.EP.database")
@SpringBootApplication
public class EpApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(EpApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(EpApplication.class);
    }
}

