package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.database.AimRepository;
import de.unipassau.ep.team3.EP.database.UserRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A spring service class to manage aims of the online training plattform.
 */
@Service
public class AimService {

    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger( AimService.class.getName() );
    /**
     * The database repository for aims.
     */
    private final AimRepository aimRepository;

    /**
     * The database repository for users.
     */
    private final UserRepository userRepository;

    /**
     * Creates a new aim service.
     *
     * @param aimRepository The aim service's aim repository automatically injected.
     * @param userRepository The aim service'S user repository automaticaly injected.
     */
    public AimService(AimRepository aimRepository, UserRepository userRepository) {
        this.aimRepository = aimRepository;
        this.userRepository = userRepository;
    }

    /**
     * Creates a new aim.
     * @param aimDto The aim's dto to be created from.
     */
     @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
     public void createNewAim(AimDto aimDto){
        if(aimDto.getName()== null){
            log.error(BadRequestReason.NAME_MISSING.toString());
            throw new BadRequestException(BadRequestReason.NAME_MISSING);
        } else if (aimDto.getName().equals("")){
            log.error(BadRequestReason.INVALID_NAME.toString());
            throw new BadRequestException(BadRequestReason.INVALID_NAME);
        } else if(aimDto.getSuperAimId() == null || aimDto.getSuperAimId() <0){
            log.error(BadRequestReason.INVALID_SUPER_AIM_ID.toString()+" id: " +aimDto.getSuperAimId());
            throw new BadRequestException(BadRequestReason.INVALID_SUPER_AIM_ID);
        } else if(aimAlreadyExists(aimDto.getName(),aimDto.getSuperAimId())) {
            log.error(BadRequestReason.ALREADY_EXISTS +" name:"+aimDto.getName()+", superaimid:" + aimDto.getSuperAimId());
            throw new BadRequestException(BadRequestReason.ALREADY_EXISTS );
        }else{
            Aim aim = DtoConverter.convertFromDto(aimDto);
            log.debug("An Aim was created from the aimDto");

            if(aimRepository.findByAimId(aimDto.getSuperAimId()) != null){
                Aim superAim = findAimById(aimDto.getSuperAimId());
                aim.setSuperAim(superAim);
                log.debug("The super aim was set. super aim id:" + superAim.getId());
                superAim.addSubAim(aim);
                log.debug("The aim was set as one of the superaim's subaim");

            }

            String currentUser =  SecurityContextHolder.getContext().getAuthentication().getName();
            User editor = userRepository.findByZim(currentUser);
            aim.setEditor(editor);
            log.debug("The editor:"+editor.getZim()+" of the aim was set.");
            Aim db_aim = aimRepository.save(aim);
            log.info("The aim was saved in the database. name:"+db_aim.getName()+", ID:"+db_aim.getId());
        }

    }

    /**
     * Checks if an aim already exists {@code true},
     * {@code false} otherwise.
     *
     * @param name The name of the aim.
     * @param superAimId The super aim's id.
     * @return true if the aim exists, false otherwise
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
     boolean aimAlreadyExists(String name, Long superAimId){


        List<Aim> sameNameAims = aimRepository.findByName(name);
        if(sameNameAims.size()>0){

            // the aim isn't a sub aim
            if(superAimId.equals(0L)){
                return true;
            }

            //the aim is a sub aim
            for(Aim aimIterator :sameNameAims){
                if(aimIterator.getSuperAim().getId().equals(superAimId)){
                    return true;
                }
            }
            return false;
        }else {
            return false;
        }
    }

    /**
     * Gets all aims from the database as DTOs.
     *
     * @return A list of DTOS.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public Iterable<AimDto> findAll(){
        return aimRepository.findAll().stream()
                .map(DtoConverter::convertToDto).collect(Collectors.toList());
    }

    /**
     * A method to delete an aim. If its an super aim , all sub aims are deleted too.
     *
     * @param id the aim dto of the aim to delete
     */
    @Modifying
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public void delete(Long id) {
        if(aimRepository.findAllSuperAims().size() == 1 && aimRepository.findByAimId(id).getSuperAim() == null){
            log.error(BadRequestReason.LAST_AIM.toString() +" id of the aim:" +id);
            throw new BadRequestException(BadRequestReason.LAST_AIM);
        }
        Aim aim = aimRepository.findByAimId(id);
        if(aim == null){
            log.error(BadRequestReason.AIM_NOT_EXISTING.toString()+" id of the aim:" +id);
            throw new BadRequestException(BadRequestReason.AIM_NOT_EXISTING);
        } else {
        if(aim.getSuperAim() != null){
            aim.getSuperAim().getSubAims().remove(aim);
        }
        aimRepository.delete(aimRepository.findByAimId(id));
        log.info("The aim: "+id+" was deleted");}
    }

    /**
     * A method to update an aim.
     *
     * @param aimDto The aimDto to get the updates from.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public void update(AimDto aimDto) {
        Aim aim = findAimById(aimDto.getId());
        //Not duplicated because in experience service an experience dto is used
        if(aimDto.getName()== null){
            log.error(BadRequestReason.NAME_MISSING.toString());
            throw new BadRequestException(BadRequestReason.NAME_MISSING);
        } else if (aimDto.getName().equals("")){
            log.error(BadRequestReason.INVALID_NAME.toString());
            throw new BadRequestException(BadRequestReason.INVALID_NAME);
        }
        aim.setName(aimDto.getName());
        aim.setDescription(aimDto.getDescription());
        aim.setFurtherInformation(aimDto.getFurtherInformation());
        log.debug("Name,description and further information of the aim was set");

        String currentUser =  SecurityContextHolder.getContext().getAuthentication().getName();
        User editor = userRepository.findByZim(currentUser);
        aim.setEditor(editor);
        log.debug("Editor:" + editor.getZim()+" was set for the aim");
        Aim db_aim = aimRepository.save(aim);
        log.info("The aim was updated in the database. ID: "+db_aim.getId()+", name: "+db_aim.getName());
    }

    /**
     * Gets an aim from the database.
     *
     * @param id The ID of the aim.
     * @return The aim.
     * @throws BadRequestException Aim with this ID doesn't exists.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
     public Aim findAimById(Long id) {

        Aim aim = aimRepository.findByAimId(id);
        if (aim == null) {
            log.error(BadRequestReason.AIM_NOT_EXISTING.toString() +" id:"+id);
            throw new BadRequestException(BadRequestReason.AIM_NOT_EXISTING);
        }
        log.info("The aim with name: " + aim.getName()+" was loaded from the database. id: " +aim.getId());
        return aim;
    }

    /**
     * Gets the description of a specific aim.
     *
     * @param id The id of the aim.
     * @return The description.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public String getDescription(Long id){
        Aim aim = findAimById(id);
        if(aim.getDescription() != null){
            log.debug("Description of aim:"+id+" description"+ aim.getDescription());
            return aim.getDescription();
        }else{
            log.debug("No description for aim with id:" +id);
            return null;
        }
    }

}