package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.model.AimDto;
import de.unipassau.ep.team3.EP.services.AimService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * A RestController providing the endpoints subsequent to /aims .
 */
@Component
@RestController
@RequestMapping(value = "/aims")
@CrossOrigin(origins = "*")
public class AimController {

    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger( AimController.class.getName() );

    /**
     * The MIME type to use.
     */
    private static final String MIME_TYPE_JSON = "application/json";

    /**
     * The service to use to manage Aim entities.
     */
    private final AimService aimService;

    /**
     * Creates a new AimController object.
     *
     * @param aimService The aim controller's service to manage aim entities.
     */
    @Autowired
    public AimController(AimService aimService) {
        this.aimService = aimService;
    }

    /**
     * An endpoint to create a new aim
     *
     * @param aimDto The aim's data transfer object.
     */
    @PostMapping(value ="", consumes = MIME_TYPE_JSON)
    @ResponseStatus(HttpStatus.OK)
    public void createAim(@RequestBody AimDto aimDto){
        log.info("Got a POST request on /aims/");
        aimService.createNewAim(aimDto);
        log.info("Aim was created");
    }

    /**
     * An endpoint to get all aims.
     */
    @GetMapping(value = "")
    public Iterable<AimDto> getAll(){
        log.info("Got a GET request on /aims");
        return aimService.findAll();
    }

    /**
     * An endpoint to delete an aim.
     *
     * @param id The aimDto of the aim to delete.
     */
    @DeleteMapping(value = "/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id) {
        log.info("Got an PUT request on /aims/delete. ID of aimDto: "+ id);
        aimService.delete(id);
    }

    /**
     * An endpoint to update an aim.
     *
     * @param aimDto The aimDto to update from.
     */
    @PutMapping(value = "/update", consumes = MIME_TYPE_JSON)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody AimDto aimDto) {
        log.info("Got an PUT request on /aims/update. ID of aimDto: "+ aimDto.getId());
        aimService.update(aimDto);
    }

    /**
     * An endpoint to get the description of an aim.
     *
     * @param id The id of the aim.
     * @return The description.
     */
    @GetMapping(value = "/{id}/getDescription")
    public String getDescriptions(@PathVariable Long id){
        log.info("Got an GET request on /aims/"+id+"/getDescription");
        return aimService.getDescription(id);
    }
}