package de.unipassau.ep.team3.EP.model;

import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;


/**
 * Class for converting data transfer objects to model objects and back.
 */
public class DtoConverter {


    /**
     * Converts a user DTO to a user object.
     *
     * @param userDto The DTO to be converted.
     * @return The user.
     */
    public static User convertFromDto(UserDto userDto){
        User user = new User(userDto.getZim(),userDto.getName(),
                userDto.getPassword(), userDto.getVerificationToken());
        LocalDate localDate = LocalDate.of(userDto.getYearOfBirth(), userDto.getMonthOfBirth(),
                userDto.getDayOfBirth());
        user.setHash((new BCryptPasswordEncoder()).encode(userDto.getPassword()));
        user.setBirthday(localDate);
        return user;
    }

    /**
     * Converts the user to an user dto.
     *
     * @param user The user to be converted.
     * @return The user dto.
     */
    public static UserDto convertToDto(User user){
        UserDto userDto = new UserDto(user.getZim(),user.getName());
        userDto.setHeight(user.getHeight());
        userDto.setWeight(user.getWeight());
        if(user.getAim()!= null){
        userDto.setAimId(user.getAim().getId());}
        if(user.getExperience() != null){
            userDto.setExperienceId(user.getExperience().getId());
        }
        if(user.getSchedule() != null){
            userDto.setScheduleId(user.getSchedule().getId());
        }
        if(user.getGender() != null){
            userDto.setGender(user.getGender().toString());
        }
        return userDto;
    }

    /**
     * Converts an aim DTO to an aim object.
     *
     *  @param aimDto The DTO to be converted.
     * @return The aim.
     */
    public static Aim convertFromDto(AimDto aimDto){
        Aim aim = new Aim (aimDto.getName());
        aim.setDescription(aimDto.getDescription());
        aim.setFurtherInformation(aimDto.getFurtherInformation());

        return aim;
    }

    /**
     * Converts an aim object to an aim DTO.
     *
     * @param aim The aim to be converted.
     * @return The aim DTO
     */
    public static AimDto convertToDto(Aim aim){

        long superAimId;
        if(aim.getSuperAim() == null){
            superAimId = 0;
        }else{
            superAimId = aim.getSuperAim().getId();
        }
        AimDto aimDto = new AimDto(aim.getName());
        aimDto.setSuperAimId(superAimId);
        aimDto.setDescription(aim.getDescription());
        aimDto.setFurtherInformation(aim.getFurtherInformation());
        aimDto.setId(aim.getId());
        aimDto.setEditorZim(aim.getEditor().getZim());
        long [] subAimsIDs = new long [aim.getSubAims().size()];
        List<Aim> subAims = aim.getSubAims();
        for(int i = 0; i < subAimsIDs.length;++i){
            subAimsIDs[i] = subAims.get(i).getId();
        }
        aimDto.setSubAims(subAimsIDs);
        aimDto.setSubAimsCount(subAimsIDs.length);
        return aimDto;
    }

    /**
     * Converts an exercise DTO to an exercise object.
     *
     * @param exerciseDto The exercise DTO.
     * @return The exercise object.
     */
    public static Exercise convertFromDto(ExerciseDto exerciseDto){
        Exercise exercise = new Exercise(exerciseDto.getName());
        exercise.setDescription(exerciseDto.getDescription());
        if (exerciseDto.getVideoURL() == null){
            exercise.setVideo(null);
        }else
        if(exerciseDto.getVideoURL().equals("")) {
            exercise.setVideo(null);

        }else
        if(exerciseDto.getVideoURL() != null){
            try{
                URL video = new URL (exerciseDto.getVideoURL());
                exercise.setVideo(video);

            } catch (MalformedURLException e) {
                throw new BadRequestException(BadRequestReason.INVALID_URL);  } }


        exercise.setExecutions(exerciseDto.getExecutions());
        return exercise;
    }

    /**
     * Converts an exercise object to an exercise DTO.
     *
     * @param exercise The exercise object.
     * @return The exercise DTO.
     */
    public static ExerciseDto convertToDto(Exercise exercise){
        ExerciseDto exerciseDto = new ExerciseDto(exercise.getName());
        exerciseDto.setId(exercise.getId());
        exerciseDto.setDescription(exercise.getDescription());
        if(exercise.getVideo() != null){
            exerciseDto.setVideoURL(exercise.getVideo().toString());}
        exerciseDto.setExecutions(exercise.getExecutions());
        exerciseDto.setEditor(exercise.getEditor().getZim());

        int size = exercise.getAlternativeExercises().size();
        long [] alternatives = new long[size];
        for(int i = 0; i < size; ++i){
            alternatives[i] = exercise.getAlternativeExercises().get(i).getId();
        }
        exerciseDto.setAlternativeIds(alternatives);
        return exerciseDto;
    }

    /**
     * Converts an experience LeveL DTO to an experienceLevel object.
     *
     * @param experienceDto The experienceLevel DTO.
     * @return The experienceLevel object.
     */
    public static Experience convertFromDto(ExperienceDto experienceDto){
        Experience experience = new Experience(experienceDto.getName());
        experience.setDescription(experienceDto.getDescription());
        return experience;
    }

    /**
     * Converts an experience object to an experience DTO.
     *
     * @param experience The experience object.
     * @return experience DTO.
     */
    public static ExperienceDto convertToDto(Experience experience){
        ExperienceDto experienceDto = new ExperienceDto(experience.getName());
        experienceDto.setDescription(experience.getDescription());
        experienceDto.setEditor(experience.getEditor().getZim());
        experienceDto.setId(experience.getId());
        return experienceDto;
    }

    /**
     * Converts an training schedule object to an training schedule DTO.
     *
     * @param schedule The training schedule object.
     * @return The training schedule DTO.
     */
    public static ScheduleDto convertToDto(Schedule schedule){
        ScheduleDto scheduleDto = new ScheduleDto();

        if(schedule.getAim() != null){
            scheduleDto.setAimId(schedule.getAim().getId());
        }

        if(schedule.getLevel() != null){
            scheduleDto.setLevelId(schedule.getLevel().getId());
        }

        if(schedule.getAimName() != null){
            scheduleDto.setAimName(schedule.getAimName());
        }

        if(schedule.getExperienceName()!= null){
            scheduleDto.setExperienceName(schedule.getExperienceName());
        }
        scheduleDto.setGender(schedule.getGender().toString());
        if(schedule.getEditor() != null){
        scheduleDto.setEditorZim(schedule.getEditor().getZim());}
        scheduleDto.setId(schedule.getId());
        scheduleDto.setUnlocked(schedule.isUnlocked());

        int size = schedule.getRank().size();
        scheduleDto.setRank(schedule.getRank().toArray(new String[size]));
        size = schedule.getRankIndexes().size();
        int [] rankIndexes = new int[size];
        for(int i = 0; i < size; ++i){
            rankIndexes[i]   = schedule.getRankIndexes().get(i);
        }
        scheduleDto.setRankIndexes(rankIndexes);
        size = schedule.getExecutions().size();
        scheduleDto.setExecutions(schedule.getExecutions().toArray(new String [size]));

        // Converting the list of exercises to an array of exercise DTOs.
        size = schedule.getExercises().size();
        ExerciseDto [] exerciseDtos = new ExerciseDto[size];
        for(int i = 0; i < size; ++i){
            Exercise exercise = schedule.getExercises().get(i);
            exerciseDtos[i] = convertToDto(exercise);
        }
        scheduleDto.setExercises(exerciseDtos);

        //Setting the indexes array
        if(schedule.getWrappers() != null){
            size = schedule.getWrappers().size();
            ExerciseWrapperDto [] wrapperDtos = new ExerciseWrapperDto[size];
            for(int i = 0; i < size; ++i){
                wrapperDtos[i] = convertToDto(schedule.getWrappers().get(i));
            }
            scheduleDto.setWrapper(wrapperDtos);}


        // Converting the list of alternative exercises to an array of exercise DTOs.
        size = schedule.getAlternativeExercises().size();
        ExerciseDto [] alternativeExerciseDtos = new ExerciseDto[size];
        for(int i = 0; i < size; ++i){
            Exercise exercise = schedule.getAlternativeExercises().get(i);
            alternativeExerciseDtos[i] = convertToDto(exercise);
        }
        scheduleDto.setAlternativeExercises(alternativeExerciseDtos);
        scheduleDto.setCurrentUnit(schedule.getCurrentUnit());
        scheduleDto.setMaxUnit(schedule.getMaxUnit());

        return scheduleDto;
    }

    /** Converts an experience object to an experience DTO.
     *
     * @param wrapperDto The exercise wrapper object.
     * @return The exercise wrapper DTO.
     */
    public static ExerciseWrapper convertFromDto(ExerciseWrapperDto wrapperDto){
        return new ExerciseWrapper(wrapperDto.getSpeed(),
                wrapperDto.getPause(),wrapperDto.getIteration(),wrapperDto.getLoad());
    }

    /** Converts an experience DTO to an experience object.
     *
     * @param wrapper The exercise wrapper DTO.
     * @return The exercise wrapper object.
     */
     private static ExerciseWrapperDto convertToDto(ExerciseWrapper wrapper){
        ExerciseWrapperDto wrapperDto = new ExerciseWrapperDto(wrapper.getSpeed(),
                wrapper.getPause(),wrapper.getIteration(),wrapper.getLoad());
        wrapperDto.setId(wrapper.getId());
        return wrapperDto;
    }
}