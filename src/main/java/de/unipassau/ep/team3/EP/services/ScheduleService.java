package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.database.*;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScheduleService {

    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger(ScheduleService.class.getName());

    /**
     * The highest rank allowed :scheduleDto.getRankindexes.length() -1;
     */
    private int maxRank;

    /**
     * The lowest rank allowed 0;
     */
    private int minRank = 0;

    /**
     * The format a rank must match.
     */
    private String rankFormat = "(^[A-Z][1-9][0-9]*$)";
    /**
     * The database repository for training schedule.
     */
    private final ScheduleRepository scheduleRepository;

    /**
     * The database repository for experience level.
     */
    private final ExperienceRepository experienceRepository;

    /**
     * The service for experience level.
     */
    private final ExperienceService experienceService;

    /**
     * The database repository for aims.
     */
    private final AimRepository aimRepository;

    /**
     * The service for aims.
     */
    private final AimService aimService;

    /**
     * The database repository for exercises.
     */
    private final ExerciseRepository exerciseRepository;

    /**
     * The service for exercises
     */
    private final ExerciseService exerciseService;

    /**
     * The database repository for users.
     */
    private final UserRepository userRepository;


    /**
     * The database repository for exercise wrapper
     */
    private final ExerciseWrapperRepository wrapperRepository;

    /**
     * Creates a new training schedule service.
     *
     * @param scheduleRepository   The schedules service's schedule repository automatically injected.
     * @param experienceRepository The schedule service's experience repository automatically injected.
     * @param experienceService    The schedule service's experience services automatically injected.
     * @param aimRepository        The schedule service's aim repository automatically injected.
     * @param aimService           The schedule service's aim services automatically injected.
     * @param exerciseRepository   The schedules service's exercise repository automatically injected.
     * @param exerciseService      The schedule service's exercise services automatically injected.
     * @param userRepository       The schedule service's user repository automatically injected.
     * @param wrapperRepository    The schedule service's wrapper repository automatically injected.
     */
    public ScheduleService(ExerciseService exerciseService, ScheduleRepository scheduleRepository, ExperienceRepository experienceRepository, ExperienceService experienceService, AimRepository aimRepository, AimService aimService, ExerciseRepository exerciseRepository, UserRepository userRepository, ExerciseWrapperRepository wrapperRepository) {
        this.scheduleRepository = scheduleRepository;
        this.experienceRepository = experienceRepository;
        this.experienceService = experienceService;
        this.aimRepository = aimRepository;
        this.aimService = aimService;
        this.exerciseRepository = exerciseRepository;
        this.exerciseService = exerciseService;
        this.userRepository = userRepository;
        this.wrapperRepository = wrapperRepository;
    }

    /**
     * Creates a schedule entity from a schedule DTO
     *
     * @param scheduleDto The schedule to create from.
     * @return The created schedule as DTO
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    @Transactional
    public Long createNewSchedule(ScheduleDto scheduleDto) {
        Aim aim = aimService.findAimById(scheduleDto.getAimId());
        Experience experience = experienceService.findExperienceById(scheduleDto.getLevelId());

        Schedule schedule = new Schedule(aim, experience, testValidGender(scheduleDto.getGender()));
        log.debug("A new schedule with aim " + aim.getId() + ", experience " + experience.getId() + " and gender " + scheduleDto.getGender() + "was created");

        aim.getSchedules().add(schedule);
        experience.getSchedules().add(schedule);
        schedule.setUnlocked(true);
        setScheduleConfiguration(scheduleDto, schedule);

        log.debug("The configuration for the schedule were set");
        Schedule dbSchedule = scheduleRepository.save(schedule);

        setReferencesFromExerciseToSchedule(dbSchedule);
        scheduleRepository.save(dbSchedule);
        log.info("The training schedule with id " + dbSchedule.getId() + "was saved in the database.");
        return dbSchedule.getId();
    }

    /**
     * Sets references from the exercises to the training schedule.
     *
     * @param dbSchedule The schedule to set references to.
     */
    void setReferencesFromExerciseToSchedule(Schedule dbSchedule) {
        for (Exercise e : dbSchedule.getExercises()) {
            e.getScheduleList().add(dbSchedule);
            exerciseRepository.save(e);
            log.debug("For exercise " + e.getId() + " the schedule " + dbSchedule.getId() + " was added to the schedule list");
        }

        for (Exercise e : dbSchedule.getAlternativeExercises()) {
            e.getScheduleListAlternative().add(dbSchedule);
            exerciseRepository.save(e);
            log.debug("For alternative exercise " + e.getId() + " the schedule " + dbSchedule.getId() + " was added to the schedule list");
        }
        log.info("The references from the schedules exercises to the schedule were set");
    }

    /**
     * Sets the configuration in the training schedule.
     *
     * @param scheduleDto The scheduleDTO to get the configuration from.
     * @param schedule The schedule to set the configurations.
     */
    private void setScheduleConfiguration(ScheduleDto scheduleDto, Schedule schedule) {
        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        User editor = findUserByZim(currentUser);
        schedule.setEditor(editor);

        if (scheduleDto.getRank() != null) {
            testRank(scheduleDto);
            schedule.setRank(new LinkedList<>(Arrays.asList(scheduleDto.getRank())));
        }
        log.debug("The rank was set for the schedule " + schedule.getId());


        if (scheduleDto.getRankIndexes() != null) {
            testRankIndexes(scheduleDto);
            List<Integer> rankIndexes = new LinkedList<>();
            for (int i : scheduleDto.getRankIndexes()) {
                rankIndexes.add(i);
            }
            schedule.setRankIndexes(rankIndexes);
        }
        log.debug("The rank indexes was set for the schedule " + schedule.getId());

        schedule.setExecutions(new LinkedList<>(Arrays.asList(scheduleDto.getExecutions())));

        if (scheduleDto.getExerciseIds() != null && scheduleDto.getAlternativeExerciseIds() != null) {
            if (scheduleDto.getExerciseIds().length != scheduleDto.getAlternativeExerciseIds().length) {
                log.error(BadRequestReason.UNEQUAL_NUMBER_EXERCISES_ALTERNATIVES.toString());
                throw new BadRequestException(BadRequestReason.UNEQUAL_NUMBER_EXERCISES_ALTERNATIVES);
            }

            // The exercises are added to the training schedule
            if (scheduleDto.getExerciseIds().length > 0) {
                long[] exerciseIds = scheduleDto.getExerciseIds();
                for (long exercise1 : exerciseIds) {
                    if (exercise1 > 1) {
                        Exercise exercise = exerciseService.findExerciseById(exercise1);
                        schedule.getExercises().add(exercise);
                        log.debug("The exercise " + exercise.getId() + " was added to the schedule");
                    } else if (exercise1 == 1) {
                        log.error(BadRequestReason.DUMMY_EXERCISE_ID.toString());
                        throw new BadRequestException(BadRequestReason.DUMMY_EXERCISE_ID);
                    } else {
                        log.error(BadRequestReason.INVALID_EXERCISE_ID.toString() + " " + exercise1);
                        throw new BadRequestException(BadRequestReason.INVALID_EXERCISE_ID);
                    }
                }
            }
        }

        if (scheduleDto.getWrapper() != null) {

            // The wrappers are added to the training schedule
            if (scheduleDto.getWrapper().length > 0) {
                ExerciseWrapperDto[] wrapperDtos = scheduleDto.getWrapper();
                for (int i = 0; i < scheduleDto.getWrapper().length; ++i) {
                    ExerciseWrapper wrapper = wrapperRepository.save(DtoConverter.convertFromDto(wrapperDtos[i]));
                    schedule.getWrappers().add(wrapper);
                    log.debug("The wrapper " + wrapper.getId() + " was added to the schedule");
                }
            }
        }


        // The alternative exercises are added to the training schedule
        if (scheduleDto.getAlternativeExerciseIds() != null) {
            long[] alternativeExercises = scheduleDto.getAlternativeExerciseIds();
            List<Exercise> exercises = schedule.getExercises();
            for (int i = 0; i < alternativeExercises.length; ++i) {
                if (alternativeExercises[i] > 0) {
                    Exercise exercise = exerciseService.findExerciseById(alternativeExercises[i]);
                    if (exercise.getId().equals(exercises.get(i).getId())) {
                        log.error(BadRequestReason.ALTERNATIVE_EXERCISE_OWN_EXERCISE.toString());
                        throw new BadRequestException(BadRequestReason.ALTERNATIVE_EXERCISE_OWN_EXERCISE);

                    }
                    schedule.getAlternativeExercises().add(exercise);
                    log.debug("The alternative exercise " + exercise.getId() + " was added to the schedule");


                } else {
                    log.error(BadRequestReason.EXERCISE_NOT_EXISTING.toString());
                    throw new BadRequestException(BadRequestReason.EXERCISE_NOT_EXISTING);
                }
            }
        }

        schedule.setCurrentUnit(0);
        if (schedule.getWrappers().size() >0) {
            schedule.setMaxUnit(schedule.getWrappers().get(0).getSpeed().length);
        } else {
            schedule.setMaxUnit(0);
        }
        log.info("The current unit an the max unit " + schedule.getMaxUnit() + " were set");


    }

    /**
     * Gets all training schedules with no user.
     *
     * @return A String representing the training schedules.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public String findAllUnassigned() {
        LinkedList<Schedule> blancoSchedules = scheduleRepository.findUnassigned();
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        while (blancoSchedules.peekFirst() != null) {
            Schedule schedule = blancoSchedules.pollFirst();
            sb.append("{");
            Long id;
            if(schedule.getId() == null){
                id = 0L;
            }else{
                id = schedule.getId();
            }
            sb.append("\"id\"" + ":" + "\"").append(id).append("\"");
            sb.append(",");
            String name;
            if (schedule.getAim() == null) {
                name = "No aim";
            } else {
                name = schedule.getAim().getName();
            }
            sb.append("\"aimName\"" + ":" + "\"").append(name).append("\"");
            sb.append(",");
            if (schedule.getLevel() == null) {
                name = "No experience level";
            } else {
                name = schedule.getLevel().getName();
            }
            sb.append("\"experienceName\"" + ":" + "\"").append(name).append("\"");
            sb.append(",");
            if (schedule.getGender() == null) {
                name = "No gender";
            } else {
                name = schedule.getGender().toString();
            }
            sb.append("\"gender\"" + ":" + "\"").append(name).append("\"");
            sb.append("}");
            if (blancoSchedules.peekFirst() != null) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * A method to test if the ranks matches the rank format.
     *
     * @param scheduleDto The schedule dto to get the ranks from.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    private void testRank(ScheduleDto scheduleDto) {
        String[] ranks = scheduleDto.getRank();
        for (String i : ranks) {
            if (!i.matches(rankFormat)) {
                log.error(BadRequestReason.INVALID_RANK_FORMAT.toString() + " " + i);
                throw new BadRequestException(BadRequestReason.INVALID_RANK_FORMAT);
            }
        }
    }


    /**
     * A method to test if the given ranks are to high or to low.
     *
     * @param scheduleDto The schedule to get the rank indexes of.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    private void testRankIndexes(ScheduleDto scheduleDto) {
        int[] rankIndexes = scheduleDto.getRankIndexes();
        maxRank = rankIndexes.length - 1;
        for (int rankIndex : rankIndexes) {
            if (rankIndex > maxRank) {
                log.error(BadRequestReason.RANK_TOO_HIGH.toString() + " " + rankIndex);
                throw new BadRequestException(BadRequestReason.RANK_TOO_HIGH);
            } else if (rankIndex < minRank) {
                log.error(BadRequestReason.RANK_TOO_LOW.toString() + " " + rankIndex);
                throw new BadRequestException(BadRequestReason.RANK_TOO_LOW);
            }
        }
    }

    /**
     * A method to test if the given gender is a valid gender.
     * @param gender The string to be parsed in Gender enum.
     * @return The valid gender.
     */
    Gender testValidGender(String gender) {
        try {
            return Gender.valueOf(gender);
        } catch (IllegalArgumentException e) {
            log.error(e + " " + gender + ", " + BadRequestReason.INVALID_GENDER.toString());
            throw new BadRequestException(BadRequestReason.INVALID_GENDER);
        }

    }

    /**
     * Gets an exercise from the database.
     *
     * @param id The ID of the exercise.
     * @return The exercise.
     * @throws BadRequestException Exercise with this ID doesn't exists.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public ScheduleDto getScheduleById(Long id) {
        return DtoConverter.convertToDto(findScheduleById(id));
    }

    /**
     * Finds an schedule by its id.
     *
     * @param id The id of the schedule.
     * @return The schedule.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    private Schedule findScheduleById(Long id) {
        Schedule schedule = scheduleRepository.findByTrainingScheduleId(id);
        if (schedule == null) {
            log.error(BadRequestReason.SCHEDULE_NOT_EXISTING.toString() + " " + id);
            throw new BadRequestException(BadRequestReason.SCHEDULE_NOT_EXISTING);
        }
        log.info("The schedule " + schedule.getId() + " was loaded from the database");
        return schedule;
    }

    /**
     * A method to delete an training schedule.
     *
     * @param id The id of the training schedule to deleted.
     */
    @Modifying
    @Transactional
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public void delete(Long id) {
        scheduleRepository.delete(removeAllFromSchedule(id));
     }

    /**
     * A method to delete references to the training schedule.
     *
     * @param id The id of the schedule.
     * @return The cleared schedule.
     */
    private Schedule removeAllFromSchedule(Long id) {
        Schedule toDelete = findScheduleById(id);
        if (toDelete.getUser() != null) {
            log.error(BadRequestReason.SCHEDULE_IS_ASSIGNED.toString() + " " + id);
            throw new BadRequestException(BadRequestReason.SCHEDULE_IS_ASSIGNED);
        }

        if(toDelete.getAim() != null){
        toDelete.getAim().getSchedules().remove(toDelete);}
        if(toDelete.getLevel() != null){
        toDelete.getLevel().getSchedules().remove(toDelete);}
        log.debug("Aim and Experience were removed from the schedule");

        for(Exercise exercise:toDelete.getExercises()){
            exercise.getScheduleList().remove(toDelete);
        }
        toDelete.setExercises(new LinkedList<>());
        log.debug(" The exercises were removed from the schedule");

        List<Long> wrappersId = new LinkedList<>();
        for (ExerciseWrapper wrapper : toDelete.getWrappers()) {
            wrappersId.add(wrapper.getId());
        }

        toDelete.setWrappers(new LinkedList<>());

        for(Long wrapperId: wrappersId){
            ExerciseWrapper wrapper = findWrapperById(wrapperId);
            wrapperRepository.delete(wrapper);
        }
        log.debug("The wrappers were removed from the schedule");
        //The alternative exercise and its wrapper

        for (Exercise exercise : toDelete.getAlternativeExercises()) {
            exercise.getScheduleListAlternative().remove(toDelete);
        }
        toDelete.setAlternativeExercises(new LinkedList<>());
        log.debug("The alternative exercises were removed");

        return toDelete;
    }

    /**
     * Gets an exercise wrapper by its id.
     *
     * @param id The id of the exercise wrapper to get from the database.
     * @return The exercise wrapper
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public ExerciseWrapper findWrapperById(Long id) {
        ExerciseWrapper wrapper = wrapperRepository.findByWrapperId(id);
        if (wrapper == null) {
            log.error(BadRequestReason.WRAPPER_NOT_EXISTING.toString() + " " + id);
            throw new BadRequestException(BadRequestReason.WRAPPER_NOT_EXISTING);
        }
        log.info("The wrapper " + wrapper.getId() + " was loaded from the database.");
        return wrapper;
    }

    /**
     * A method to update an training schedule.
     *
     * @param dto The schedule dto to update from.
     */
    @Transactional
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public void update(ScheduleDto dto) {
        Schedule toUpdate = removeAllFromSchedule(dto.getId());
        log.debug("Everything from the schedule " + dto.getId() + "was removed");
        Aim aim = aimService.findAimById(dto.getAimId());
        toUpdate.setAim(aim);
        log.debug("The aim " + aim.getId() + " was set for the schedule");

        Experience experience = experienceService.findExperienceById(dto.getLevelId());
        toUpdate.setLevel(experience);
        log.debug("The experience " + experience.getId() + " was set for the schedule");

        toUpdate.setGender(testValidGender(dto.getGender()));
        log.debug("The gender " + toUpdate.getGender() + " was set for the schedule");
        aim.getSchedules().add(toUpdate);
        experience.getSchedules().add(toUpdate);

        toUpdate.setUnlocked(dto.isUnlocked());
        setScheduleConfiguration(dto, toUpdate);
        setReferencesFromExerciseToSchedule(toUpdate);
        scheduleRepository.save(toUpdate);
        log.info("The updates for the schedule were saved in teh database");
    }

    /**
     * Creates a deep Copy of an training schedule. Replaces Aim/Experience id with its names.
     *
     * @param toCopy The schedule to be copied.
     * @return The deep copy.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public Schedule deepCopy(Schedule toCopy){
        Schedule deepCopy = new Schedule(toCopy.getAim().getName(),toCopy.getLevel().getName(),toCopy.getGender());
        log.debug("The deep copy was created");

        LinkedList<String> rank = new LinkedList<>(toCopy.getRank());
        deepCopy.setRank(rank);
        log.debug("The ranks were copied");

        LinkedList<Integer> index = new LinkedList<>(toCopy.getRankIndexes());
        deepCopy.setRankIndexes(index);
        log.debug("The rank indexes were copied");

        rank = new LinkedList<>(toCopy.getExecutions());
        deepCopy.setExecutions(rank);
        log.debug("The executions were copied");

        if (toCopy.getExercises().size() > 0) {
            for (Exercise exercise : toCopy.getExercises()) {
                if (exercise.getId() > 1) {
                    deepCopy.getExercises().add(exercise);
                    log.debug("The exercise " + exercise.getId() + " was added to the deep copy");
                } else if (exercise.getId() == 1) {
                    log.error(BadRequestReason.DUMMY_EXERCISE_ID.toString());
                    throw new BadRequestException(BadRequestReason.DUMMY_EXERCISE_ID);
                } else {
                    log.error(BadRequestReason.INVALID_EXERCISE_ID.toString() + " " + exercise.getId());
                    throw new BadRequestException(BadRequestReason.INVALID_EXERCISE_ID);
                }
            }
        }


        if (toCopy.getWrappers() != null) {

            // The wrappers are added to the training schedule
            if (toCopy.getWrappers().size() > 0) {

                for (int i = 0; i < toCopy.getWrappers().size(); ++i) {
                    ExerciseWrapper wrapperToCopy = toCopy.getWrappers().get(i);
                    ExerciseWrapper wrapper = wrapperRepository.save(new ExerciseWrapper(wrapperToCopy.getSpeed(), wrapperToCopy.getPause(),
                            wrapperToCopy.getIteration(), wrapperToCopy.getLoad()));
                    deepCopy.getWrappers().add(wrapper);
                    log.debug("The wrapper " + wrapper.getId() + " was created and added to the deep copy");
                }
            }
        }

        // The alternative exercises are added to the training schedule
        if (toCopy.getAlternativeExercises().size() > 0) {
            for(Exercise exercise:toCopy.getAlternativeExercises())
                if (exercise.getId() > 0) {
                    deepCopy.getAlternativeExercises().add(exercise);
                    log.debug("The alternative exercise " + exercise.getId() + " was added to the deep copy");


                }
        }
        deepCopy.setCurrentUnit(toCopy.getCurrentUnit());
        deepCopy.setMaxUnit(toCopy.getMaxUnit());
        log.info("The deep copy was created");
        return deepCopy;
    }

    /**
     * Gets an experience level from the database.
     *
     * @param zim The zim ID of the user.
     * @return The user.
     * @throws BadRequestException User with this zim ID doesn't exists.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    User findUserByZim(String zim) {
        User user = userRepository.findByZim(zim);
        if (user == null) {
            log.error(BadRequestReason.USER_NOT_EXISTING.toString());
            throw new BadRequestException(BadRequestReason.USER_NOT_EXISTING);
        }
        log.info("The user " + user.getZim() + " was loaded from the database");
        return user;
    }

    /**
     * Gets the aim name of a schedule.
     *
     * @param id The id of the schedule.
     * @return The name.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public String getAimNameOfSchedule(Long id) {
        Schedule schedule = findScheduleById(id);
        if (schedule.getUser() == null) {
            log.error(BadRequestReason.NOT_A_USER_SCHEDULE.toString());
            throw new BadRequestException(BadRequestReason.NOT_A_USER_SCHEDULE);
        }
        log.info("The schedule " + schedule.getId() + " has the aim " + schedule.getAimName());
        return schedule.getAimName();
    }

    /**
     * Gets the progress of a schedule.
     *
     * @param id The id of the schedule.
     * @return The progress
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public String getProgress(Long id) {
        Schedule schedule = findScheduleById(id);
        String sb = "{" +
                "\"currentUnit\"" + ":" + "\"" + schedule.getCurrentUnit() + "\"" +
                "," +
                "\"maxUnit\"" + ":" + "\"" + schedule.getMaxUnit() + "\"" +
                "}";
        log.info("The schedule has the current unit " + schedule.getCurrentUnit() + " and max unit" + schedule.getMaxUnit());
        return sb;
    }

    /**
     * A method to update an training schedule after a training or to delete it.
     *
     * @param scheduleDto The schedule to get the updates from.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public void endUnit(ScheduleDto scheduleDto){
        Schedule schedule = findScheduleById(scheduleDto.getId());

        if(scheduleDto.getCurrentUnit() == schedule.getMaxUnit()){
           schedule.getUser().setSchedule(null);
            schedule.setUser(null);
            schedule = scheduleRepository.save(schedule);
            delete(schedule.getId());
        } else{
            schedule.setCurrentUnit(scheduleDto.getCurrentUnit());

            for(ExerciseWrapperDto wrapperDto: scheduleDto.getWrapper()){
                ExerciseWrapper wrapper = findWrapperById(wrapperDto.getId());
                wrapper.setIteration(wrapperDto.getIteration());
                wrapperRepository.save(wrapper);
            }
            scheduleRepository.save(schedule);
        }
    }
}