package de.unipassau.ep.team3.EP.database;

import de.unipassau.ep.team3.EP.model.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExerciseRepository extends JpaRepository<Exercise,Long> {
    /**
     * Gets an exercise by its id.
     *
     * @param id The exercise's id.
     * @return The corresponding exercise or {@code null}.
     */
    @Query("SELECT e FROM Exercise AS e WHERE e.id = :id")
    Exercise findByExerciseId(@Param("id") long id);

    /**
     * Gets a list of exercises by a exercises name.
     *
     * @param name The exercise's name.
     * @return The corresponding exercises or {@code null}.
     */
    @Query("SELECT e FROM Exercise AS e WHERE e.name =:name")
    List<Exercise> findByName(@Param("name") String name);

    /**
     * Gets the highest exercise id in the database.
     *
     * @return The highest id.
     */
    @Query("SELECT id FROM Exercise WHERE id=(SELECT max(id) FROM Exercise)")
    Long findHighestId();

    /**
     * Gets all exercises from the database except the dummy exercise.
     *
     * @return A list of all not dummyExercises.
     */
    @Query("SELECT e FROM Exercise AS e WHERE e.id > 1 AND e.assigned = false " )
    List<Exercise> findAllButDummyExercise ();


}

