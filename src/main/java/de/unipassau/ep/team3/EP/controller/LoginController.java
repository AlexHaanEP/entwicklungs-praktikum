package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * A restController providing the endpoints subsequent to /login .
 */
@Component
@RestController
@RequestMapping(value = "/login")
@CrossOrigin(origins = "*")
public class LoginController {

    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger(LoginController.class.getName() );

    /**
     * The user service to use to manage user entities.
     */
    private final UserService userService;

    /**
     * Creates a new login controller object.
     *
     * @param userService The login controller's user service.
     */
    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    /**
     * An endpoint to log the user in and setting the last login timestamp.
     */
    @GetMapping(value="")
    @ResponseStatus(HttpStatus.OK)
    public void login() {
        log.info("Got a GET request on /login");
        String zim = SecurityContextHolder.getContext().getAuthentication().getName();
        userService.setCurrentLastLogin(zim);
        log.info("The user:" +zim+" was logged in and the last login was updated");
    }
}
