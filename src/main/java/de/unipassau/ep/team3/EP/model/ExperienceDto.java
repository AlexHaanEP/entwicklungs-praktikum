package de.unipassau.ep.team3.EP.model;


/**
 * Data transfer object class for the {@Link experience} model.
 */
public class ExperienceDto {

    /**
     * The ID of the experience level.
     */
    private Long id;

    /**
     * The name of the experience level.
     */
    private String name;

    /**
     * The description of the experience level.
     */
    private String description;

    /**
     * The user who edited the experience the latest.
     */
    private String editor;

    /**
     * Creates a new experience level.
     */
    public ExperienceDto() {}

    /**
     * Creates a new experience level.
     */
    public ExperienceDto(String name){
        this.name = name;
    }

    /**
     * Creates a new experience level.
     */
    public ExperienceDto(String name, String description) {
        this.name = name;
        this.description = description;
    }

    //Getter & Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }
}
