package de.unipassau.ep.team3.EP.model;

public enum Gender {
    /**
     * The user is from the gender male.
     */
    MALE ,

    /**
     * The user is from the gender female.
     */
    FEMALE,

    /**
     * If a training schedule is both, male and female.
     */
    BOTH,
}
