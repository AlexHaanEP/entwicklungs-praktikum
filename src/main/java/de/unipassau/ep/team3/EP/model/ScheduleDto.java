package de.unipassau.ep.team3.EP.model;


/**
 * Data transfer object class for the Schedule model.
 */
public class ScheduleDto {

    /**
     * The ID of the training schedule
     */
    private Long id;

    /**
     * The aim ID of the training schedule.
     */
    private long aimId;

    private String aimName;

    /**
     * The experience level of the training schedule
     */
    private long levelId;

    private String experienceName;

    /**
     * The gender to assign the plan to a user with this gender.
     */
    private String gender;


    /**
     * The user who edited the training schedule last.
     */
    private String editorZim;

    /**
     * The array of ranks for the exercises.
     */
    private String [] rank;

    /**
     * The array of indexes of the ranks sorted ascending.
     */
    private int [] rankIndexes;

    /**
     * The array of ids of exercises in the training schedule.
     */
    private long[] exerciseIds;

    /**
     * The array of exercise DTOS in the training schedule.
     */
    private ExerciseDto [] exercises;

    /**
     * The array of executions for the exercises.
     */
    private String [] executions;

    /**
     * The array of IDS of wrappers for the of the exercises
     */
    private ExerciseWrapperDto [] wrapper;

    /**
     * The array of ids of alternative exercises in the training schedule.
     */
    private long[] alternativeExerciseIds;

    /**
     * The array of alternative exercise DTOS in the training schedule.
     */
    private ExerciseDto [] alternativeExercises;

    /**
     * If false, an exercise was deleted an a trainer has to update the trainings schedule
     */
    private int currentUnit;

    /**
     * The maximal number of units in the trainings schedules.
     */
    private int maxUnit;

    /**
     * False if the schedule needs to be modified, true otherwise.
     */
    private boolean unlocked;

    /**
     * Creates a training schedule.
     */
    public ScheduleDto() {}

    /**
     * Creates an training schedule.
     *
     * @param aimId The id of the aim.
     * @param levelId The id of the experience level.
     * @param gender The gender.
     */
    public ScheduleDto(long aimId, long levelId, String gender){
        this.aimId = aimId;
        this.levelId = levelId;
        this.gender = gender;
    }

    // Getter & Setter
    public long getAimId() {
        return aimId;
    }

    void setAimId(long aimId) {
        this.aimId = aimId;
    }

    public long getLevelId() {
        return levelId;
    }

    void setLevelId(long levelId) {
        this.levelId = levelId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEditorZim() {
        return editorZim;
    }

    void setEditorZim(String editorZim) {
        this.editorZim = editorZim;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String[] getRank() {
        return rank;
    }

    public void setRank(String[] rank) {
        this.rank = rank;
    }

    public int[] getRankIndexes() {
        return rankIndexes;
    }

    public void setRankIndexes(int[] rankIndexes) {
        this.rankIndexes = rankIndexes;
    }

    public String[] getExecutions() {
        return executions;
    }

    public void setExecutions(String[] executions) {
        this.executions = executions;
    }

    public ExerciseWrapperDto [] getWrapper() {
        return wrapper;
    }

    public void setWrapper(ExerciseWrapperDto [] wrapper) {
        this.wrapper = wrapper;
    }

    public long[] getExerciseIds() {
        return exerciseIds;
    }

    public void setExerciseIds(long[] exerciseIds) {
        this.exerciseIds = exerciseIds;
    }

    public ExerciseDto[] getExercises() {
        return exercises;
    }

    public void setExercises(ExerciseDto[] exercises) {
        this.exercises = exercises;
    }

    public long[] getAlternativeExerciseIds() {
        return alternativeExerciseIds;
    }

    public void setAlternativeExerciseIds(long[] alternativeExerciseIds) {
        this.alternativeExerciseIds = alternativeExerciseIds;
    }

    public ExerciseDto[] getAlternativeExercises() {
        return alternativeExercises;
    }

    void setAlternativeExercises(ExerciseDto[] alternativeExercises) {
        this.alternativeExercises = alternativeExercises;
    }


    public boolean isUnlocked() {
        return unlocked;
    }

    void setUnlocked(boolean unlocked) {
        this.unlocked = unlocked;
    }

    public String getAimName() {
        return aimName;
    }

    void setAimName(String aimName) {
        this.aimName = aimName;
    }

    public String getExperienceName() {
        return experienceName;
    }

    void setExperienceName(String experienceName) {
        this.experienceName = experienceName;
    }

    public int getCurrentUnit() {
        return currentUnit;
    }

    public void setCurrentUnit(int currentUnit) {
        this.currentUnit = currentUnit;
    }

    public int getMaxUnit() {
        return maxUnit;
    }



    public void setMaxUnit(int maxUnit) {
        this.maxUnit = maxUnit;
    }
}