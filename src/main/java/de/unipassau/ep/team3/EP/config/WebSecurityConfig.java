package de.unipassau.ep.team3.EP.config;

import de.unipassau.ep.team3.EP.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)

public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * The service to manage user entities.
     */
    private final UserService userService;

    /**
     * Creates a new WebSecurityCondig object.
     *
     * @param userService The userservice to fetch user details.
     */
    @Autowired
    public WebSecurityConfig(UserService userService) {
        this.userService = userService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().authorizeRequests()
                .antMatchers(HttpMethod.GET,"/", "/**/*.js",
                        "/**/*.css","/users**","/users/verify/**","/index.html","users").permitAll()
                .antMatchers(HttpMethod.POST,"/users").permitAll()
                .anyRequest().fullyAuthenticated()
                .and()
                .httpBasic()
                .and()
                //.csrf().disable().authorizeRequests().and()
                .csrf().ignoringAntMatchers("users","/users**","/aims","/users/verify/**","/aims/update/","/aims/delete/{id}","/schedules","/experience","/experiences"
                    ,"/experience/delete/{id}","/exercises/update","/exercises/delete/{id}","/schedules/{id}", "/exercises","/users/updateUser",
                "/{id}/getSchedule","/users/firstLogin", "/users/blockUser", "/users/changePersonalData", "users/{zim}/changePassword","/schedules/endUnit",
                "/users/selectedAim")
                .and()
                .csrf().ignoringAntMatchers("/login");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());
    }
}
