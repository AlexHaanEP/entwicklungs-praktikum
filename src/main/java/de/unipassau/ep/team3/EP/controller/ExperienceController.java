package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.model.ExperienceDto;
import de.unipassau.ep.team3.EP.services.ExperienceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * A RestController providing the endpoints subsequent to /experiences .
 */
@Component
@RestController
@RequestMapping(value = "/experiences")
@CrossOrigin(origins = "*")
public class ExperienceController {

    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger(ExperienceController.class.getName() );

    /**
     * The MIME type to use.
     */
    private static final String MIME_TYPE_JSON = "application/json";

    /**
     * The service to use to manage Experience entities.
     */
    private final ExperienceService experienceService;

    /**
     * Creates a new ExerciseController object.
     *
     * @param experienceService The aim controller's service to manage aim entities.
     */
    @Autowired
    public ExperienceController(ExperienceService experienceService) {
        this.experienceService = experienceService;
    }

    /**
     * An endpoint to create a new exercise.
     *
     * @param experienceDto The aim's data transfer object.
     */
    @PostMapping(value ="", consumes = MIME_TYPE_JSON)
    @ResponseStatus(HttpStatus.OK)
    public void createNewExperience(@RequestBody ExperienceDto experienceDto){
        log.info("Got a POST request on /experience/");
        experienceService.createNewExperienceLevel(experienceDto);
        log.info("Experience level was created");
    }

    /**
     * An endpoint to get all aims.
     */
    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<ExperienceDto> getAll(){
        log.info("Got a GET request on /experiences");
        return experienceService.findAll();
    }

    /**
     * An endpoint to update an experience level.
     *
     * @param levelDto The experience dto to update from.
     */
    @PutMapping(value = "", consumes = MIME_TYPE_JSON)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody ExperienceDto levelDto) {
        log.info("Got a PUT request on /experiences/update");
        experienceService.update(levelDto);
    }

    /**
     * An endpoint to delete an experience level.
     *
     * @param id The id of the experience level to delete.
     */
    @GetMapping(value = "/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id) {
        log.info("Got a GET request on /experiences/delete/"+id);
        experienceService.delete(id);
    }

    /**
     * An endpoint to get the description of an experience level.
     *
     * @param id The ID of the experience level.
     * @return The description
     */
    @GetMapping(value = "/{id}/getDescription")
    public String getDescriptions(@PathVariable Long id){
        log.info("Got a GET request on /experiences/"+id+"/getDescription");
        return experienceService.getDescription(id);
    }
}
