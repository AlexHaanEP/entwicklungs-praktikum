package de.unipassau.ep.team3.EP.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import java.util.logging.Logger;

/**
 * Exception to be thrown if the request is bad.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
    private static final Logger log = Logger.getLogger( BadRequestException.class.getName() );

    public BadRequestException(BadRequestReason reason) {
        super(reason.toString());
        log.config(reason.toString());
    }
}