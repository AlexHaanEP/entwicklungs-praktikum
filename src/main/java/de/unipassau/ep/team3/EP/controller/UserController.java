package de.unipassau.ep.team3.EP.controller;

import de.unipassau.ep.team3.EP.model.StatusDto;
import de.unipassau.ep.team3.EP.model.UserDto;
import de.unipassau.ep.team3.EP.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;


/**
 * A restcontroller providing the endpoints.txt subsequent to /users .
 */
@CrossOrigin(origins= {"*"}, maxAge = 4800 ,allowCredentials = "false" )
@Component
@RestController
@RequestMapping(value = "/users")
public class UserController {


    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger( UserController.class.getName() );

    /**
     * The service to use to manage User entities.
     */
    private final UserService userService;

    /**
     * The MIME type to use.
     */
    private static final String MIME_TYPE_JSON = "application/json";

    /**
     * Creates a user controller.
     *
     * @param userService The user controller'S service to manage user entities.
     */
    @Autowired
    public UserController( UserService userService){
        this.userService = userService;
    }

    /**
     * A method to create a new user.
     *
     * @param userDto The DTO to create a new user.
     */
    @PostMapping(value = "", consumes = MIME_TYPE_JSON)
    public void createUser(@RequestBody UserDto userDto){
        log.info("Got a POST request on /users/");
        userService.createNewUser(userDto);
        log.info("User was created");
    }

    /**
     * Verifies an already registered user using an unique verification token.
     *
     * @param verificationToken unigue verification token
     */
    @GetMapping(value = "/verify/{verificationToken}")
    public RedirectView verifyUser(@PathVariable String verificationToken) {
        log.info("Recieved GET request on /users/verify");
        userService.verifyUser(verificationToken);
        log.info("User verified");
        return new RedirectView("https://epws18.padim.fim.uni-passau.de/team3/");
    }

    /**
     * An endpoint to get the status of a specific user.
     *
     * @param id The user's zim ID.
     * @return The user status as string.
     */
    @GetMapping(value ="/{id}/status")
    public String getUserStatus(@PathVariable String id){
        log.info("Got a GET request on /users/"+id+"/status");
        return userService.getUserStatus(id);
    }

    /**
     * An endpoint to get all user from the database.
     *
     * @return All user represented as a String.
     */
    @GetMapping(value = "/getAllUser", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllUser() {
        log.info("Got a GET request on /users/getAllUser");
        return userService.getAllUser();
    }

    /**
     * An endpoint to update a user's status.
     *
     * @param statusDto The dto the get the user's ZIM-ID and the new status from.
     */
    @PostMapping(value = "/updateUser", consumes = MIME_TYPE_JSON)
    public void updateUserStatus(@RequestBody StatusDto statusDto) {
        log.info("Got a POST request on /users/updateUser");
        userService.updateUserStatus(statusDto);
    }

    /**
     * An endpoint to get the ID of the user'S training schedule.
     *
     * @param zim The user's ZIM-ID.
     * @return The training schedule's ID.
     */
    @GetMapping(value ="/{zim}/getSchedule")
    public Long getSchedule(@PathVariable String zim){
      log.info("Got a GET request on /users/"+zim+"/getSchedule");
      return  userService.getScheduleId(zim);
    }

    /**
     * An endpoint to get the ID of the user's aim.
     *
     * @param zim The user's zim ID.
     * @return The aim's ID.
     */
    @GetMapping(value ="/{zim}/getAim")
    public Long getAim(@PathVariable String zim){
        log.info("Got a GET request on /users/"+zim+"/getAim");
        return  userService.getAimId(zim);
    }

    /**
     * An endpoint to save user data und save a suitable training schedule for him.
     *
     * @param userDto The user DTO to get the data from.
     * @return The ID of the user'S training schedule
     */
    @PutMapping(value = "/firstLogin", consumes = MIME_TYPE_JSON)
    @ResponseStatus(HttpStatus.OK)
    public Long firstLogin(@RequestBody UserDto userDto) {
        log.info("Got a GET request on /users/firstLogin");
        return userService.firstLogin(userDto);
    }

    /**
     * An endpoint to change a user's password.
     *
     * @param zim The zim ID of the user to change its password.
     * @param password The password to change.
     */
    @PutMapping(value = "/{zim}/changePassword")
    @ResponseStatus(HttpStatus.OK)
    public void changePassword(@PathVariable String zim, @RequestBody String password){
        log.info("Got a PUT request on /users/"+zim+"/changePassword");
        userService.changePassword(zim,password);
    }

    /**
     * An endpoint to get an specific user.
     *
     * @param zim The ZIM-ID of the user.
     * @return The user dto.
     */
    @GetMapping(value = "/{zim}/getUser")
    public UserDto getUser(@PathVariable String zim){
        log.info("Got a GET request on /users/"+zim+"/getUser");
        return userService.getUser(zim);
    }

    /**
     * An endpoint to block a user.
     *
     * @param zim The zim ID of the user to be blocked.
     */
    @PutMapping(value ="/blockUser")
    public void blockUser(@RequestBody String zim){
        log.info("Got a PUT request on /users/blockUser");
        userService.blockUser(zim);
    }

    /**
     * An endpoint to update the aim of a user and select a new schedule for him/her.
     *
     * @param userDto The user dto to get the ZIM-ID and the aim from.
     * @return The id of the selected schedule.
     */
    @PutMapping(value = "/selectedAim",consumes = MIME_TYPE_JSON)
    public Long selectSchedule(@RequestBody UserDto userDto){
        log.info("Got a GET request on /users/selectedAim");
        return userService.selectSchedule(userDto);
    }

    /**
     * An endpoint to change userdata (height,weight,experience level).
     *
     * @param userDto The user DTO to get the changes from.
     */
    @PutMapping(value ="/changePersonalData")
    public void changePersonalData(@RequestBody UserDto userDto){
        log.info("Got a PUT request on /users/changePersonalData");
        userService.changePersonalData(userDto);
    }
}
