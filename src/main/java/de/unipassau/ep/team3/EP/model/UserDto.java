package de.unipassau.ep.team3.EP.model;

import java.time.LocalDate;

/**
 * Data transfer object class for the {@link User} model.
 */
public class UserDto {

    /**
     * The user'S zim ID.
     */
    private String zim;

    /**
     * The user'S name.
     */
    private String name;

    /**
     * The password of the user.
     */
    private String password;

    /**
     * The day on which the user was born.
     */
    private int dayOfBirth;

    /**
     * The month in which the user was born.
     */
    private int monthOfBirth;

    /**
     * The year in which the user was born.
     */
    private int yearOfBirth;

    /**
     * The token to verify a user.
     */
    private String verificationToken;

    /**
     * The id of the aim of the user.
     */
    private Long aimId;

    /**
     * The id of the experience of the user.
     */
    private Long experienceId;

    /**
     * The gender of teh user.
     */
    private String gender;

    /**
     * The schedule id of the user's schedule.
     */
    private Long scheduleId;

    /**
     * The height of the user in centimetre.
     */
    private int height;

    /**
     * The weight of the user in kilogramm.
     */
    private int weight;

    /**
     * Creates a new user transfer object.
     */
   public UserDto() {}

    /**
     * Creates a new user transfer object.
     * @param zim The user's zim ID.
     * @param name The user'S name.
     * @param password The user's password.
     * @param dayOfBirth The user's day of birth.
     * @param monthOfBirth The user's month of birth.
     * @param yearOfBirth The user's year of birth.
     */
    public UserDto(String zim, String name, String password, int dayOfBirth,
                   int monthOfBirth, int yearOfBirth){
        this.zim = zim;
        this.name = name;
        this.password = password;
        this.dayOfBirth = dayOfBirth;
        this.monthOfBirth = monthOfBirth;
        this.yearOfBirth = yearOfBirth;
    }

    UserDto(String zim,String name){
        this.zim = zim;
        this.name = name;}

    //Getter & Setter

    public String getZim() {
        return zim;
    }

    public void setZim(String zim) {
        this.zim = zim;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(int dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public int getMonthOfBirth() {
        return monthOfBirth;
    }

    public void setMonthOfBirth(int monthOfBirth) {
        this.monthOfBirth = monthOfBirth;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public void setBirthday(LocalDate birthday) {
        yearOfBirth = birthday.getYear();
        monthOfBirth = birthday.getMonthValue(); // 1-12
        dayOfBirth = birthday.getDayOfMonth(); // 1-31
    }


    String getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(String verificationToken) {
        this.verificationToken = verificationToken;
    }

    public Long getAimId() {
        return aimId;
    }

    public void setAimId(Long aimId) {
        this.aimId = aimId;
    }

    public Long getExperienceId() {
        return experienceId;
    }

    public void setExperienceId(Long experienceId) {
        this.experienceId = experienceId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }
}