package de.unipassau.ep.team3.EP.model;


/**
 * Data transfer object class for the  Aim model.
 */
public class AimDto {

    /**
     * The ID of the aim.
     */
    private Long id;

    /**
     * The name of the aim.
     */
    private String name;

    /**
     * The id of the super aim of this aim.
     */
    private Long superAimId;

    /**
     * The description of the aim.
     */
    private String description;

    /**
     * The description of the aim.
     */
    private String furtherInformation;

    /**
     * The zim id of the user who edited the aim latest.
     */
    private String editorZim;

    /**
     * The count of sub aims of the aim.
     */
    private int subAimsCount;

    /**
     * An Array with the sub aim id's of the aim.
     */
    private long [] subAims;

    /**
     * Creates a new aim transfer object.
     */
    public AimDto(){

    }

    /**
     * Creates a new aim transfer object.
     *
     * @param name The aim's name.
     */
    public AimDto(String name){
        this.name = name;}

    //Getter & Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFurtherInformation() {
        return furtherInformation;
    }

    public void setFurtherInformation(String furtherInformation) {
        this.furtherInformation = furtherInformation;
    }

    public Long getSuperAimId() {
        return superAimId;
    }

    public void setSuperAimId(Long superAimId) {
        this.superAimId = superAimId;
    }

    public String getEditorZim() {
        return editorZim;
    }

    public void setEditorZim(String editorZim) {
        this.editorZim = editorZim;
    }

     int getSubAimsCount() {
        return subAimsCount;
    }

    /**
     * Sets the new count of sub aims of the aim.
     * @param subAimsCount The aim's new sub aim count.
     */
    void setSubAimsCount(int subAimsCount) {
        this.subAimsCount = subAimsCount;
    }

    public long[] getSubAims() {
        return subAims;
    }

    void setSubAims(long[] subAims) {
        this.subAims = subAims;
    }
}
