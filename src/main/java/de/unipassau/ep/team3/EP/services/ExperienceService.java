package de.unipassau.ep.team3.EP.services;

import de.unipassau.ep.team3.EP.database.ExperienceRepository;
import de.unipassau.ep.team3.EP.database.UserRepository;
import de.unipassau.ep.team3.EP.exceptions.BadRequestException;
import de.unipassau.ep.team3.EP.exceptions.BadRequestReason;
import de.unipassau.ep.team3.EP.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A spring service class to manage experience level of the online training plattform.
 */
@Service
public class ExperienceService {
    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger(ExperienceService.class.getName());
    /**
     * The database repository for experiences.
     */
    private final ExperienceRepository experienceRepository;

    /**
     * The database repository for users.
     */
    private final UserRepository userRepository;

    /**
     * Creates a new experience level service.
     * @param experienceRepository The experience level service's user repository automaticaly injected.
     * @param userRepository       The experience level service'S user repository automaticaly injected.
     */
    public ExperienceService(ExperienceRepository experienceRepository, UserRepository userRepository) {
        this.experienceRepository = experienceRepository;
        this.userRepository = userRepository;
    }

    /**
     * Creates a new experience level.
     * @param experienceDto The experience level's DTO to be created from.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public void createNewExperienceLevel(ExperienceDto experienceDto) {
        if (experienceDto.getName() == null) {
            log.error(BadRequestReason.NAME_MISSING.toString());
            throw new BadRequestException(BadRequestReason.NAME_MISSING);
        } else if (experienceDto.getName().equals("")) {
            log.error(BadRequestReason.INVALID_NAME.toString());
            throw new BadRequestException(BadRequestReason.INVALID_NAME);
        } else if (experienceAlreadyExists(experienceDto.getName())) {
            log.error(BadRequestReason.ALREADY_EXISTS.toString() + "name " + experienceDto.getName());
            throw new BadRequestException(BadRequestReason.ALREADY_EXISTS);
        } else {
            Experience experience = DtoConverter.convertFromDto(experienceDto);
            log.debug("An experience was created from the experienceDto");
            String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
            User editor = userRepository.findByZim(currentUser);
            experience.setEditor(editor);
            log.debug("The editor " + editor.getZim() + " for the experience was set");
            experienceRepository.save(experience);
            log.info("The experience level was saved in the database.");
        }

    }

    /**
     * Checks if an experience level already exists {@code true},
     * {@code false} otherwise.
     *
     * @param name The name of the experience level.
     * @return {@code true} if it already exists {@code false}
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    private boolean experienceAlreadyExists(String name) {

        List<Experience> sameNameExperience = experienceRepository.findByExperienceName(name);
        return sameNameExperience.size() > 0;
    }

    /**
     * Get all experience levels from the database as DTOs.
     *
     * @return The list of esperience level DTOs.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public Iterable<ExperienceDto> findAll() {
        return experienceRepository.findAll().stream()
                .map(DtoConverter::convertToDto).collect(Collectors.toList());
    }


    /**
     * Gets an experience level from the database.
     *
     * @param id The ID of the experience level.
     * @return The experience level.
     * @throws BadRequestException Experience level with this ID doesn't exists.
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    Experience findExperienceById(Long id) {
        Experience level = experienceRepository.findByExperienceId(id);
        if (level == null) {
            log.error(BadRequestReason.EXPERIENCE_LEVEL_NOT_EXISTING.toString() + " " + id);
            throw new BadRequestException(BadRequestReason.EXPERIENCE_LEVEL_NOT_EXISTING);
        }
        log.info("The experience level with name " + level.getName() + " loaded from the database. id: " + level);
        return level;
    }

    /**
     * A method to update an experience level.
     *
     * @param levelDto The experienceDto to get the updates from.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public void update(ExperienceDto levelDto) {
        Experience level = findExperienceById(levelDto.getId());
        //Not duplicated because in aim service an aim dto is used
        if (levelDto.getName() == null) {
            log.error(BadRequestReason.NAME_MISSING.toString());
            throw new BadRequestException(BadRequestReason.NAME_MISSING);
        }else if (levelDto.getName().equals("")) {
            log.error(BadRequestReason.INVALID_NAME.toString());
            throw new BadRequestException(BadRequestReason.INVALID_NAME);
        }
        level.setName(levelDto.getName());
        level.setDescription(levelDto.getDescription());

        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        User editor = userRepository.findByZim(currentUser);
        level.setEditor(editor);
        log.debug("The editor " + editor.getZim() + " was set for the experience");
        experienceRepository.save(level);
        log.info("The experience level with name:" + level.getName() + " was updated in the database.id:" + level.getId());
    }

    /**
     * A method to delete an experience level
     *
     * @param id The id of the experience level to deleted.
     */
    @PreAuthorize("isAuthenticated()and hasRole('TRAINER')")
    public void delete(Long id) {
        if (experienceRepository.findAll().size() == 1) {
            log.error(BadRequestReason.LAST_EXPERIENCE.toString());
            throw new BadRequestException(BadRequestReason.LAST_EXPERIENCE);
        }
        experienceRepository.delete(experienceRepository.findByExperienceId(id));
        log.info("The experience with the id: " + id + " was deleted");
    }


    /**
     * Get the description of a specific experience level.
     *
     * @param id The id of the experience level.
     * @return The description
     */
    @PreAuthorize("isAuthenticated()and hasAnyRole('TRAINER','USER')")
    public String getDescription(Long id) {
        Experience experience = findExperienceById(id);
        if (experience.getDescription() != null) {
            log.debug("Description of experience:" + id + " description" + experience.getDescription());
            return experience.getDescription();
        } else {
            log.debug("No description for experience with id:" + id);
            return null;
        }
    }
}