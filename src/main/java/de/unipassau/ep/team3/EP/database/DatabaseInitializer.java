package de.unipassau.ep.team3.EP.database;


import de.unipassau.ep.team3.EP.model.*;
import de.unipassau.ep.team3.EP.services.AimService;
import de.unipassau.ep.team3.EP.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Initializes the database at startup time for testing purposes.
 */
@Component
@Transactional
public class DatabaseInitializer implements ApplicationRunner {

    /**
     * The user service.
     */
    private final UserService userService;

    /**
     * The aim service.
     */
    private final AimService aimService;

    /**
     * The exercise repository.
     */
    private final ExerciseRepository exerciseRepository;

    /**
     * The user repository.
     */
    private final UserRepository userRepository;

    /**
     * The aim repository.
     */
    private final AimRepository aimRepository;

    /**
     * The experience level repository.
     */
    private final ExperienceRepository experienceRepository;



    private final ScheduleRepository scheduleRepository;


    /**
     * Creates a new database initializer
     *
     * @param userService          The user service.
     * @param aimService           The aim service.
     * @param userRepository       The user repository.
     * @param aimRepository        The aim repository.
     * @param experienceRepository The experience level repository.
     * @param exerciseRepository   The exercise repository.
     * @param scheduleRepository   The schedule repository.
     */
    @Autowired
    public DatabaseInitializer(UserService userService, AimService aimService, UserRepository userRepository, AimRepository aimRepository, ExperienceRepository experienceRepository, ExerciseRepository exerciseRepository, ScheduleRepository scheduleRepository){
        this.userService = userService;
        this.userRepository = userRepository;
        this.aimRepository = aimRepository;
        this.experienceRepository = experienceRepository;
        this.exerciseRepository = exerciseRepository;
        this.scheduleRepository = scheduleRepository;
        this.aimService = aimService;
    }

    /**
     * Stores initial data in the database.
     *
     * @param args The command line argument.
     */
    @Override
    public void run(ApplicationArguments args) {

        userService.createNewUser(new UserDto("herman26","Vanessa", "12345678",14,9,1997));
        User user = userRepository.findByZim("herman26");
        user.setVerificationToken(null);
        user.setStatus(UserStatus.TRAINER);
        userRepository.save(user);

        userService.createNewUser((new UserDto("winter57","Marco","12345678",30,11,1996)));
        user = userRepository.findByZim("winter57");
        user.setVerificationToken(null);
        user.setStatus(UserStatus.TRAINER);
        userRepository.save(user);

        userService.createNewUser(new UserDto("haan01","Alex","12345678",22,11,1999));
        user = userRepository.findByZim("haan01");
        user.setVerificationToken(null);
        user.setStatus(UserStatus.TRAINER);
        userRepository.save(user);

        User trainer = new User("ludwig22","Dominik","12345678",null);
        trainer.setHash((new BCryptPasswordEncoder()).encode("12345678"));
        trainer.setStatus(UserStatus.TRAINER);
        userRepository.save(trainer);

        Exercise exercise = new Exercise("NoExercise");
        exercise.setAlternativeExercises(null);
        exercise.setEditor(userRepository.findByZim("herman26"));
        exerciseRepository.save(exercise);



        }
    }
