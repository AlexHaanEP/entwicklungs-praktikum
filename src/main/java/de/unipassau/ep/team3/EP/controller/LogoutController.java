package de.unipassau.ep.team3.EP.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A RestController providing the endpoints subsequent to /logout .
 */
@Component
@Controller
public class LogoutController {

    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger(LogoutController.class.getName() );

    /**
     * A method to "logout" by clearing the security context.
     */
    @GetMapping(value = "/logout")
    @ResponseStatus(value = HttpStatus.OK)
    public void logout() {
        log.info("Got an GET request on /logout");
        SecurityContextHolder.clearContext();
        log.info("The security context was cleared");
    }
}
