package de.unipassau.ep.team3.EP.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.List;
import java.util.LinkedList;

/**
 * A model class representing an exercise of the online training plattform.
 */
@Entity
@Table(name="\"exercise\"")
public class Exercise {

    /**
     * The ID of the exercise.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The name of an exercise
     */
    @NotNull
    private String name;

    /**
     * The description of an exercise.
     */
    @Column(length = 1000)
    private String description;

   /**
     * The user who edited the exercise last.
     */
    @ManyToOne
    private User editor;

    /**
     * If true, the exercise is either the dummy exercise or an phantom exercise.
     */
    private boolean assigned;

    /**
     * The list of alternative exercises.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Exercise> alternativeExercises = new LinkedList<>();

    /**
     * The list of training schedules in which the exercise is used.
     */
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, mappedBy = "exercises")
    private List<Schedule> scheduleList = new LinkedList<>();

    /**
     * The list of training schedules in which the exercise is used as alternative exercise.
     */
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, mappedBy = "alternativeExercises")
    private List<Schedule> scheduleListAlternative = new LinkedList<>();


    /**
     * The URL of the videio for the exercise.
     */
    private URL video;

    /**
     * A list of execution
     */
    private String [] executions;

    /**
     * Creates an exercise.
     */
    public Exercise() {}

    /**
     * Creates an exercise.
     *
     * @param name The exercise's name.
     */
    public Exercise(String name){
        this.name = name;
    }

    //Getter & Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getEditor() {
        return editor;
    }

    public void setEditor(User editor) {
        this.editor = editor;
    }

    public List<Exercise> getAlternativeExercises() {
        return alternativeExercises;
    }

    public void setAlternativeExercises(List<Exercise> alternativeExercises) {
        this.alternativeExercises = alternativeExercises;
    }

    public List<Schedule> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(List<Schedule> scheduleList) {
        this.scheduleList = scheduleList;
    }

    public void addAlternativeExercise(Exercise exercise){
        this.alternativeExercises.add(exercise);
    }


    public URL getVideo() {
        return video;
    }

    public void setVideo(URL video) {
        this.video = video;
    }

    String[] getExecutions() {
        return executions;
    }

    public void setExecutions(String[] executions) {
        this.executions = executions;
    }

    public List<Schedule> getScheduleListAlternative() {
        return scheduleListAlternative;
    }

    public void setScheduleListAlternative(List<Schedule> scheduleListAlternative) {
        this.scheduleListAlternative = scheduleListAlternative;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }
}