package de.unipassau.ep.team3.EP.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDate;

/**
 * A model class representing a user of the online training platform.
 */
@Entity
@Table(name="\"user\"")
public class User {

    /**
     * The domain of the university of Passau.
     */
    private static final String UNI_PASSAU_DE = "@gw.uni-passau.de";

    /**
     * The zim-identifier of the user.
     */
    @Id
    private String zim;

    /**
     * The hash value of the user's password.
     */
    @NotNull
    private String hash;

    /**
     * The name of the user.
     */
    @NotNull
    private String name;

    /**
     * The token to verificate the user.
     */
    private String verificationToken;

    /**
     * The gender of the user.
     */
    private Gender gender;

    /**
     * The height of the user in centimetre.
     */
    private int height;

    /**
     * The weight of the user in kilogramm.
     */
    private int weight;

    /**
     * The birthday of the user.
     */
    private LocalDate birthday;

    /**
     * The status of the user.
     */
    @NotNull
    private UserStatus status;

    /**
     * The level of experience in training of the user.
     */
    @ManyToOne
    private Experience experience;

    /**
     * The aim of training of the user.
     */
    @ManyToOne
    @JoinColumn(name="aim_id")
    private Aim aim;

    /**
     * The email address of the user
     */
    private String emailAdress;

    /**
     * The trainings schedule of the user.
     */
    @OneToOne
    private Schedule schedule;

    /**
     * The time of the last login.
     */
    private Timestamp lastLogin;

    /**
     * {@code true} if user is valid, {code false} otherwise.
     */
    @NotNull
    private boolean validUser;

    /**
     * Creates a new user.
     */
    public User() {
    }

    /**
     * Creates a new user.
     *
     * @param zim  The user'S zim ID.
     * @param hash The password hash.
     * @param name The user's name.
     */
    public User(String zim, String name, String hash, String verificationToken) {
        this.zim = zim;
        this.hash = hash;
        this.name = name;
        this.status = UserStatus.UNKNOWN;
        this.emailAdress = zim + UNI_PASSAU_DE;
        this.lastLogin = new Timestamp(System.currentTimeMillis());
        this.validUser = false;
        this.verificationToken = verificationToken;
    }

    //Getter & Setter

    public String getZim() {
        return zim;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public LocalDate getBirthday() {
        return birthday;
    }


    /**
     * Sets the birthday of the user.
     *
     * @param birthday The user's new birthday.
     */
    void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }

    public Aim getAim() {
        return aim;
    }

    public void setAim(Aim aim) {
        this.aim = aim;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Timestamp getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Timestamp lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isValidUser() {
        return validUser;
    }

    public void setValidUser(boolean validUser) {
        this.validUser = validUser;
    }

    public String getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(String verificationToken) {
        this.verificationToken = verificationToken;
    }

    public void setZim(String zim) {
        this.zim = zim;
    }
}