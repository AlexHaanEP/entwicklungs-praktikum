package de.unipassau.ep.team3.EP.database;

import de.unipassau.ep.team3.EP.model.Aim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AimRepository extends JpaRepository<Aim,Long> {

    /**
     * Gets an aim by its id.
     *
     * @param id The aim's id.
     * @return The corresponding aim or {@code null}.
     */
    @Query("SELECT a FROM Aim AS a WHERE a.id = :id")
    Aim findByAimId(@Param("id") long id);

    /**
     * Gets an aim by its name.
     *
     * @param name The aim's name.
     * @return The corresponding aim or {@code null}.
     */
    @Query("SELECT a FROM Aim AS a WHERE a.name =:name")
    List<Aim> findByName(@Param("name") String name);


    /**
     * Gets a list of sub aims with the given super aim id.
     *
     * @param superAimId The aim's super aimid.
     * @return The corresponding aims or {@code null}.
     */
    @Query("SELECT a FROM Aim AS a WHERE a.superAim.id = :superAimId")
    List<Aim> findAimsWithSuperAimID(@Param("superAimId") long superAimId);

    /**
     * Gets all super aims.
     *
     * @return The corresponding aims or {@code null}.
     */
    @Query("SELECT a FROM Aim AS a WHERE a.superAim = null")
    List<Aim> findAllSuperAims();


}
