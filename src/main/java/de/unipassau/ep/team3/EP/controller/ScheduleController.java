package de.unipassau.ep.team3.EP.controller;


import de.unipassau.ep.team3.EP.model.ScheduleDto;
import de.unipassau.ep.team3.EP.services.ScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * A RestController providing the endpoints subsequent to /schedules .
 */
@Component
@RestController
@RequestMapping(value = "schedules")
@CrossOrigin(origins = "*")
public class ScheduleController {

    /**
     * The logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger( ScheduleController.class.getName() );

    /**
     * The MIME type to use.
     */
    private static final String MIME_TYPE_JSON = "application/json";

    /**
     * The service to use to manage schedule entities.
     */
    private final ScheduleService scheduleService;

    /**
     * Creates a new AimController object.
     *
     * @param scheduleService The schedules controller's service to manage schedules entities.
     */
    @Autowired
    public ScheduleController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }


    /**
     * An endpoint to create a new training schedule.
     *
     * @param scheduleDto The trainings schedules data transfer object.
     * @return The created schedule's id.
     */
    @PostMapping(value ="", consumes = MIME_TYPE_JSON)
    @ResponseStatus(HttpStatus.OK)
    public Long createSchedule(@RequestBody ScheduleDto scheduleDto){
        log.info("Got a POST request on /schedules/");
        Long scheduleId = scheduleService.createNewSchedule(scheduleDto);
        log.info("Training schedule was created");
        return scheduleId;
    }

    /**
     * An endpoint to get all training schedule, which has no owners.
     */
    @GetMapping(value = "", produces = MIME_TYPE_JSON)
    public String getAllUnassigned(){
        log.info("Got a GET request on /schedules/ to get all unassigned schedules.");
        return scheduleService.findAllUnassigned();
    }

    /**
     * An endpoint to get an schedule from the database.
     *
     * @param id The id of the schedule
     * @return The schedule as DTO
     */
    @GetMapping(value = "/{id}")
    public ScheduleDto getById(@PathVariable Long id){
        log.info("Got a GOT request on /schedules/"+id);
        return scheduleService.getScheduleById(id);
    }

    /**
     * An endpoint to delete an schedule.
     *
     * @param id The id of the schedule.
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id){
        log.info("Got a DELETE request on /schedules/" +id);
        scheduleService.delete(id);
    }

    /**
     * An endpoint to update an schedule.
     *
     * @param dto The schedule DTO to get the updates from.
     */
    @PutMapping(value = "/update",consumes = MIME_TYPE_JSON)
    public void update(@RequestBody ScheduleDto dto){
        log.info("Got a PUT request on /schedules/update");
        scheduleService.update(dto);
    }

    /**
     * An endpoint to get the name of an aim of the training schedule.
     *
     * @param id The ID of the training schedule.
     * @return The name of the aim.
     */
    @GetMapping(value = "/{id}/getAim")
    public String getAim(@PathVariable Long id){
        log.info("Got a GET request on /schedules/"+id+"/getAim");
        return scheduleService.getAimNameOfSchedule(id);
    }

    /**
     * An endpoint to get Progress of an training schedule.
     *
     * @param id The ID of the training schedule.
     * @return The Progress represented as a String.
     */
    @GetMapping(value ="/{id}/getProgress")
    public String getProgress(@PathVariable Long id){
        log.info("Got a GET request on /schedules/"+id+"/getProgress");
        return scheduleService.getProgress(id);
    }

    /**
     * An endpoint to update a schedule after a training session
     * or to delete it after the last session.
     *
     * @param scheduleDto The schedule to get the updates from.
     */
    @PutMapping(value = "/endUnit", consumes = MIME_TYPE_JSON)
    public void endUnit(@RequestBody ScheduleDto scheduleDto){
        scheduleService.endUnit(scheduleDto);
    }

}