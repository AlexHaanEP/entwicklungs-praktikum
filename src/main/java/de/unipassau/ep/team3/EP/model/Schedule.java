package de.unipassau.ep.team3.EP.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A model class representing a training schedule of the online training platform.
 */
@Entity
@Table(name="\"trainingSchedule\"")
public class Schedule implements Serializable {

    /**
     * The ID of the training schedule.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The aim of the training schedule.
     */
    @ManyToOne
    private Aim aim;

    private String aimName;

    /**
     * The experience level of the training schedule
     */
    @ManyToOne
    private Experience level;

    private String experienceName;

    /**
     * The gender to assign the plan to a user with this gender.
     */
    private Gender gender;


    /**
     * The user who edited the training schedule last.
     */
    @OneToOne
    private User editor;

    /**
     * The user who trains with the training schedule .
     */
    @OneToOne
    private User user;

    /**
     * The array of ranks for the exercises.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<String>  rank = new ArrayList<>();

    /**
     * The array of indexes of the ranks sorted ascending.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Integer>  rankIndexes = new ArrayList<>();

    /**
     * The list of exercises in the training schedule.
     */
    @ManyToMany(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Exercise> exercises = new LinkedList<>();

    /**
     * The array of executions for the exercises.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<String> executions = new ArrayList<>();

    /**
     * The array of wrapper for the exercises
     */
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @OrderColumn(name = "exercise_wrappers")
    private List<ExerciseWrapper>  wrappers = new LinkedList<>();

    /**
     * The list of alternative exercises in the training schedule.
     */
    @ManyToMany(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Exercise> alternativeExercises = new LinkedList<>();


    /**
     * If false, an exercise was deleted an a trainer has to update the trainings schedule
     */
    private boolean unlocked;

    /**
     * The current number units in the trainings schedule.
     */
    private int currentUnit;

    /**
     * The maximal number of units in the trainings schedules.
     */
    private int maxUnit;


    /**
     * Creates a training schedule.
     */
    public Schedule() {}

    /**
     * Creates an training schedule.
     *
     * @param aim The aim.
     * @param level The experience level.
     * @param gender The gender.
     */
    public Schedule(Aim aim, Experience level, Gender gender){
        this.aim = aim;
        this.level = level;
        this.gender = gender;
        this.unlocked = true;
    }

    /**
     * Creates an training schedule for a user.
     *
     * @param aimName The name of the aim.
     * @param experienceName The name of the expereince level.
     * @param gender The gender.
     */
    public Schedule(String aimName, String experienceName, Gender gender) {
        this.aimName = aimName;
        this.experienceName = experienceName;
        this.gender = gender;
    }

   //Getter & Setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getEditor() {
        return editor;
    }

    public void setEditor(User editor) {
        this.editor = editor;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    public Aim getAim() {
        return aim;
    }

    public void setAim(Aim aim) {
        this.aim = aim;
    }

    public Experience getLevel() {
        return level;
    }

    public void setLevel(Experience level) {
        this.level = level;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<String> getRank() {
        return rank;
    }

    public void setRank(List<String> rank) {
        this.rank = rank;
    }

    public List<Integer> getRankIndexes() {
        return rankIndexes;
    }

    public void setRankIndexes(List<Integer> rankIndexes) {
        this.rankIndexes = rankIndexes;
    }

    public List<String> getExecutions() {
        return executions;
    }

    public void setExecutions(List<String> executions) {
        this.executions = executions;
    }

    public List<Exercise> getAlternativeExercises() {
        return alternativeExercises;
    }

    public void setAlternativeExercises(List<Exercise> alternativeExercises) {
        this.alternativeExercises = alternativeExercises;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<ExerciseWrapper> getWrappers() {
        return wrappers;
    }

    public void setWrappers(List<ExerciseWrapper> wrappers) {
        this.wrappers = wrappers;
    }



    public void addExerciseWrapper(ExerciseWrapper wrapper){
        this.wrappers.add(wrapper);
    }

    boolean isUnlocked() {
        return unlocked;
    }

    public void setUnlocked(boolean unlocked) {
        this.unlocked = unlocked;
    }

    public String getAimName() {
        return aimName;
    }

    public void setAimName(String aimName) {
        this.aimName = aimName;
    }

    String getExperienceName() {
        return experienceName;
    }

    public void setExperienceName(String experienceName) {
        this.experienceName = experienceName;
    }

    public int getCurrentUnit() {
        return currentUnit;
    }

    public void setCurrentUnit(int currentUnit) {
        this.currentUnit = currentUnit;
    }

    public int getMaxUnit() {
        return maxUnit;
    }

    public void setMaxUnit(int maxUnit) {
        this.maxUnit = maxUnit;
    }
}