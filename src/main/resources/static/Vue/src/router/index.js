import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login'
import Admin from '../components/Admin'
import Register from '../components/Register'
import Trainingsplan from '../components/Trainingsplan'
import Dashboard from '../components/Dashboard'
import Exercise from '../components/Exercise'
import addExercise from '../components/addExercise'
import Targets from "../components/Targets"
import addTarget from "../components/addTarget"
import Users from "../components/Users"
import Settings from "../components/Settings"
import addPlan from "../components/addPlan"
import Experience from "../components/Experience"
import addExperience from "../components/addExperience"
import Trainingsuebersicht from "../components/Trainingsuebersicht"
import TrainingStarten from "../components/TrainingStarten";
import Training from "../components/Training";
import Details from "../components/Details";
import Password from "../components/Password";
import TrainingDone from "../components/TrainingDone";

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login
        },

        {
            path: '/admin',
            name: 'Admin',
            component: Admin
        },

        {
            path: '/register',
            name: 'Register',
            component: Register
        },

        {
            path: '/trainingsplan',
            name: 'Trainingsplan',
            component: Trainingsplan
        },

        {
            path: '/dashboard',
            name: 'Dashboard',
            component: Dashboard
        },

        {
            path: '/exercise',
            name: 'Exercise',
            component: Exercise
        },

        {
            path: '/addExercise',
            name: 'addExercise',
            component: addExercise
        },

        {
            path: '/targets',
            name: 'targets',
            component: Targets
        },

        {
            path: '/addTarget',
            name: 'addTarget',
            component: addTarget
        },

        {
            path: '/users',
            name: 'users',
            component: Users
        },

        {
            path: '/settings',
            name: 'settings',
            component: Settings
        },

        {
            path: '/addPlan',
            name: 'addPlan',
            component: addPlan
        },

        {
            path: '/experience',
            name: 'experience',
            component: Experience
        },

        {
            path: '/addExperience',
            name: 'addExperience',
            component: addExperience
        },

        {
            path: '/Trainingsuebersicht',
            name: 'Trainingsuebersicht',
            component: Trainingsuebersicht
        },

        {
            path: '/TrainingStarten',
            name: 'TrainingStarten',
            component: TrainingStarten
        },

        {
            path: '/Training',
            name: 'Training',
            component: Training
        },

        {
            path: '/details',
            name: 'details',
            component: Details
        },

        {
            path: '/password',
            name: 'password',
            component: Password
        },

        {
            path: '/trainingDone',
            name: 'TrainingDone',
            component: TrainingDone
        }

    ]
})