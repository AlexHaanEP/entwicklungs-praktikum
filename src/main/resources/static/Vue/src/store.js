import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
  userId: undefined,
  userPassword: undefined,
  aims: undefined,
  exercises: undefined,
  experiences: undefined,
};

export const getters = {

  userId(state) {
    return state.userId;
  },

  userPassword(state) {
    return state.userPassword;
  },

  aims(state) {
    return state.aims;
  },

  exercises(state) {
    return state.exercises;
  },

  experiences(state) {
    return state.experiences;
  }
};

export const mutations = {

  resetUserData(state) {
    state.userId = undefined;
    state.userPassword = undefined;
  },

  saveId(state, id) {
    state.userId = id;
  },

  savePassword(state, password) {
    state.userPassword = password;
  },

  saveAims(state, aims) {
    state.aims = aims;
  },

  addAim(state, aim) {
    state.aims.push(aim);
  },

  saveExercises(state, exercises) {
    state.exercises = exercises;
  },

  addExercise(state, exercise) {
    state.exercises.push(exercise);
  },

  saveExperiences(state, experiences) {
    state.experiences = experiences;
  },

  addExperiences(state, experiences) {
    state.experiences.push(experiences);
  },

};

export default new Vuex.Store({
  state,
  getters,
  mutations
});