import Vue from 'vue';
import moxios from 'moxios';
import axios from 'axios';
import sinon from 'sinon';
import { equal } from 'assert-plus';
import { mount, RouterLinkStub} from "@vue/test-utils";
import { getters, mutations } from '../../../src/store';
import addPlan from '../../../src/components/addPlan';

describe('addPlan.vue', () => {

  it('should render correct contents', () => {
    let createdResult;
    const addPlanTest = Object.assign({}, addPlan, {
      created() {
        const res = addPlan.created.call(this);
        createdResult = res;
        return res;
      }
    });
    const state = {
      userId: 'TestMan1',
      userPassword: 'TestPassword1',
      exercises: [
        {exerciseId: 'TestID' , name: 'TestName' , editorZIM: 'TestZIM'},
        {exerciseId: 'TestID2' , name: 'TestName2' , editorZIM: 'TestZIM2'}
      ],
      experiences: [
        {name: 'TestExperience', description: 'Test description'}
      ],
      aims: [
        {id: 1, name: 'TestAim', superAimId: 0, description: null,
          editorZIM: 'haan01', subAimsCount: 0, subAims: []}
      ]
    };
    const wrapper = mount(addPlanTest, {
      stubs: {
        RouterLink: RouterLinkStub
      },
      computed: {
        userId() {
          return state.userId;
        },

        userPassword() {
          return state.userPassword;
        },

        exercises() {
          return state.exercises;
        },

        experiences() {
          return state.experiences;
        },

        aims() {
          return state.aims;
        }
      },
    });
    expect(wrapper.find(RouterLinkStub).props().to)
      .to.equal('Dashboard');
    expect(wrapper.findAll(RouterLinkStub).at(0).text())
      .to.equal('Profil');
    expect(wrapper.find('b-button').text())
      .to.equal('Admin');
    expect(wrapper.findAll('b-button').at(2).text())
      .to.equal('zurück zur Übersicht');
  });

  describe('requests', () => {
    let onFulfilled;
    let onRejected;

    beforeEach(() => {
      moxios.install();
      onFulfilled = sinon.spy();
      onRejected = sinon.spy();
    });

    afterEach(() => {
      moxios.uninstall();
    });

    it('should mock responses of the logout controller', (done) => {
      axios.get('http://localhost:8080/logout').then(onFulfilled);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
        }).then(() => {
          let response = onFulfilled.getCall(0).args[0];
          equal(onFulfilled.called, true);
          equal(response.status, 200);
          done()
        })
      })
    });

    it('should mock response error of the logout controller', (done) => {
      axios.get('http://localhost:8080/logout')
        .then(onFulfilled, onRejected);

      moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 401
        }).then(function () {
          equal(onFulfilled.called, false);
          equal(onRejected.called, true);
          done()
        });
      });
    });

    it('should mock responses of the exercise controller', (done) => {
      axios.get('http://localhost:8080/exercises')
        .then(onFulfilled);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
        }).then(() => {
          let response = onFulfilled.getCall(0).args[0];
          equal(onFulfilled.called, true);
          equal(response.status, 200);
          done()
        });
      });
    });

    it('should mock response error of the exercise controller', (done) => {
      axios.get('http://localhost:8080/exercises')
        .then(onFulfilled, onRejected);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 401
        }).then(() => {
          equal(onFulfilled.called, false);
          equal(onRejected.called, true);
          done()
        });
      });
    });
  });
});
