import Vue from 'vue'
import Exercise from '../../../src/components/Exercise'
import {mount, RouterLinkStub} from "@vue/test-utils";
import {getters, mutations} from "../../../src/store";
import { equal } from 'assert-plus'
import moxios from "moxios";
import sinon from "sinon";
import axios from "axios";

describe('Excercise.vue', () => {

  it('should render correct contents', () => {
    let createdResult;
    const ExerciseTest = Object.assign({}, Exercise, {
      created() {
        const res = Exercise.created.call(this);
        createdResult = res;
        return res;
      }
    });
    const state = {
      userId: 'TestMan1',
      userPassword: 'TestPassword1',
      exercises: [
        {exerciseId: 'TestID' , name: 'TestName' , editorZIM: 'TestZIM'},
        {exerciseId: 'TestID2' , name: 'TestName2' , editorZIM: 'TestZIM2'}
      ]
    };
    const wrapper = mount(ExerciseTest, {
      stubs: {
        RouterLink: RouterLinkStub
      },
      computed: {
        userId() {
          return state.userId;
        },

        userPassword() {
          return state.userPassword;
        },

        exercises() {
          return state.exercises;
        }
      },
    });
    expect(wrapper.find(RouterLinkStub).props().to).to.equal('Dashboard');
    expect(wrapper.findAll(RouterLinkStub).at(0).text()).to.equal('Profil');
    expect(wrapper.find('b-button').text()).to.equal('Admin');
    expect(wrapper.find('b-alert').text()).to.equal('Übersicht der Übungen');
    expect(wrapper.findAll('b-button').at(3).text())
      .to.equal('neue Übung erstellen');
  });

  describe('Store getters', () => {

    it('returns the userId', () => {
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1'
      };
      const result = getters.userId(state);
      expect(result).to.deep.equal('TestMan1');
    });

    it ('returns the userPassword', () => {
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1'
      };
      const result = getters.userPassword(state);
      expect(result).to.deep.equal('TestPassword1');
    });

    it('returns the exercises', () => {
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1',
        exercises: [
          {exerciseId: 'TestID' , name: 'TestName' , editorZIM: 'TestZIM'},
          {exerciseId: 'TestID2' , name: 'TestName2' , editorZIM: 'TestZIM2'}
        ]
      };
      const result = getters.exercises(state);
      expect(result[0].exerciseId).to.deep.equal('TestID');
      expect(result[0].name).to.deep.equal('TestName');
      expect(result[0].editorZIM).to.deep.equal('TestZIM');
      expect(result[1].exerciseId).to.deep.equal('TestID2');
      expect(result[1].name).to.deep.equal('TestName2');
      expect(result[1].editorZIM).to.deep.equal('TestZIM2');
    });
  });

  describe('Store mutations', () => {

    it('saves the userId', () => {
      let newUserId = 'TestMan1';
      const state = {userId: undefined};
      mutations.saveId(state, newUserId);
      expect(state.userId).to.equal('TestMan1');
    });

    it('saves the userPassword', () => {
      let newUserPassword = 'TestPassword1';
      const state = {userPassword: undefined};
      mutations.savePassword(state, newUserPassword);
      expect(state.userPassword).to.equal('TestPassword1');
    });

    it('resets the userData', () => {
      let resetUserId = undefined;
      let resetUserPassword = undefined;
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1'
      };
      mutations.resetUserData(state, resetUserId, resetUserPassword);
      expect(state.userId).to.equal(undefined);
      expect(state.userPassword).to.equal(undefined);
    });

    it('saves the exercises', () => {
      let newExercises = [
        {exerciseId: 'TestID' , name: 'TestName' , editorZIM: 'TestZIM'},
        {exerciseId: 'TestID2' , name: 'TestName2' , editorZIM: 'TestZIM2'}
      ];
      const state = {exercises: undefined};
      mutations.saveExercises(state, newExercises);
      expect(state.exercises[0].exerciseId).to.deep.equal('TestID');
      expect(state.exercises[0].name).to.deep.equal('TestName');
      expect(state.exercises[0].editorZIM).to.deep.equal('TestZIM');
      expect(state.exercises[1].exerciseId).to.deep.equal('TestID2');
      expect(state.exercises[1].name).to.deep.equal('TestName2');
      expect(state.exercises[1].editorZIM).to.deep.equal('TestZIM2');
    });

    it('adds an additional exercise', () => {
      let newExercise = {
        exerciseId: 'TestID3',
        name: 'TestName3',
        editorZIM: 'TestZIM3'
      };
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1',
        exercises: [
          {exerciseId: 'TestID', name: 'TestName', editorZIM: 'TestZIM'},
          {exerciseId: 'TestID2', name: 'TestName2', editorZIM: 'TestZIM2'}
        ]
      };
      mutations.addExercise(state, newExercise);
      expect(state.exercises[0].exerciseId).to.deep.equal('TestID');
      expect(state.exercises[0].name).to.deep.equal('TestName');
      expect(state.exercises[0].editorZIM).to.deep.equal('TestZIM');
      expect(state.exercises[1].exerciseId).to.deep.equal('TestID2');
      expect(state.exercises[1].name).to.deep.equal('TestName2');
      expect(state.exercises[1].editorZIM).to.deep.equal('TestZIM2');
      expect(state.exercises[2].exerciseId).to.deep.equal('TestID3');
      expect(state.exercises[2].name).to.deep.equal('TestName3');
      expect(state.exercises[2].editorZIM).to.deep.equal('TestZIM3');
    });
  });

  describe('requests', () => {
    let onFulfilled;
    let onRejected;

    beforeEach(() => {
      moxios.install();
      onFulfilled = sinon.spy();
      onRejected = sinon.spy();
    });

    afterEach(() => {
      moxios.uninstall();
    });

    it('should mock responses of the logout controller', (done) => {
      axios.get('http://localhost:8080/logout').then(onFulfilled);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
        }).then(() => {
          let response = onFulfilled.getCall(0).args[0];
          equal(onFulfilled.called, true);
          equal(response.status, 200);
          done()
        })
      })
    });

    it('should mock response error of the logout controller', (done) => {
      axios.get('http://localhost:8080/logout')
        .then(onFulfilled, onRejected);

      moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 401
        }).then(function () {
          equal(onFulfilled.called, false);
          equal(onRejected.called, true);
          done()
        });
      });
    });

    it('should mock responses of the exercise controller', (done) => {
      axios.get('http://localhost:8080/exercises')
        .then(onFulfilled);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
        }).then(() => {
          let response = onFulfilled.getCall(0).args[0];
          equal(onFulfilled.called, true);
          equal(response.status, 200);
          done()
        });
      });
    });

    it('should mock response error of the exercise controller', (done) => {
      axios.get('http://localhost:8080/exercises')
        .then(onFulfilled, onRejected);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 401
        }).then(() => {
          equal(onFulfilled.called, false);
          equal(onRejected.called, true);
          done()
        });
      });
    });
  });
});
