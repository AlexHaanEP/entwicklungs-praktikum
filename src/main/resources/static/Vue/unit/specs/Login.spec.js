import Vue from 'vue'
import moxios from 'moxios'
import axios from 'axios'
import sinon from 'sinon'
import { equal } from 'assert-plus'
import {mount, RouterLinkStub} from "@vue/test-utils"
import Login from '../../../src/components/Login'

describe('Login.vue', () => {

  it('should render correct contents', () => {
    const wrapper = mount(Login , {
      stubs: {
        RouterLink: RouterLinkStub
      }
    });
    expect(wrapper.find('h5').text())
      .to.equal('Login');
    expect(wrapper.findAll(RouterLinkStub).at(0).text())
      .to.equal('Registrieren');
    expect(wrapper.findAll(RouterLinkStub).at(1).text())
      .to.equal('Passwort vergessen?');
    expect(wrapper.find('b-button').text())
      .to.equal('Login');
  });

//id unit tests

  it('checks the entered id and declines it because it´s longer than 8 characters', () =>  {
    expect(Login.methods.isValidID('12345678910')).to.equal(false);
    expect(Login.methods.validateInput('12345678910', 'testPassword'))
      .to.equal(false);
  });

  it('checks the entered id and accepts it', () => {
    expect(Login.methods.isValidID('testme01')).to.equal(true);
    expect(Login.methods.validateInput('testme01', 'testPassword'))
      .to.equal(true);
  });

//password unit tests

  it('checks the entered password and declines it because it´s shorter than 8 character', () => {
    expect(Login.methods.isValidPassword('wrongPW')).to.equal(false);
    expect(Login.methods.validateInput('testID', 'wrongPW'))
      .to.equal(false);
  });

  it('checks the entered password and accepts it', () => {
    expect(Login.methods.isValidPassword('12345678')).to.equal(true);
    expect(Login.methods.validateInput('testID', '12345678'))
      .to.equal(true);
  });


  describe('requests', function () {
    let onFulfilled;
    let onRejected;

    beforeEach(function () {
      moxios.install();
      onFulfilled = sinon.spy();
      onRejected = sinon.spy();
    });

    afterEach(function () {
      moxios.uninstall();
    });

    it('should mock responses', function (done) {
      let id = 'testman';
      axios.get('http://localhost:8080/users/' + id + '/status').then(onFulfilled);

      moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
          response: 'USER'
        }).then(function () {
          let response = onFulfilled.getCall(0).args[0];
          equal(onFulfilled.called, true);
          equal(response.data, 'USER');
          equal(response.status, 200);
          done()
        })
      })
    });

    it('should mock response error', function (done) {
      let id = 'testman';
      axios.get('http://localhost:8080/users/' + id + '/status')
        .then(onFulfilled, onRejected);

      moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 401
        }).then(function () {
          equal(onFulfilled.called, false);
          equal(onRejected.called, true);
          done()
        })
      })
    })
  });
});
