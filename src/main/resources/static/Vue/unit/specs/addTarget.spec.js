import Vue from 'vue';
import moxios from 'moxios';
import axios from 'axios';
import sinon from 'sinon';
import { equal } from 'assert-plus';
import { mount, RouterLinkStub} from "@vue/test-utils";
import { getters, mutations } from '../../../src/store';
import addTarget from '../../../src/components/addTarget';

describe('addTarget.vue', () =>  {

  it('should render correct contents', () => {
    let createdResult;
    const addTargetTest = Object.assign({}, addTarget, {
      created() {
        const res = addTarget.created.call(this);
        createdResult = res;
        return res;
      }
    });
    const state = {
      userId: 'TestMan1',
      userPassword: 'TestPassword1',
      aims: [
        {id: 'TestID' , name: 'TestName'},
        {id: 'TestID2' , name: 'TestName2'}
      ]
    };
    const wrapper = mount(addTargetTest, {
      stubs: {
        RouterLink: RouterLinkStub
      },
      computed: {
        userId() {
          return state.userId;
        },

        userPassword() {
          return state.userPassword;
        },

        aims() {
          return state.aims;
        }
      },
    });
    expect(wrapper.find(RouterLinkStub).props().to)
      .to.equal('Dashboard');
    expect(wrapper.findAll(RouterLinkStub).at(0).text())
      .to.equal('Profil');
    expect(wrapper.find('b-button').text())
      .to.equal('Admin');
    expect(wrapper.findAll('b-button').at(2).text())
      .to.equal('logout');
    expect(wrapper.findAll('b-button').at(3).text())
    .to.equal('Trainingsziel hinzufügen');
  });

  describe('Store getters', () => {

    it('returns the userId', () => {
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1'
      };
      const result = getters.userId(state);
      expect(result).to.deep.equal('TestMan1');
    });

    it ('returns the userPassword', () => {
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1'
      };
      const result = getters.userPassword(state);
      expect(result).to.deep.equal('TestPassword1');
    });

    it('returns the aims', () => {
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1',
        aims: [
          {id: 'TestID' , name: 'TestName'},
          {id: 'TestID2' , name: 'TestName2'}
        ]
      };
      const result = getters.aims(state);
      expect(result[0].id).to.deep.equal('TestID');
      expect(result[0].name).to.deep.equal('TestName');
      expect(result[1].id).to.deep.equal('TestID2');
      expect(result[1].name).to.deep.equal('TestName2');
    });
  });

  describe('Store mutations', () => {

    it('saves the userId', () => {
      let newUserId = 'TestMan1';
      const state = {userId: undefined};
      mutations.saveId(state, newUserId);
      expect(state.userId).to.equal('TestMan1');
    });

    it('saves the userPassword', () => {
      let newUserPassword = 'TestPassword1';
      const state = {userPassword: undefined};
      mutations.savePassword(state, newUserPassword);
      expect(state.userPassword).to.equal('TestPassword1');
    });

    it('resets the userData', () => {
      let resetUserId = undefined;
      let resetUserPassword = undefined;
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1'
      };
      mutations.resetUserData(state, resetUserId, resetUserPassword);
      expect(state.userId).to.equal(undefined);
      expect(state.userPassword).to.equal(undefined);
    });

    it('saves the aims', () => {
      let newAims = [
        {id: 'TestID' , name: 'TestName'},
        {id: 'TestID2' , name: 'TestName2'}
      ];
      const state = {aims: undefined};
      mutations.saveAims(state, newAims);
      expect(state.aims[0].id).to.deep.equal('TestID');
      expect(state.aims[0].name).to.deep.equal('TestName');
      expect(state.aims[1].id).to.deep.equal('TestID2');
      expect(state.aims[1].name).to.deep.equal('TestName2');
    });

    it('adds an additional aims', () => {
      let newAim = {id: 'TestID3' , name:'TestName3'};
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1',
        aims: [
        {id: 'TestID' , name: 'TestName'},
        {id: 'TestID2' , name: 'TestName2'}
      ]};
      mutations.addAim(state, newAim);
      expect(state.aims[0].id).to.deep.equal('TestID');
      expect(state.aims[0].name).to.deep.equal('TestName');
      expect(state.aims[1].id).to.deep.equal('TestID2');
      expect(state.aims[1].name).to.deep.equal('TestName2');
      expect(state.aims[2].id).to.deep.equal('TestID3');
      expect(state.aims[2].name).to.deep.equal('TestName3');
    });
  });


  describe('requests', () => {
    let onFulfilled;
    let onRejected;

    beforeEach(() => {
      moxios.install();
      onFulfilled = sinon.spy();
      onRejected = sinon.spy();
    });

    afterEach(() => {
      moxios.uninstall();
    });

    it('should mock responses of the logout controller', (done) => {
      axios.get('http://localhost:8080/logout').then(onFulfilled);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
        }).then(() => {
          let response = onFulfilled.getCall(0).args[0];
          equal(onFulfilled.called, true);
          equal(response.status, 200);
          done()
        })
      })
    });

    it('should mock response error of the logout controller', (done) => {
      axios.get('http://localhost:8080/logout')
        .then(onFulfilled, onRejected);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 401
        }).then(() => {
          equal(onFulfilled.called, false);
          equal(onRejected.called, true);
          done()
        });
      });
    });

    //Load aims tests

    it('should mock responses of the aim controller', (done) => {
      axios.get('http://localhost:8080/aims')
        .then(onFulfilled);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
        }).then(() => {
          let response = onFulfilled.getCall(0).args[0];
          equal(onFulfilled.called, true);
          equal(response.status, 200);
          done()
        });
      });
    });

    it('should mock response error of the aim controller', (done) => {
      axios.get('http://localhost:8080/aims')
        .then(onFulfilled, onRejected);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 401
        }).then(() => {
          equal(onFulfilled.called, false);
          equal(onRejected.called, true);
          done()
        });
      });
    });
  });
});
