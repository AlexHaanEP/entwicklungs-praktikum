import Vue from 'vue';
import moxios from 'moxios';
import axios from 'axios';
import sinon from 'sinon';
import { equal } from 'assert-plus';
import { mount, RouterLinkStub} from "@vue/test-utils";
import { getters, mutations } from '../../../src/store';
import Trainingsplan from '../../../src/components/Trainingsplan';

describe('Trainingsplan.vue', () =>  {

  it('should render correct contents', () => {
    const wrapper = mount(Trainingsplan, {
      stubs: {
        RouterLink: RouterLinkStub
      }
    });
    expect(wrapper.find(RouterLinkStub).props().to)
      .to.equal('Dashboard');
    expect(wrapper.findAll(RouterLinkStub).at(0).text())
      .to.equal('Profil');
    expect(wrapper.find('b-button').text())
      .to.equal('Admin');
    expect(wrapper.findAll('b-button').at(2).text())
      .to.equal('logout');
  });

  describe('Store getters', () => {

    it('returns the userId', () => {
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1'
      };
      const result = getters.userId(state);
      expect(result).to.deep.equal('TestMan1');
    });

    it ('returns the userPassword', () => {
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1'
      };
      const result = getters.userPassword(state);
      expect(result).to.deep.equal('TestPassword1');
    });
  });

  describe('Store mutations', () => {

    it('saves the userId', () => {
      let newUserId = 'TestMan1';
      const state = {userId: undefined};
      mutations.saveId(state, newUserId);
      expect(state.userId).to.equal('TestMan1');
    });

    it('saves the userPassword', () => {
      let newUserPassword = 'TestPassword1';
      const state = {userPassword: undefined};
      mutations.savePassword(state, newUserPassword);
      expect(state.userPassword).to.equal('TestPassword1');
    });

    it('resets the userData', () => {
      let resetUserId = undefined;
      let resetUserPassword = undefined;
      const state = {
        userId: 'TestMan1',
        userPassword: 'TestPassword1'
      };
      mutations.resetUserData(state, resetUserId, resetUserPassword);
      expect(state.userId).to.equal(undefined);
      expect(state.userPassword).to.equal(undefined);
    });
  });


  describe('requests', function () {
    let onFulfilled;
    let onRejected;

    beforeEach(function () {
      moxios.install();
      onFulfilled = sinon.spy();
      onRejected = sinon.spy();
    });

    afterEach(function () {
      moxios.uninstall();
    });

    it('should mock responses', function (done) {
      axios.get('http://localhost:8080/logout').then(onFulfilled);

      moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
        }).then(function () {
          let response = onFulfilled.getCall(0).args[0];
          equal(onFulfilled.called, true);
          equal(response.status, 200);
          done()
        })
      })
    });

    it('should mock response error', function (done) {
      axios.get('http://localhost:8080/logout')
        .then(onFulfilled, onRejected);

      moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 401
        }).then(function () {
          equal(onFulfilled.called, false);
          equal(onRejected.called, true);
          done()
        });
      });
    });
  });
});
