import Vue from 'vue'
import moxios from 'moxios';
import axios from 'axios'
import sinon from 'sinon'
import { equal } from 'assert-plus'
import { mount } from "@vue/test-utils"
import Register from '../../../src/components/Register'

describe('Register.vue', () => {

  it('should render correct contents', () => {
    const vm = mount(Register);
    expect(vm.find('h5').text())
      .to.equal('Registrierung');
    expect(vm.find('b-button').text()).to.equal('Registrieren');
    expect(vm.findAll('div').at(1).text())
      .to.equal('Sie sind nun registriert!\n      Bitte verifizieren Sie sich mit Ihrer studentischen E-mail:\n' +
      '      \n        E-mail-Service der Universität Passau')
  });

  it('checks the entered user data and accepts it', () => {
    expect(Register.methods.isValidID('12345678')).to.equal(true);
    expect(Register.methods.isValidPasswords('12345679',
      '12345679', '12345678')).to.equal(true);
    expect(Register.methods.isValidBirthDate('19991122')).to.equal(true);
    expect(Register.methods.validateInput('12345679',
      '12345679', '12345678',
      '19981122', 'Testperson')).to.equal(true)
  });


//password unit tests

  it('checks the entered password and declines it because it´s too short', () => {
    expect(Register.methods.isValidPasswords('12345', '12345',
      'test')).to.equal(false);
    expect(Register.methods.validateInput('12345',
      '12345', 'abcdefgh',
      '19981122', 'Testperson')).to.equal(false)
  });

  it('checks the entered password and declines it because it´s not similar to the confirmed password', () => {
    expect(Register.methods.isValidPasswords('12345678', '123',
      'test')).to.equal(false);
    expect(Register.methods.validateInput('12345678',
      '123', 'abcdefgh',
      '19981122', 'Testperson')).to.equal(false)
  });

  it('checks the entered password and declines it because it´s similar to the entered id', () => {
    expect(Register.methods.isValidPasswords('12345678', '12345678',
      '12345678')).to.equal(false);
    expect(Register.methods.validateInput('12345678',
      '12345678', '12345678',
      '19981122', 'Testperson')).to.equal(false)
  });

  it('checks the entered password and accepts it', () => {
    expect(Register.methods.isValidPasswords('12345678', '12345678',
      'abcdefgh')).to.equal(true);
    expect(Register.methods.validateInput('12345678',
      '12345678', 'abcdefgh',
      '19981122', 'Testperson')).to.equal(true)
  });


//id unit tests

  it('checks the entered id and declines it because it´s longer than 8 characters', () => {
    expect(Register.methods.isValidID('12345678910')).to.equal(false);
    expect(Register.methods.validateInput('12345679',
      '12345679', '12345678910',
      '19981122', 'Testperson')).to.equal(false)
  });

  it('checks the entered id and accepts it', () => {
    expect(Register.methods.isValidID('testme01')).to.equal(true);
    expect(Register.methods.validateInput('12345679',
      '12345679', 'testme01',
      '19981122', 'Testperson')).to.equal(true)
  });


//birth date unit tests

  it('checks the entered birth date and accepts it', () => {
    expect(Register.methods.isValidBirthDate('19991122')).to.equal(true);
    expect(Register.methods.validateInput('12345679',
      '12345679', '12345678',
      '19981122', 'Testperson')).to.equal(true)
  });

  it('checks the entered birth date and declines it because it´s in the future', () => {
    expect(Register.methods.isValidBirthDate('24561122')).to.equal(false);
    expect(Register.methods.validateInput('12345679',
      '12345679', '12345678',
      '24561122', 'Testperson')).to.equal(false)
  });

  it('checks the entered birth date and declines it because the user too young', () => {
    expect(Register.methods.isValidBirthDate('20101224')).to.equal(false);
    expect(Register.methods.validateInput('12345679',
      '12345679', '12345678',
      '20101224', 'Testperson')).to.equal(false)
  });
});


//http request unit tests

  describe('requests', function () {
    let onFulfilled;
    let onRejected;

    beforeEach(function () {
      moxios.install();
      onFulfilled = sinon.spy();
      onRejected = sinon.spy();
    });

    afterEach(function () {
      moxios.uninstall();
    });

    it('should mock responses', function (done) {
      axios.post('http://localhost:8080/users').then(onFulfilled);

      moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
        }).then(function () {
          let response = onFulfilled.getCall(0).args[0];
          equal(onFulfilled.called, true);
          equal(response.status, 200);
          done()
        })
      })
    })
  });


